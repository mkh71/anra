<?php

namespace App\Components;

use Postmark\PostmarkClient;
use Illuminate\Support\Facades\Log;
use App;

class Email
{

    private static $client = null;

    private static function getClient()
    {
        if (!self::$client) {
            self::$client = new PostmarkClient(getenv('POSTMARK_API_TOKEN'));
        }
        return self::$client;
    }

    private static function sendEmail($data)
    {

        try {
             if (getenv('EMAIL_TRIGGER') == 'true' && $data['template_id']) {
                return self::getClient()->sendEmailWithTemplate(
                    $data['from'],
                    $data['to'],
                    (int)$data['template_id'],
                    $data['template_data'],
                    true,
                    null,
                    true,
                    null,
                    $data['cc'] ?? null
                );
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    private static function getTemplateId($type)
    {
        $config = [
            'sendSupplierWelcomeEmail' => getenv('SUPPLIER_WELCOME_EMAIL'),
        ];
        if (isset($config[$type]))
        {
            return $config[$type];
        }
        return null;
    }

    public static function prepareData($options = [], $encode = true)
    {
        $data = [
            'from' => isset($options['from']) ? $options['from'] : getenv('SEND_EMAIL_FROM'),
            'to' => $options['to'],
            'template_data' =>$options['template_data'] ?? '',
            'template_id' => $options['template_id'] ?? self::getTemplateId(debug_backtrace()[1]['function']),
            'cc' => $options['cc'] ?? null,
        ];
        if ($encode) {
            return "'" . json_encode($data) . "'";
        }
        return $data;
    }

    public function unreadMessageNotification($data): void
    {
        $email_data = self::prepareData([
            'to' => $data['email'],
            'template_data' => [
                'action_url' => getenv('FRONTEND_URL').'/messages',
                'user_type' => $data['user_type'],
            ],
        ], false);
        self::sendEmail($email_data);
    }
}
