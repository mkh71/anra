<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorWorkExperience extends Model
{
    // use HasFactory;
    use Uuids;
    protected $table = 'doc_work_experience';
    protected $guarded = [];
    public $incrementing = false;
    protected $keyType = 'string';
}
