<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Medicine;

class MedReminder extends Model
{
    use SoftDeletes;
    // public $timestamps = false;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $table = 'med_reminder';
    protected $casts = [
      'select_days' => 'array', 'other_params' => 'json', 'times' => 'array',
    ];
    /**
    * Override parent boot and Call deleting event
    *
    * @return void
    */
    protected static function boot()  {
        parent::boot();
    }

    // public function options()
    // {
    //     return $this->hasMany('App\QuestionOption');
    // }

    public function med_info()
    {
        return $this->hasOne('App\Models\Medicine', 'id', 'med_id');
    }
}
 