<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavDoctorPatient extends Model
{

    protected $table = 'fav_doctor_patient';
    protected $guarded = [];

    public function fav_doc_info() {
        return $this->hasOne('App\Models\User', 'id', 'doctor_id');
    }

    public function fav_pat_info() {
        return $this->hasOne('App\Models\User', 'id', 'patient_id');
    }

    public function created_by_info() {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
}
