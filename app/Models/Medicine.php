<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{   
    use Uuids;

    protected $table = 'medicine';
    public $incrementing = false;
    protected $guarded = ['id'];
}
