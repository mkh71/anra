<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorProfile extends Model
{
	use Uuids;
    // use HasFactory;
    protected $table = 'doctor_profile';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
}
