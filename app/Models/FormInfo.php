<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormInfo extends Model
{

    protected $table = 'form_info';
    protected $guarded = [];

   	public function form_categories() {
        return $this->hasMany('App\Models\FormCateg', 'form_id', 'id')->orderBy('created_at', 'desc');
    }
}
