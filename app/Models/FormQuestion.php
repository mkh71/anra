<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormQuestion extends Model
{

    protected $table = 'form_questions';
    protected $guarded = [];
    protected $casts = [
    	'field_options' => 'array',
    ];

    public function field_type_info() {
        return $this->hasOne('App\Models\FieldType', 'id', 'field_type_id');
    }

    public function field_action_info() {
        return $this->hasOne('App\Models\FieldAction', 'id', 'field_action_id');
    }

    public function answer_info() {
        return $this->hasOne('App\Models\UserFormAnswer', 'question_id', 'id');
    }
}
