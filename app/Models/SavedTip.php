<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SavedTip extends Model
{
    protected $table = 'saved_tips';
    protected $guarded = [];
    
    public function tip_info() {
        return $this->hasOne('App\Models\Tip', 'id', 'tip_id');
    }
}
