<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormCateg extends Model
{

    protected $table = 'form_categories';
    protected $guarded = [];

    public function form_questions() {
        return $this->hasMany('App\Models\FormQuestion', 'form_categ_id', 'id');
    }
}
