<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorSpecialities extends Model
{
    // use HasFactory;
    protected $table = 'doctor_specialities';
    protected $guarded = [];

    public function speciality_details() {
        return $this->hasOne('App\Models\Category', 'id', 'categ_id');
    }

    public function doctor_specialities() {
        return $this->hasMany('App\Models\Category', 'id', 'categ_id');
    }
}
