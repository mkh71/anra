<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorEducation extends Model
{
    // use HasFactory;
    use Uuids;
    protected $table = 'doc_education';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
}
