<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class MedicinePrescription extends Model
{	
    // use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'pat_id', 'doc_id', 'med_name', 'dosage', 'unit', 'how_many_days', 'group_no', 'pdf', 'frequency', 'duration'
    ];

    protected $table = 'medicine_prescription';
    
    // public function patient_info()
    // {
    //     return $this->hasOne('App\User', 'id', 'pat_id')->select('id', 'name', 'email');
    // }

    // public function doc_info()
    // {
    //     return $this->hasOne('App\User', 'id', 'doc_id')->select('id', 'name', 'email');
    // }

    public function med_info()
    {
        return $this->hasOne('App\Medicine', 'id', 'med_name');
    }
}
