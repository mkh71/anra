<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
	use Uuids;

    protected $table = 'user_addresses';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
}
