<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFormAnswer extends Model
{

    protected $table = 'user_form_answers';
    protected $guarded = [];
    protected $casts = [
    	'options_answers' => 'array',
    	'files' => 'array',
    ];
}
