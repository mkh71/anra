<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldAction extends Model
{

    protected $table = 'field_action';
    protected $guarded = [];

}
