<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TokBoxHistory extends Model
{
    use Uuids;
    protected $table = 'tokbox_history';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];

}
