<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DoctorSpecialities;

class Appointment extends Model
{
    use Uuids;

    public $incrementing = false;
    protected $keyType = 'string';
    protected $table = 'appointment';
    protected $guarded = [];

    public function doctor_info() {
        return $this->hasOne('App\Models\User', 'id', 'doctor_id');
    }

    public function patient_info() {
        return $this->hasOne('App\Models\User', 'id', 'patient_id');
    }

    public function specialities() {
        return $this->hasMany(DoctorSpecialities::class, 'doc_id', 'doctor_id');
    }
}
