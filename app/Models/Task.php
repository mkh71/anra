<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // use HasFactory;
    protected $table = 'task';
    protected $guarded = [];

    protected $casts = [
    	'file_path' => 'array', 'file_type' => 'array',
    ];

    public function added_by_info() {
        return $this->hasOne('App\Models\User', 'id', 'added_by')->select('id', 'name', 'email', 'status');
    }

    public function categ_info() {
        return $this->hasOne('App\Models\TaskCateg', 'id', 'task_categ_id');
    }
}
