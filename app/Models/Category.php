<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // use HasFactory;
    use Uuids;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $guarded = [];

    public function doctor_ids() {
        return $this->hasMany('App\Models\DoctorSpecialities', 'categ_id', 'id');
    }
}
