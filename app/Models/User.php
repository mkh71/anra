<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\{
    DoctorEducation,
    DoctorSpecialities,
    DoctorWorkExperience,
    DoctorAward,
    DoctorProfile,
    Appointment,
};


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Uuids, HasRoles;

    public $incrementing = false;
    protected $keyType = 'string';

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name',
    //     'email',
    //     'password',
    //     'phone',
    //     'country_code',
    //     'dob',
    //     'device_token',
    //     'device_type',
    //     'otp',
    //     'status',
    //     'is_otp_verified',
    //     'profile_photo',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'parent_id',
        'refrence_key',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['user_type', 'chart_no'];

    public function getUserTypeAttribute() {
        $role_name = $this->roles[0]->slug;
        unset($this->roles);
        return $role_name;
    }

    public function getChartNoAttribute() {
        $role_name = $this->roles[0]->slug;
        unset($this->roles);
        $chart_no = '';
        if($role_name != 'Patient') {
            $chart_no = 'ANR-'.$this->refrence_key;
        } else {
            if(isset($this->user_address)) {
                $chart_no = $this->user_address->province.$this->refrence_key;
            } else {
                $chart_no = 'ANR'.$this->refrence_key;
            }
        }
        return $chart_no;
    }


    public function user_address() {
        return $this->hasOne('App\Models\UserAddress', 'user_id', 'id')->orderBy('created_at', 'asc');
    }

    public function doctor_basic_profile() {
        return $this->hasOne('App\Models\DoctorProfile', 'doctor_id', 'id');
    }

    public function basic_profile() {
        return $this->hasOne('App\Models\DoctorProfile', 'doctor_id', 'id');
    }

    public function doc_specialities() {
        return $this->hasOne('App\Models\DoctorSpecialities', 'doctor_id', 'id');
    }

    public function educations() {
        return $this->hasMany(DoctorEducation::class, 'doc_id', 'id');
    }

    public function specialities() {
        return $this->hasMany(DoctorSpecialities::class, 'doc_id', 'id');
    }

    public function experiences() {
        return $this->hasMany(DoctorWorkExperience::class, 'doc_id', 'id');
    }

    public function awards() {
        return $this->hasMany(DoctorAward::class, 'doc_id', 'id');
    }

    public function appointment() {
        return $this->hasMany(Appointment::class, 'doctor_id', 'id');
    }

    public function patient() {
        return $this->hasMany(Appointment::class, 'doctor_id', 'id');
    }

    public function lastAppointment() {
        return $this->hasMany(Appointment::class, 'patient_id', 'id')
            ->select('id', 'patient_id', 'book_on', 'doctor_id', 'start_time', 'created_at')
            ->orderBy('id', 'DESC')
            ->limit(1);
    }

    public function patientAppointment() {
        return $this->hasMany(Appointment::class, 'patient_id', 'id')
            ->select('id', 'patient_id', 'book_on', 'doctor_id', 'start_time', 'created_at', 'status')
            ->orderBy('id', 'DESC')
            ->limit(1);
    }

}
