<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Vital extends Model
{
    use Uuids;
    protected $table = 'vitals';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];

    protected $casts = [
        'vital_date' => 'datetime:Y-m-d H:i',
    ];

    public function getVitalDateAttribute($date) 
    { 
        if (!empty($date)) 
            return \Carbon\Carbon::parse($date)->format('Y-m-d H:i'); 
    }
}
