<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorAvailability extends Model
{
    // use HasFactory;
    use Uuids;
    protected $table = 'doc_availability';
    protected $guarded = [];
    public $incrementing = false;
    protected $keyType = 'string';
}
