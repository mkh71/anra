<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorTaskCategory extends Model
{
    // use HasFactory;
    protected $table = 'doctor_task_categories';
    protected $guarded = [];

    public function task_categ_info() {
        return $this->hasOne('App\Models\TaskCateg', 'id', 'task_categ_id');
    }

    public function doctor_info() {
        return $this->hasOne('App\Models\User', 'id', 'doc_id')->select('id, name, email, status');
    }
}
