<?php

namespace App\Models;

use App\Models\Message;
use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    use Uuids;
    
    protected $table = 'receivers';
 	public $incrementing = false;
    protected $guarded = [];

    public function message() {
        return $this->hasMany(Message::class, 'id', 'message_id');
    }
}
