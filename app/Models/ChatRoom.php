<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{   
    use Uuids;

    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
    protected $casts = [
        'user_ids' => 'array',
    ];

    public function messages() {
        return $this->hasMany('App\Models\Message', 'chat_room_id')->with('sender')->latest()->take(25);
    }

    public function last_message() {
        return $this->hasOne('App\Models\Message', 'chat_room_id')->orderBy('created_at', 'desc');
    }
 }
