<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipCateg extends Model
{

    protected $table = 'tips_categ';
    protected $guarded = [];


   public function tips() {
        return $this->hasOne('App\Models\Tip', 'tip_categ_id', 'id');
    }
}
