<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskInfo extends Model
{
    // use HasFactory;
    protected $table = 'task_info';
    protected $guarded = [];
    protected $hidden = [
        'file_path',
        'file_type',
    ];

    public function assign_by_info() {
        return $this->hasOne('App\Models\User', 'id', 'assign_by')->select('id', 'name', 'email', 'status');
    }

    public function assign_to_info() {
        return $this->hasOne('App\Models\User', 'id', 'assign_to')->select('id', 'name', 'email', 'status');
    }

    public function task_details() {
        return $this->hasOne('App\Models\Task', 'id', 'task_id');
    }
}
