<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Doctor extends Model
{
    use HasFactory;

    public function profileInfo(string $id = null, string $keyword = null)
    {
        return User::select([
            'users.id', 'name', 'profile_photo', 'gender',  'country_code', 'phone', 'email', 'dob',
            'about_me', 'address', 'city', 'state', 'country', 'postal_code'
        ])
        ->with([
            'educations',
            'specialities.doctor_specialities',
            'experiences',
            'awards',
        ])
        ->join('doctor_profile', 'doctor_profile.doctor_id', 'users.id')
        ->when($id, function($query) use ($id) {
            return $query->where('users.id', $id);
        })
        ->when($keyword, function($query) use ($keyword) {
            return $query->where('users.name', 'LIKE', '%'.$keyword.'%');
        })
        ->get();
    }
}
