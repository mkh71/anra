<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    // use HasFactory;
    use Uuids;
    protected $table = 'notes';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $guarded = [];


    public function doctor_info() {
        return $this->hasOne('App\Models\User', 'id', 'doctor_id')->select('id', 'name', 'email', 'status');
    }

    public function patient_info() {
        return $this->hasOne('App\Models\User', 'id', 'patient_id')->select('id', 'name', 'email', 'status');
    }
}
