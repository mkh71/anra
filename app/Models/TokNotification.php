<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TokNotification extends Model
{
    use Uuids;
    protected $table = 'tok_notification';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
    

    public function user_details() {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->select('name', 'id', 'email', 'profile_photo');
    }

}
