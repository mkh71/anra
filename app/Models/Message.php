<?php

namespace App\Models;

use App\Models\User;
use App\Models\Receiver;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	use Uuids;
 	public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];
    protected $casts = [
        'file_url' => 'array',
    ];

    public function sender() {
        return $this->hasOne(User::class, 'id', 'sender_id')->select(['id', 'name', 'email', 'profile_photo']);
    }

    public function receivers() {
        return $this->hasMany(Receiver::class);
    }
}
