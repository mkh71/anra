<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskCateg extends Model
{
    // use HasFactory;
    protected $table = 'task_categories';
    protected $guarded = [];

    public function added_by_info() {
        return $this->hasOne('App\Models\User', 'id', 'added_by')->select('id', 'name', 'email', 'status');
    }
}
