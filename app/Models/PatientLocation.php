<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientLocation extends Model
{

	protected $table = 'patient_location';
	 protected $guarded = [];
}
