<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorOtherWorkLocation extends Model
{
    // use HasFactory;
    use Uuids;
    protected $table = 'doc_other_location';
    protected $guarded = [];
    public $incrementing = false;
    protected $keyType = 'string';


    public function location_availability() {
        return $this->hasMany('App\Models\DoctorAvailability', 'other_location_id', 'id');
    }
}
