<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRelation extends Model
{
    
    protected $table = 'user_relation';
    
    protected $guarded = [];
    
    public function patient_info() {
        return $this->hasOne('App\Models\User', 'id', 'patient_id');
    }

    public function doctor_info() {
        return $this->hasOne('App\Models\User', 'id', 'doctor_id');
    }
}
