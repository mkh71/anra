<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class CheckPatientHaveValidPackage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(!Auth::user()->have_active_package) {
            return response(['msg' => 'Sorry ! You have not an active Package. Please purcahse a package first.', 'status' => false, 'have_active_package' => Auth::user()->have_active_package], 403);
        }

        return $next($request);
    }
}
