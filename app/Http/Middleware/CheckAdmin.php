<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::user()->hasRole('Admin')) {
            if(\Request::wantsJson()) {
                return response(['msg' => 'Sorry ! You are not authorized to access this.', 'status' => false], 403);
            } else {
                return response()->view('errors.404');
            }
        }

        if(!Auth::user()->status) {
            if(\Request::wantsJson()) {
                return response(['msg' => 'Your account is not active. Please contact with admin.', 'status' => false], 403);
            } else {
                return response()->view('errors.404');
            }
        }

        return $next($request);
    }
}
