<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePatient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email",
            "name" => "required|string|min:3|max:100",
            "phone" => "required|integer|digits:10|unique:users,phone",
            "gender" => "required|string|in:male,female",
            "dob" => "required|date",
            "country_code" => "required|in:+91,+1",
            "address" => "required|string|min:2|max:150",
            "state" => "required|string|min:2|max:150",
            "city" => "required|string|min:2|max:150",
            "country" => "required|string|min:2|max:150",
            "province" => "required|string|min:2|max:10",
            "password" => "required|min:5|required_with:password_confirmation|same:password_confirmation"
        ];
    }
}
