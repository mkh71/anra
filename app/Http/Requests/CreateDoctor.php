<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDoctor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email",
            "name" => "required|string|min:3|max:100",
            "phone" => "required|string|unique:users,phone",
            "gender" => "required|string|in:male,female",
            "dob" => "required|date",
            "about_me" => "nullable|string",
            "address" => "required|string|min:2|max:150",
            "state" => "required|string|min:2|max:150",
            "city" => "required|string|min:2|max:150",
            "country" => "required|string|min:2|max:150",
            "postal_code" => "required|string|min:2|max:10",
            "degree" => "nullable|array",
            "college" => "nullable|array",
            "starting_year" => "nullable|array",
            "completion_year" => "nullable|array",
            "hospital_name" => "nullable|array",
            "started_from" => "nullable|array",
            "end_to" => "nullable|array",
            "designation" => "nullable|array",
            "award_name" => "nullable|array",
            "award_year" => "nullable|array",
            "specialist" => "nullable|string",
            "services" => "nullable|string",
        ];
    }
}
