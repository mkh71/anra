<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Appointment;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class AppointmentController extends Controller
{

    public function getAppointments() {
    	$appointments = Appointment::where('doctor_id', Auth::User()->id)->with('patient_info')->get();
        return view('doctor.appointments', compact('appointments'));
    }
    
    // public function getProfileSettings() {
    //     return view('doctor.profile-settings');
    // }

    // public function getScheduleTimings() {
    //     return view('doctor.schedule-timings');
    // }
 
}