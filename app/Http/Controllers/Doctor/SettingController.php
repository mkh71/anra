<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\{
    UpdateProfileSetting,
    UpdatePassword,
};
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    Doctor,
    User,
    DoctorProfile,
    DoctorEducation,
    DoctorWorkExperience,
    DoctorAward,
    Category,
    DoctorSpecialities,
    DoctorAvailability,
    Appointment,
};
use Illuminate\Support\Str;

class SettingController extends Controller
{

    public function getChangePassword() {
        return view('doctor.change-password');
    }
    
    public function getProfileSettings() {
        $id = Auth::user()->id;
        $categories = Category::all();
        $doctor_profile = (new Doctor)->profileInfo($id)->toArray();
        $specialities = $doctor_profile[0]["specialities"][0]["doctor_specialities"];
        $selected = [];
        foreach ($specialities as $special) {
            $selected[] = $special["id"];
        }
        return view('doctor.profile-settings', \compact("doctor_profile", "categories", "selected"));
    }

    public function updateProfileSettings(Request $request) {
        $input = $request->all();
        User::where('id', $input['id'])->update([
            "name" => $input["name"],
            "phone" => $input["phone"],
            "gender" => $input["gender"],
            "dob" => $input["dob"],
        ]);

        DoctorProfile::where('doctor_id', $input['id'])->update([
            "about_me" => $input["about_me"],
            "address" => $input["address"],
            "city" => $input["city"],
            "state" => $input["state"],
            "country" => $input["country"],
            "postal_code" => $input["postal_code"],
        ]);
        
        DoctorSpecialities::where("doc_id", $input['id'])->delete();
        DoctorSpecialities::insert([
            "doc_id" => $input['id'],
            "categ_id" => $input['specialization'],
        ]);

        DoctorEducation::where("doc_id", $input['id'])->delete();
        $education = [];
        $counter = 0;
        foreach($input['degree'] as $degree) {
            $temp_education = [
                "id" => (string) Str::uuid(),
                "doc_id" => $input['id'],
                "degree" => $degree,
                "college" => $input['college'][$counter],
                "starting_year" => $input['starting_year'][$counter],
                "completion_year" => $input['completion_year'][$counter],
            ];
            array_push($education, $temp_education);
            $counter++;
        }
        DoctorEducation::insert($education);

        DoctorWorkExperience::where("doc_id", $input['id'])->delete();
        $experience = [];
        $counter = 0;
        foreach($input['hospital_name'] as $name) {
            $temp_experience = [
                "id" => (string) Str::uuid(),
                "doc_id" => $input['id'],
                "hospital_name" => $name,
                "started_from" => $input['started_from'][$counter],
                "end_to" => $input['end_to'][$counter],
                "designation" => $input['designation'][$counter],
            ];
            array_push($experience, $temp_experience);
            $counter++;
        }
        DoctorWorkExperience::insert($experience);

        DoctorAward::where("doc_id", $input['id'])->delete();
        $awards = [];
        $counter = 0;
        foreach($input['award_name'] as $award) {
            $temp_award = [
                "doc_id" => $input['id'],
                "award_name" => $award,
                "award_year" => $input['award_year'][$counter],
            ];
            array_push($awards, $temp_award);
            $counter++;
        }
        DoctorAward::insert($awards);

        return back()->with('message','Profile has been updated.');
    }

    public function getScheduleTimings() {

        $doc_id = Auth::user()->id;
        $doctor_availability = DoctorAvailability::where('doc_id', $doc_id)
        ->orderBy('day_number')
        ->orderBy('start_time')
        ->get()
        ->toArray();
        $day_availability = ["0" => [], "1" => [], "2" => [], "3" => [], "4" => [], "5" => [], "6" => []];
        foreach ($doctor_availability as $day) {
            $day_availability[$day["day_number"]][] = $day;
        }

        return view('doctor.schedule-timings', \compact('day_availability'));
    }

    public function addScheduleTimings(Request $request) {
        $doc_id = Auth::user()->id;
        $input = $request->all();

        $save_format = [];
        $counter = 0;
        foreach($input["start_time"] as $time) {
            $save_format[] = [
                "id" => (string) Str::uuid(),
                "doc_id" => $doc_id,
                "time_slot_duration" => $input["time_slot_duration"],
                "day_number" => $input["day_number"],
                "start_time" => $time,
                "end_time" => $input["end_time"][$counter],
            ];
            $counter++;
        }
        DoctorAvailability::insert($save_format);
        return back()->with('message','Record has been updated.');
    }

    public function updatePassword(UpdatePassword $request) {
        $input = $request->validated();
        $id = Auth::user()->id;
        User::where('id', $id)->update([
            "password" => \bcrypt($input["new_password"]),
        ]);
        return back()->with('message','Password has been updated.');
    }

    public function deleteSchedule(Request $request) {

        $doc_id = Auth::user()->id;

        $input = $request->all();
        $input["start_time"];

        $booked_slots = Appointment::where('doctor_id', '=', $doc_id)
            ->whereRaw("DAYOFWEEK(DATE(book_on)) - 1 = ".$input['day'])
            ->where('start_time', '=', $input['start_time'])
            ->where('end_time', '=', $input['end_time'])
            ->where('status', '=', 'pending')
            ->count();

        if($booked_slots > 0) {
            return json_encode([
                "message" => "Appointment booked for this slot",
                "status" => true,
            ]);
        }

        DoctorAvailability::where("id", $input["id"])->delete();

        return json_encode([
            "message" => "Your availability has been updated",
            "status" => true,
        ]);
    }
 
}