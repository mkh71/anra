<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Appointment;
use \Carbon\Carbon;

class DashboardController extends Controller
{

    public function getDashboard() {
    	$upcomming_start = Carbon::today()->addDays(1);
    	$upcomming_end = Carbon::today()->addDays(20);

    	$appointments['today'] = Appointment::whereDate('book_on', Carbon::today())->where('doctor_id', Auth::User()->id)->with('patient_info')->orderBy('book_on', 'asc')->orderBy('start_time', 'asc')->get();  
    	$appointments['upcomming'] = Appointment::whereBetween('book_on', [$upcomming_start, $upcomming_end])->where('doctor_id', Auth::User()->id)->with('patient_info')->orderBy('book_on', 'asc')->orderBy('start_time', 'asc')->get();
        return view('doctor.dashboard', compact('appointments'));
    }
    
 
}