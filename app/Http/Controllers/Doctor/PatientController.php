<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserRelation;

class PatientController extends Controller
{

    public function getPatientListByDoctors() {

        $id = Auth::user()->id;
        $patients = UserRelation::with('patient_info', 'patient_info.user_address')
        ->where('doctor_id', '1a1f6260-c810-11eb-8686-a5ca26a7a15e')
        ->get();

        return view('doctor.patient_list', compact('patients'));
    }
    
    // public function getProfileSettings() {
    //     return view('doctor.profile-settings');
    // }

    // public function getScheduleTimings() {
    //     return view('doctor.schedule-timings');
    // }
 
}