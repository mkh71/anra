<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        if(Auth::user()->hasRole('Admin')) {
            return redirect('admin/dashboard');
        } elseif (Auth::user()->hasRole('Doctor')) {
            return redirect('doctor/dashboard');
        } elseif (Auth::user()->hasRole('System Nurse')) {
            return redirect('doctor/dashboard');
        } else {
            return redirect('patient/dashboard');
        }

    }
}
