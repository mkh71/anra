<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Session;
use Stripe;
use Validator;
use \Carbon\Carbon;


class PackageController extends Controller
{
    /**
     * List packages
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();
        $response = ['status' => true, 'data' => $packages, 'msg' => 'Packages list found'];
        return response($response, 200);
    }

    public function purchase(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'package_id' => 'required|exists:packages,id',
            'cc_no' => 'required|numeric',
            'cc_month' => 'required|date_format:m',
            'cc_year' => 'required|date_format:Y',
            'cc_cvv' => 'required|numeric',
            'card_holder_name' => 'required|max:255',
            'billing_address' => 'required|max:255',
            'billing_address_city' => 'required|max:255',
            'billing_address_country' => 'required|max:255',
            'billing_address_postal' => 'required|max:255',
            'billing_contact' => 'required||numeric',
            'billing_email' => 'required|email|max:255',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        if(config('app.env') != 'local') {
            $stripe_secret = 'sk_test_51I53o8EUly6ppNsTtHnNgRMNRfSKdn6Fp0CUeu34jBsG6qt1Z4QeMOIkZFhUgpMKZAaoUJACfMRtvXUY6ivINh4I00ZxshSUeh';
        } else {
            $stripe_secret = 'sk_test_51I53o8EUly6ppNsTtHnNgRMNRfSKdn6Fp0CUeu34jBsG6qt1Z4QeMOIkZFhUgpMKZAaoUJACfMRtvXUY6ivINh4I00ZxshSUeh';
        }

        $stripe = new \Stripe\StripeClient($stripe_secret);
        try {
            $token = $stripe->tokens->create([
                'card' => [
                    'number' => $input['cc_no'],
                    'exp_month' => $input['cc_month'],
                    'exp_year' => $input['cc_year'],
                    'cvc' => $input['cc_cvv'],
                ],
            ]);
            if(!isset($token['id'])) {
                return redirect()->back()->with('payment-error', 'Sorry Unable to process your payment.');
            }  

            $charge = $stripe->charges->create([
                        'card' => $token['id'],
                        'currency' => 'CAD',
                        'amount' => 10*100,
                        'description' => 'Package Purchased',
                     ]);
            if($charge->status === 'succeeded') { 
                User::whereId(Auth::user()->id)->update(['have_active_package' => true]);
                $user = Auth::User();
                $user_data['id'] = $user->id;
                $user_data['name'] = $user->name;
                $user_data['email'] = $user->email; 
                $user_data['profile_photo'] = $user->profile_photo;
                $user_data['dob'] = $user->dob;
                $user_data['device_type'] = $user->device_type; 
                $user_data['status'] = $user->status;
                $user_data['user_type'] = Auth::user()->roles[0]->name;
                $user_data['status'] = $user->status;
                $user_data['have_active_package'] = $user->have_active_package; 
                return response(['msg' => 'Package Purchased successfully.', 'status' => true, 'data' => $user_data], 200);
            } else {
                return response(['msg' => 'Oops ! Unable to capture your payment.', 'status' => false], 422);
            }
        } catch (Exception $e) {
            return response(['msg' => 'Oops ! '.$e->getMessage(), 'status' => false], 422);
        } catch(\Stripe\Exception\InvalidRequestException $e) {
            return response(['msg' => 'Oops ! '.$e->getMessage(), 'status' => false], 422);
        } catch(\Stripe\Exception\CardException $e) {
            return response(['msg' => 'Oops ! '.$e->getMessage(), 'status' => false], 422);
        }    
    }

    public function purchasePackageWithPaymentIntent(Request $request) {
        $input = $request->all();

        if(config('app.env') != 'local') {
            $stripe_secret = 'sk_test_51I53o8EUly6ppNsTtHnNgRMNRfSKdn6Fp0CUeu34jBsG6qt1Z4QeMOIkZFhUgpMKZAaoUJACfMRtvXUY6ivINh4I00ZxshSUeh';
        } else {
            $stripe_secret = 'sk_test_51I53o8EUly6ppNsTtHnNgRMNRfSKdn6Fp0CUeu34jBsG6qt1Z4QeMOIkZFhUgpMKZAaoUJACfMRtvXUY6ivINh4I00ZxshSUeh';
        }

        \Stripe\Stripe::setApiKey($stripe_secret);
        $intent = \Stripe\PaymentIntent::retrieve($input['payment_id']);
        $intent->capture();
        
        echo '<pre>'; print_r($intent); die;

    }

}