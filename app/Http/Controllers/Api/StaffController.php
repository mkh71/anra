<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\DoctorProfile;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Mail;
use Illuminate\Support\Arr;


class StaffController extends Controller
{
    /**
     * List doctors
     *
     * @return \Illuminate\Http\Response
     */
    public function createStaff(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            "email" => "required|email|unique:users,email",
            "name" => "required|string|min:3|max:100",
            "phone" => "required|integer|digits:10",
            "gender" => "required|string|in:male,female",
            "dob" => "required|date_format:Y-m-d|before:today",
            "country_code" => 'required|in:+91,+1',
            'profile_photo' => 'required',
            // 'device_type' => 'required|in:android,ios',
            // 'device_token' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postal_code' => 'required',
            'country' => 'required',
            'designation' => 'required',
            'license_no' => 'required',
            'active_license_province' => 'required',
            'experience' => 'required|numeric',
            'user_type' => 'required|in:5,6',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $password = 'Anra@'.mt_rand(100000, 999999);
        $input['password'] = bcrypt($password);
        $input['otp'] = mt_rand(100000, 999999);

        $user = User::create([
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'gender' => $input['gender'],
                    'country_code' => $input['country_code'],
                    'phone' => $input['phone'],
                    'dob' => $input['dob'],
                    'profile_photo' => $input['profile_photo'],
                    'device_token' => $input['device_token']??null,
                    'device_type' => $input['device_type']??'ios',
                    'password' => $input['password'],
                    'otp' => $input['otp'],
                    'parent_id' => Auth::User()->id,
                ]);

        DoctorProfile::create([
                        'doctor_id' => $user->id,
                        'address' => $input['address'],
                        'fax' => $input['fax']??null,
                        'city' => $input['city'],
                        'state' => $input['state'],
                        'postal_code' => $input['postal_code'],
                        'country' => $input['country'],
                        'designation' => $input['designation'],
                        'license_no' => $input['license_no'],
                        'active_license_province' => $input['active_license_province'],
                        'experience' => $input['experience'],
                    ]);

        $role_details = Role::whereId($input['user_type'])->first();

        $user->assignRole($role_details->name);

        $email_string = 'Your temporary password for login :'.$password.' and Verification OTP :'.$input['otp'].' . Please change your temporary password after login.';
        (new MailController)->sendBasicEmail($input['email'],  $email_string, 'Account Verification OTP');
        
        $response = ['status' => true, 'msg' => 'Staff Added successfully.'];
        return response($response, 200);
    }

    public function getStaffList(Request $request) {
        $staff_list = User::where('parent_id', Auth::User()->id)->with('basic_profile')->select('id', 'name', 'email', 'gender', 'country_code', 'phone', 'dob', 'profile_photo', 'device_type', 'is_otp_verified', 'status')->get();
        $response = ['status' => true, 'msg' => 'Staff List.', 'data' => $staff_list];
        return response($response, 200);
    }

    public function updateStaffStatus(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            "staff_id" => "required|exists:users,id",
            "status" => 'required|in:0,1',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $staff_info = User::where('parent_id', Auth::User()->id)->where('id', $input['staff_id'])->first();
        if(!$staff_info) {
            $response = ['status' => false, 'msg' => 'You are not authorize to change status.'];
            return response($response, 403);
        }
        $staff_info->status = $input['status'];
        $staff_info->save();
        $response = ['status' => true, 'msg' => 'Status updated successfully.'];
        return response($response, 200);
    }

    public function getStaffTypeList(Request $request) {
        $roles = Role::where('is_doc_staff', true)->select('id', 'slug')->get();
        $response = ['status' => true, 'data' => $roles, 'msg' => 'Staff Roles Type'];
        return response($response, 200);
    }
}