<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class PushNotificationController extends Controller
{
    /**
     * List doctors
     *
     * @return \Illuminate\Http\Response
     */
    public function sendAndroidPushNotification($device_type, $device_id, $message, $title, $data) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Authorization: key=AAAAsHBeNrM:APA91bGEClIYQltfSg2zxBeRAa-tjN0cmgFGwEbno4Az-lvPt9KIiR7a1SmIrk-Rfv3FIyZdxkui9SzjvJRH5pILZP959NvgU-hLkYMbDxP58RKk9r_44LaH4pNZ920lFKExYgf0OKYH',
            'Content-Type: application/json'
        );
        $url = 'https://fcm.googleapis.com/fcm/send';
        if($device_type === 'ios') {
            $fields = array (
                'to' => $device_id,
                'priority'=>'high',
                'notification' => array (
                    "body" => $message,
                    "title" => $title,
                    "icon" => "myicon",
                    "sound" => "incomming.wav",
                    "data" => $data,
                )
            );
        } else {
            $fields = array (
                'to' => $device_id,
                'ttl' => '0s',
                'priority' => 'high',
                "data" => array (
                    "body" => $message,
                    "title" => $title,
                    "icon" => "myicon",
                    "sound"=>"default.wav",
                    "notification_data" => $data,
                )
            );
        }

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $response['msg'] = curl_error($ch);
            $response['status'] = false;
            return $response;
            // die('FCM Send Error: ' . curl_error($ch));
        }

        curl_close($ch);
        $response['msg'] = 'Sent successfully.';
        $response['status'] = true;
        return $response;
    }






    public function sendVoipNotification($voip_token, $device_token) {
        $ch = curl_init();
        $app_id = 'e0390613-9578-45d5-802b-1023d9fab0a8';
        $fields = array(
            'app_id' => $app_id, //fb77c31b-36b2-4278-87bf-e0fa73831e3c
            'identifier' => $voip_token, //ce777617da7f548fe7a9ab6febb56cf39fba6d382000c0395666288d961ee566
            'device_type' => "0",
            'language' => "en",
        );
        $fields = json_encode($fields);

        $headers = array();
        $headers[] = 'Content-Type: application/json';

        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/players');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $response['msg'] = curl_error($ch);
            $response['status'] = false;
            //return $response;
        }
        curl_close($ch);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"$app_id\",\n\"contents\": {\"en\": \"English Message\"},\n\"apns_push_type_override\": \"voip\"}");

        $headers = array();
        $headers[] = 'Content-Type: application/json; charset=utf-8';
        $headers[] = 'Authorization: Basic NmViZmMzNTgtODY4Ni00M2YwLWFjM2ItY2RjODg4NzVjOWVm';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $response['msg'] = curl_error($ch);
            $response['status'] = false;
            return $response;
        }
        curl_close($ch);

        $response['msg'] = 'Sent successfully.';
        $response['status'] = true;
        return $response;
    }

    public function testPushNotification(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'device_token' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array (
            'to' => $input['device_token'],
            'priority'=>'high',
            'notification' => array (
                "body" => 'test body',
                "title" => 'test title',
                "icon" => "myicon",
                "sound" => "default",
                // "data" => $image,
            )
        );
        $fields = json_encode ( $fields );
        $headers = array (
            'Authorization: key=AAAAsHBeNrM:APA91bGEClIYQltfSg2zxBeRAa-tjN0cmgFGwEbno4Az-lvPt9KIiR7a1SmIrk-Rfv3FIyZdxkui9SzjvJRH5pILZP959NvgU-hLkYMbDxP58RKk9r_44LaH4pNZ920lFKExYgf0OKYH',
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec($ch);
        echo '<pre>'; print_r($result); die;
    }
}




//YOUR_REST_API_KEY
//YOUR_VOIP_APP_ID
//DEVICE_VOIP_TOKEN
//YOUR_PLAYER_ID

/*$url = 'https://fcm.googleapis.com/fcm/send';
if($device_type === 'ios') {
    $fields = array (
        'to' => $device_id,
        'priority'=>'high',
        'notification' => array (
            "body" => $message,
            "title" => $title,
            "icon" => "myicon",
            "sound" => "incomming.wav",
            "data" => $data,
        )
    );
} else {
    $fields = array (
        'to' => $device_id,
        'ttl' => '0s',
        'priority' => 'high',
        "data" => array (
          "body" => $message,
          "title" => $title,
          "icon" => "myicon",
          "sound"=>"default.wav",
          "notification_data" => $data,
        )
    );
}


$fields = json_encode ( $fields );
$headers = array (
    'Authorization: key=AAAAsHBeNrM:APA91bGEClIYQltfSg2zxBeRAa-tjN0cmgFGwEbno4Az-lvPt9KIiR7a1SmIrk-Rfv3FIyZdxkui9SzjvJRH5pILZP959NvgU-hLkYMbDxP58RKk9r_44LaH4pNZ920lFKExYgf0OKYH',
    'Content-Type: application/json'
);

$ch = curl_init ();
curl_setopt ( $ch, CURLOPT_URL, $url );
curl_setopt ( $ch, CURLOPT_POST, true );
curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
$result = curl_exec($ch);
if ($result === FALSE) {
    $response['msg'] = curl_error($ch);
    $response['status'] = false;
    return $response;
    // die('FCM Send Error: ' . curl_error($ch));
}
curl_close ( $ch );
$response['msg'] = 'Sent successfully.';
$response['status'] = true;
return $response;*/
