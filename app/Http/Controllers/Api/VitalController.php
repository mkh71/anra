<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vital;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class VitalController extends Controller
{
    public function addVital(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'step_count' => 'required_without_all:sys,dis,heart_rate,weight,body_temp,respiratory_rate',
            'sys' => 'required_without_all:step_count,dis,heart_rate,weight,body_temp,respiratory_rate',
            'dis' => 'required_with:sys',
            'heart_rate' => 'required_without_all:step_count,dis,sys,weight,body_temp,respiratory_rate',
            'weight' => 'required_without_all:step_count,dis,heart_rate,sys,body_temp,respiratory_rate',
            'body_temp' => 'required_without_all:step_count,dis,heart_rate,sys,weight,respiratory_rate',
            'respiratory_rate' => 'required_without_all:step_count,dis,heart_rate,sys,body_temp,weight',
            'vital_date' => 'required',
            'type' => 'required|in:auto,manual',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        if(Vital::whereDate('vital_date', \Carbon\Carbon::parse($input['vital_date'])->format('Y-m-d'))->exists()) {
            Vital::whereDate('vital_date', \Carbon\Carbon::parse($input['vital_date'])->format('Y-m-d'))->delete();
        }

        $created_vital = Vital::create([
            'user_id' => Auth::User()->id,
            'created_by' => Auth::User()->id,
            'step_count' => $input['step_count']??null,
            'sys' => $input['sys']??null,
            'dis' => $input['dis']??null,
            'heart_rate' => $input['heart_rate']??null,
            'weight' => $input['weight']??null,
            'body_temp' => $input['body_temp']??null,
            'respiratory_rate' => $input['respiratory_rate']??null,
            'vital_date' => $input['vital_date'],
            'type' => $input['type'],
        ]);

        if($created_vital) {
            $response = ['status' => true, 'msg' => 'Created Vital', 'data' => $created_vital];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong. '];
            return response($response, 500);
        }
    }

    public function vitalSyncFromSDK(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'vitals' => 'required|array|min:1',
            'vitals.*.step_count' => 'required_without_all:vitals.*.sys,vitals.*.dis,vitals.*.heart_rate,vitals.*.weight,vitals.*.body_temp,vitals.*.respiratory_rate',
            'vitals.*.sys' => 'required_without_all:vitals.*.step_count,vitals.*.dis,vitals.*.heart_rate,vitals.*.weight,vitals.*.body_temp,vitals.*.respiratory_rate',
            'vitals.*.dis' => 'required_with:vitals.*.sys',
            'vitals.*.heart_rate' => 'required_without_all:vitals.*.step_count,vitals.*.dis,vitals.*.sys,vitals.*.weight,vitals.*.body_temp,vitals.*.respiratory_rate',
            'vitals.*.weight' => 'required_without_all:vitals.*.step_count,vitals.*.dis,vitals.*.heart_rate,vitals.*.sys,vitals.*.body_temp,vitals.*.respiratory_rate',
            'vitals.*.body_temp' => 'required_without_all:vitals.*.step_count,vitals.*.dis,vitals.*.heart_rate,vitals.*.sys,vitals.*.weight,vitals.*.respiratory_rate',
            'vitals.*.respiratory_rate' => 'required_without_all:vitals.*.step_count,vitals.*.dis,vitals.*.heart_rate,vitals.*.sys,vitals.*.body_temp,vitals.*.weight',
            'vitals.*.vital_date' => 'required',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        foreach ($input['vitals'] as $key => $vital) {
            if(Vital::whereDate('vital_date', \Carbon\Carbon::parse($vital['vital_date'])->format('Y-m-d'))->exists()) {
                Vital::whereDate('vital_date', \Carbon\Carbon::parse($vital['vital_date'])->format('Y-m-d'))->delete();
            }

            $created_vital = Vital::create([
                'user_id' => Auth::User()->id,
                'created_by' => Auth::User()->id,
                'step_count' => $vital['step_count']??null,
                'sys' => $vital['sys']??null,
                'dis' => $vital['dis']??null,
                'heart_rate' => $vital['heart_rate']??null,
                'weight' => $vital['weight']??null,
                'body_temp' => $vital['body_temp']??null,
                'respiratory_rate' => $vital['respiratory_rate']??null,
                'vital_date' => $vital['vital_date'],
                'type' => 'auto',
            ]);
        }
        if($created_vital) {
            $response = ['status' => true, 'msg' => 'Added Successfull.'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong. '];
            return response($response, 500);
        }

    }

    public function getVitalsByDate(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $vitals_according_to_date = Vital::where('user_id', Auth::User()->id)->whereBetween('vital_date', [$input['start_date'], $input['end_date']])->get();
        if($vitals_according_to_date) {
            $response = ['status' => true, 'msg' => 'Vitals', 'data' => $vitals_according_to_date];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong. '];
            return response($response, 500);
        }
    }

    public function getVitalsByPatientId(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d',
            'patient_id' => 'required|exists:users,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $vitals_according_to_date = Vital::where('user_id', $input['patient_id'])->whereBetween('vital_date', [$input['start_date'], $input['end_date']])->orderBy('vital_date', 'desc')->get();
        if($vitals_according_to_date) {
            $response = ['status' => true, 'msg' => 'Vitals', 'data' => $vitals_according_to_date];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong. '];
            return response($response, 500);
        }
    }

    public function getLastSyncDate() {
        $last_sync = Vital::where('user_id', Auth::User()->id)->orderBy('vital_date', 'desc')->first();
        $response = ['status' => true, 'msg' => 'Last Sync Date', 'data' => $last_sync->vital_date??null];
        return response($response, 200);
    }

}