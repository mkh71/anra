<?php
   
namespace App\Http\Controllers\Api;
   
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctor;
use App\Models\User;
use App\Models\Appointment;
use App\Models\DoctorEducation;
use App\Models\DoctorAward;
use App\Models\DoctorWorkExperience;
use App\Models\DoctorOtherWorkLocation;
use App\Models\DoctorAvailability;
use App\Models\DoctorSpecialities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class AppointmentController extends Controller
{

	public function bookAppointment(Request $request) {
		$input = $request->all();
		 $validator = Validator::make($request->all(), [
            'doctor_id' => 'required|exists:users,id',
            'booking_date' => 'required|date_format:Y-m-d',
            'booking_start_time' => 'required|date_format:H:i',
            'booking_end_time' => 'required|date_format:H:i',
            'booking_com_type' => 'required|in:video,audio,chat',
            'description' => 'required|max:255',
        ]);
        
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        $created_appointment = Appointment::create([
        	'doctor_id' => $input['doctor_id'],
        	'book_on' => $input['booking_date'],
        	'start_time' => $input['booking_start_time'],
        	'end_time' => $input['booking_end_time'],
        	'type' => $input['booking_com_type'],
        	'patient_id' => Auth::User()->id,
        ]);

        if($created_appointment) {
        	$response = ['status' => true, 'msg' => 'Appointment Created Successfully.'];
        	return response($response, 200);
        } else {
        	 $response = ['status' => false, 'msg' => 'Oops Something went wrong.'];
        	return response($response, 500);
        }
	}

    public function updateAppointment(Request $request) {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'appointment_id' => 'required|exists:appointment,id',
            'booking_date' => 'required|date_format:Y-m-d',
            'booking_start_time' => 'required|date_format:H:i',
            'booking_end_time' => 'required|date_format:H:i',
            'booking_com_type' => 'required|in:video,audio,chat',
            'description' => 'required|max:255',
        ]);
        
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        $appointment_details = Appointment::whereId($input['appointment_id'])->first();

        if($appointment_details->doctor_id != Auth::user()->id) {
            $response = ['status' => false, 'msg' => 'You are not authorize to update this appointment.'];
            return response($response, 403);
        }
        
        $appointment_details->book_on = $input['booking_date'];
        $appointment_details->start_time = $input['booking_start_time'];
        $appointment_details->end_time = $input['booking_end_time'];
        $appointment_details->type = $input['booking_com_type'];
        $appointment_details->save();

        if($appointment_details) {
            $response = ['status' => true, 'msg' => 'Appointment Updated Successfully.'];
            return response($response, 200);
        } else {
             $response = ['status' => false, 'msg' => 'Oops Something went wrong.'];
            return response($response, 500);
        }
    }

	public function getPatientAppointments() {
		$appointments = Appointment::where('patient_id', Auth::User()->id)->with('doctor_info')->get();
		$response = ['status' => true, 'msg' => 'Appointment List.', 'data' => $appointments];
        return response($response, 200);
	}

	public function getDoctorAppointments(Request $request) {
        $input = $request->all();
        if(isset($input['date'])) {
            $validator = Validator::make($input, [
                'date' => 'required|date_format:Y-m-d',
                'type' => 'sometimes|in:cancel,completed,pending,accepted',
            ]);
        
            if($validator->fails()) {
                $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
                return response($response, 422);  
            }
          $query = Appointment::whereDate('book_on', $input['date'])->where('doctor_id', Auth::User()->id)->with('patient_info');  
        } else {
		  $query = Appointment::where('doctor_id', Auth::User()->id)->with('patient_info');
        }

        $query->when(request('type') != '', function ($q) use($input){
            $q->where('status', $input['type']);
        });

        $appointments = $query->get();

		$response = ['status' => true, 'msg' => 'Appointment List.', 'data' => $appointments];
        return response($response, 200);
	}

    public function getPatientAppointmentsByPatientId($patient_id) {
        $appointments = Appointment::where('patient_id', $patient_id)->with('patient_info')->get();
        $response = ['status' => true, 'msg' => 'Appointment List.', 'data' => $appointments];
        return response($response, 200);
    }

    public function updateAppointmentStatus(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'appointment_id' => 'required|exists:appointment,id',
            'status' => 'required|in:completed,cancel,accepted',
        ]);
        
        if($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        if(Appointment::whereId($input['appointment_id'])->where('doctor_id', Auth::user()->id)->exists()) {
            $a = Appointment::whereId($input['appointment_id'])->update([
                'status' => $input['status'] 
            ]);

            if($a) {
                $response = ['status' => true, 'msg' => 'Status Updated Successfully.'];
                return response($response, 200);
            } else {
                $response = ['status' => false, 'msg' => 'Oops Something went wrong.']; 
                return response($response, 500);
            }
        } else {
            $response = ['status' => false, 'msg' => 'Oops Something went wrong. May be this appointment does not related to you.'];
            return response($response, 403);
        }
    }
}