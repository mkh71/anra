<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tip;
use App\Models\FavTip;
use App\Models\SavedTip;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class TipController extends Controller
{

	public function getTips() {
		$tips = Tip::all();
		$response = ['status' => true, 'msg' => 'Tips List', 'data' => $tips];
        return response($response, 200);
	}

	public function markTipAsFavourite(Request $request) {
		$input = $request->all();
		$validator = Validator::make($input, [
            'tip_id' => 'required|exists:tips,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        if(FavTip::where('user_id', Auth::User()->id)->where('tip_id', $input['tip_id'])->exists()) {
        	$response = ['status' => false, 'msg' => 'Tip is already in Favourite list'];
        	return response($response, 422);
        } else {
        	FavTip::create([
        		'user_id' => Auth::user()->id,
        		'tip_id' => $input['tip_id'],
        	]);

        	$response = ['status' => true, 'msg' => 'Added to favourite successfully.'];
        	return response($response, 200);
        }

	}

	public function getFavouriteTips() {
		$fav_tips = FavTip::where('user_id', Auth::user()->id)->with('tip_info')->get();
		$response = ['status' => true, 'msg' => 'Favourite Tips', 'data' => $fav_tips];
        return response($response, 200);
	}

	public function markTipAsSaved(Request $request) {
		$input = $request->all();
		$validator = Validator::make($input, [
            'tip_id' => 'required|exists:tips,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        if(SavedTip::where('user_id', Auth::User()->id)->where('tip_id', $input['tip_id'])->exists()) {
        	$response = ['status' => false, 'msg' => 'Tip is already in saved tips list'];
        	return response($response, 422);
        } else {
        	SavedTip::create([
        		'user_id' => Auth::user()->id,
        		'tip_id' => $input['tip_id'],
        	]);

        	$response = ['status' => true, 'msg' => 'Added successfully.'];
        	return response($response, 200);
        }
	}

	public function getSavedTips() {
		$saved_tips = SavedTip::where('user_id', Auth::user()->id)->with('tip_info')->get();
		$response = ['status' => true, 'msg' => 'Saved Tips', 'data' => $saved_tips];
        return response($response, 200);
	}

}