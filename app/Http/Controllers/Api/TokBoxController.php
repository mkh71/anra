<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\PushNotificationController;
use Validator;
use Auth;
use App\Models\User;
use App\Models\TokBoxHistory;
use App\Models\TokNotification;
use Carbon\Carbon;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use Log;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;



class TokBoxController extends Controller
{

    public function createSession() {
        $openTokAPI = new OpenTok('47246904', 'a34d3747c2f2b895689b25e5e0b3829115ec05b7');

       	$session_token = $openTokAPI->createSession(['mediaMode' => MediaMode::ROUTED]);

       	if(TokBoxHistory::where('user_id', Auth::User()->id)->exists()) {
       		TokBoxHistory::where('user_id', Auth::User()->id)->update([
       			'session_id' => $session_token,
       		]);
       	} else {
	       	$created_tox = TokBoxHistory::create([
	       		'user_id' => Auth::User()->id,
	       		'session_id' => $session_token,
	       	]);
       	}

       	$session_data['session'] = TokBoxHistory::where('user_id', Auth::User()->id)->pluck('session_id')->first();
        $response = ['status' => true, 'msg' => 'Session Information for Chat', 'data' => $session_data];
        return response($response, 200);
    }

    public function createTokBoxToken(Request $request) {
    	$input = $request->all();
    	$validator = Validator::make($input, [
            'user_id' => 'required|exists:users,id',
            'session_id' => 'required|exists:tokbox_history,session_id',
            'token_type' => 'required|in:publisher,subscriber',
      ]);

      if($validator->fails()) {
          return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
      }

      $openTokAPI = new OpenTok('47246904', 'a34d3747c2f2b895689b25e5e0b3829115ec05b7');

      $opentok_token['token'] = $openTokAPI->generateToken($input['session_id'], [
              'exerciseireTime' => time()+60,
              'data' => "60 min"
         ]);
      TokBoxHistory::where('session_id', $input['session_id'])->update([
     			'token' => $opentok_token['token'],
     			'token_type' => $input['token_type'],
     		]);

      $response = ['status' => true, 'msg' => 'Chat Token', 'data' => $opentok_token];
      return response($response, 200);
    }

    public function sendNotification(Request $request) {
    	$input = $request->all();
    	$validator = Validator::make($input, [
            'user_ids' => 'required|array|min:1',
            'user_ids.*' => 'exists:users,id',
            'topic' => 'required',
            'notification_type' => 'required',
            'call_type' => 'required',
            'caller_name' => 'required',
            'group_id' => 'required|exists:chat_rooms,id',
            'session_id' => 'required',
            // 'call_token' => 'required',
        ]);
        if ($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }
        $push_notification_custom_params['session_id'] = $input['session_id'];
        $push_notification_custom_params['call_type'] = $input['call_type'];
        $push_notification_custom_params['caller_name'] = $input['caller_name'];
        $push_notification_custom_params['notification_type'] = $input['notification_type'];
        $push_notification_custom_params['caller_pic'] = $input['caller_pic']??null;
        $push_notification_custom_params['group_id'] = $input['group_id'];
        $push_notification_custom_params['user_ids'] = $input['user_ids'];

    	// $created_tok_notification = TokNotification::create([
    	// 	'user_id' => $input['user_id'],
    	// 	'session_id' => $input['session_id'],
    	// 	'topic' => $input['topic'],
    	// 	'type' => $input['notification_type'],
    	// ]);
      foreach ($input['user_ids'] as $key => $user) {
         $user_info = User::whereId($user)->select('id', 'name', 'device_token', 'device_type', 'voip_token')->first();
        if(null != $user_info->device_token) {
            if ($user_info->device_type === 'ios') {
                $notification_details = (new PushNotificationController)
                    ->sendVoipNotification(
                        $user_info->device_token,
                        $user_info->voip_token
                    );
            }else{
                $notification_details = (new PushNotificationController)
                    ->sendAndroidPushNotification(
                        $user_info->voip_token,
                        $user_info->device_token,
                        $input['topic'],
                        'Incoming Call', $push_notification_custom_params
                    );
            }



          if(!$notification_details) {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong. Unable to send notification to mobile.'];
            return response($response, 500);
          }
        }
      }
      $response = ['status' => true, 'msg' => 'Created Notifications'];
      return response($response, 200);
    }

    public function getTokNotification() {
    	$tok_notifications = TokNotification::with('user_details')->get();
      $response = ['status' => true, 'msg' => 'Created Notifications', 'data' => $tok_notifications];
      return response($response, 200);
    }
}
