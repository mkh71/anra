<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class CategoryController extends Controller
{
    /**
     * List doctors
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::whereHas('doctor_ids')->get();
        $response = ['status' => true, 'data' => $categories, 'msg' => 'Categories list found'];
        return response($response, 200);
    }

}