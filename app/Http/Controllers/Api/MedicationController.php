<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Medicine;
use App\Models\MedicinePrescription;
use App\Models\MedReminder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class MedicationController extends Controller
{

    public function addMedication(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'medication_name' => 'required',
            'dosage' => 'required',
            'units' => 'required',
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'required|date|after:yesterday',
            'how_many_times' => 'required',
            'select_days' => 'required|array|min:1|',
            // 'medication_notes' => 'required',
            // 'time_interval' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422); 
        }

        return $this->createMedicationInDatabase($input, Auth::User()->id, 'patient', null);
    }

    public function createMedicationInDatabase($input, $patient_id, $created_by, $doc_id) {
        $medicine = Medicine::where('name', $input['medication_name'])->first();
        if (!$medicine) {
            $medicine = Medicine::create([
                'name' => Str::lower($input['medication_name']),
                'unit' => Str::lower($input['units']),
                'created_by' => $created_by,
            ]);
        }

        $med_reminder = MedReminder::create([
            'patient_id' => $patient_id,
            'doc_id' => $doc_id??null,
            'med_id' => $medicine->id,
            'dosage' => $input['dosage']??null,
            'units' => $input['units']??null,
            'how_many_times' => $input['how_many_times']??null,
            'start_date' => $input['start_date']??null,
            'end_date' => $input['end_date']??null,
            'select_days' => $input['select_days']??null,
            'medication_notes' => $input['medication_notes']??null,
            'time_interval' => $input['time_interval']??null,
        ]);
        $response = ['status' => true, 'data' => $med_reminder, 'msg' => 'Added Successfully.'];
        return response($response, 200);
    }

    public function getAllReminders($patient_id = null) {
        $reminders = MedReminder::where('patient_id', $patient_id??Auth::User()->id)->with('med_info')->get();
        $response = ['status' => true, 'msg' => 'Medication Reminders.', 'data' => $reminders ];
        return response($response, 200);
    }

    public function updateMedicationReminder(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'reminder_id' => 'required|exists:med_reminder,id',
            'medication_id' => 'required|exists:medicine,id',
            'dosage' => 'required',
            'units' => 'required',
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'required|date|after:yesterday',
            'how_many_times' => 'required',
            'select_days' => 'required|array|min:1|',
            // 'medication_notes' => 'required',
            // "time_interval" => 'required',
            // "times" => "required|array|min:1"
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422); 
        }

        $med_reminder = MedReminder::whereId($input['reminder_id'])->where('patient_id', Auth::User()->id)->first();
        if(!$med_reminder) {
            return response(['msg' => 'Sorry ! You can not set the reminder for this medication.', 'status' => false], 422); 
        }

         $med_reminder = MedReminder::where('patient_id', Auth::user()->id)->where('id', $input['reminder_id'])->update([
            'patient_id' => Auth::user()->id,
            'med_id' => $input['medication_id'],
            'dosage' => $input['dosage']??null,
            'units' => $input['units']??null,
            'how_many_times' => $input['how_many_times']??null,
            'start_date' => $input['start_date']??null,
            'end_date' => $input['end_date']??null,
            'select_days' => $input['select_days']??null,
            'medication_notes' => $input['medication_notes']??null,
            'time_interval' => $input['time_interval']??null,
            'times' => $input['times']??null,

        ]);
        $response = ['status' => true, 'msg' => 'Updated Successfully.'];
        return response($response, 200);
    }

    public function getReminderInfo($reminder_id) {
        $med_reminder = MedReminder::whereId($reminder_id)->where('patient_id', Auth::User()->id)->first();
        if(!$med_reminder) {
            return response(['msg' => 'Sorry ! You can not view the reminder details.', 'status' => false], 422); 
        }
        $response = ['status' => true, 'msg' => 'Medication Reminders.', 'data' => $med_reminder ];
        return response($response, 200);
    }

    public function addPatientMedication(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'patient_id' => 'required|exists:users,id',
            'medication_name' => 'required',
            'dosage' => 'required',
            'units' => 'required',
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'required|date|after:yesterday',
            'how_many_times' => 'required',
            'select_days' => 'required|array|min:1|',
            // 'medication_notes' => 'required',
            // 'time_interval' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422); 
        }
        return $this->createMedicationInDatabase($input, $input['patient_id'], 'doctor', Auth::User()->id);
    }

    public function getPatientAllReminders(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'patient_id' => 'required|exists:users,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422); 
        }
        return $this->getAllReminders($input['patient_id']);    
    }

    public function getPatientReminderInfo($reminder_id, Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'patient_id' => 'required|exists:users,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422); 
        }
        $med_reminder = MedReminder::whereId($reminder_id)->where('patient_id', $input['patient_id'])->first();
        if(!$med_reminder) {
            return response(['msg' => 'Sorry ! You can not view the reminder details.', 'status' => false], 422); 
        }
        $response = ['status' => true, 'msg' => 'Medication Reminders.', 'data' => $med_reminder ];
        return response($response, 200);
    }

    public function updatePatientMedicationReminder(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'patient_id' => 'required|exists:users,id',
            'reminder_id' => 'required|exists:med_reminder,id',
            'medication_id' => 'required|exists:medicine,id',
            'dosage' => 'required',
            'units' => 'required',
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'required|date|after:yesterday',
            'how_many_times' => 'required',
            'select_days' => 'required|array|min:1|',
            'medication_notes' => 'required',
            // "time_interval" => 'required',
            // "times" => "required|array|min:1"
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422); 
        }

        $med_reminder = MedReminder::whereId($input['reminder_id'])->where('patient_id', $input['patient_id'])->first();
        if(!$med_reminder) {
            return response(['msg' => 'Sorry ! You can not set the reminder for this medication.', 'status' => false], 422); 
        }

         $med_reminder = MedReminder::where('patient_id', $input['patient_id'])->where('id', $input['reminder_id'])->update([
            'patient_id' => $input['patient_id'],
            'med_id' => $input['medication_id'],
            'dosage' => $input['dosage']??null,
            'units' => $input['units']??null,
            'how_many_times' => $input['how_many_times']??null,
            'start_date' => $input['start_date']??null,
            'end_date' => $input['end_date']??null,
            'select_days' => $input['select_days']??null,
            'medication_notes' => $input['medication_notes']??null,
            'time_interval' => $input['time_interval']??null,
            'times' => $input['times']??null,
            'doc_id' => Auth::User()->id??null,
        ]);
        $response = ['status' => true, 'msg' => 'Updated Successfully.'];
        return response($response, 200);
    }

    // public function getPatientPrescriptionById($value='') {
    //     # code...
    // }
}