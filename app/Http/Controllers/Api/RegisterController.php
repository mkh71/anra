<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\MailController;
use App\Http\Controllers\SmsNotificationController;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendingEmail;


class RegisterController extends Controller
{
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'email' => 'required|email',
        ]);
   
        if($validator->fails()){
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);   
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            auth()->user()->tokens->each(function ($token, $key) {
                $token->delete();
            });
            
            $user = Auth::user(); 
            unset($user['tokens']);
            $token =  $user->createToken('Anra')->accessToken; 

            $otp = mt_rand(1000, 9999);
            $user->otp = $otp;
            $user->device_type = $request->device_type??'web'; 
            $user->save();

            $user_data['id'] = $user->id;
            $user_data['name'] = $user->name;
            $user_data['email'] = $user->email; 
            $user_data['profile_photo'] = $user->profile_photo;
            $user_data['phone'] = $user->phone;
            $user_data['dob'] = $user->dob;
            $user_data['device_type'] = $request->device_type??$user->device_type; 
            $user_data['status'] = $user->status;
            $user_data['user_type'] = Auth::user()->roles[0]->slug;

            if(!Auth::user()->hasRole('Patient')) {
                $user_data['have_active_package'] = 1;
            } else {
                $user_data['have_active_package'] = $user->have_active_package;
            }
            $user_data['status'] = $user->status;
            (new SmsNotificationController)->sendSms($user->country_code, $user->phone,'Your Verification OTP :'.$otp);
            (new MailController)->sendBasicEmail($user->email, 'Your Verification OTP :'.$otp, 'Account Verification OTP');
            $response = ['status' => true, 'msg' => 'Login Successfully', 'token' => $token, 'data' => $user_data, 'is_otp_verified' => $user->is_otp_verified, 'have_active_package' => $user_data['have_active_package']];
            return response($response, 200);
        } 
        else{ 
            $response = ['status' => false, 'msg' => 'Username or Password is invalid. Please recheck your username or password.'];
            return response($response, 422);
        } 
    }

    public function resendOTP(Request $request) {
        $otp = mt_rand(1000, 9999);
        User::whereId(Auth::user()->id)->update(['otp' => $otp, 'is_otp_verified' => false]);
        (new SmsNotificationController)->sendSms(Auth::User()->country_code, Auth::User()->phone,'Your Verification OTP :'.$otp);
        (new MailController)->sendBasicEmail(Auth::User()->email, 'Your Verification OTP :'.$otp, 'Account Verification OTP');
        $response = ['status' => true, 'msg' => 'OTP sent successfully to registered mail/phone'];
        return response($response, 200);
    }

}