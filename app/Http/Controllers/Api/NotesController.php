<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class NotesController extends Controller
{
    public function getPatientNotes() {
        $notes = Notes::where('patient_id', Auth::User()->id)->with('doctor_info')->get();
        $response = ['status' => true, 'data' => $notes, 'msg' => 'Notes List'];
        return response($response, 200);
    }

    public function getNotesByPatinetId(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'patient_id' => 'required|exists:users,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $notes = Notes::where('patient_id', $input['patient_id'])->with(['doctor_info', 'patient_info'])->get();
        $response = ['status' => true, 'data' => $notes, 'msg' => 'Notes List by other Doctors'];
        return response($response, 200);
    }

    public function saveNotes(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'patient_id' => 'required|exists:users,id',
            'title' => 'required|max:150',
            'description' => 'required|max:255',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $created_notes = Notes::create([
            'patient_id' => $input['patient_id'],
            'title' => $input['title'],
            'description' => $input['description'],
            'doctor_id' => Auth::user()->id,
        ]);

        if($created_notes) {
            $response = ['status' => true, 'data' => $created_notes, 'msg' => 'Note added successfully.'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong. Unable to add note.'];
            return response($response, 500);
        }
    }

}