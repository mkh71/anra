<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\DoctorTaskCategory;
use App\Models\TaskCateg;
use App\Models\TaskInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class TaskController extends Controller
{
    
    /**
     * List doctors
     *
     * @return \Illuminate\Http\Response
     */
    public function getTaskCategories() {
        $assigned_task_categ_id = DoctorTaskCategory::where('doc_id', auth()->user()->id)->pluck('task_categ_id')->toArray();
        $task_categ = TaskCateg::whereIn('id', $assigned_task_categ_id)->where('status', true)->whereNull('parent_id')->get();
        $response = ['status' => true, 'data' => $task_categ, 'msg' => 'Task Categories'];
        return response($response, 200);
    }

    public function getCategSubCateg($categ_id) {
        if(!DoctorTaskCategory::where('task_categ_id', $categ_id)->exists()) {
            $response = ['status' => false, 'msg' => 'Sorry ! You are not authorize to edit this task.'];
            return response($response, 422);
        }

        $sub_categ = TaskCateg::where('parent_id', $categ_id)->where('status', true)->get();
        $response = ['status' => true, 'data' => $sub_categ, 'msg' => 'Task Categories'];
        return response($response, 200);
    }

    public function getTaskByCategId($task_id) {
        $task_by_categ = Task::where('task_categ_id', $task_id)->where('status', true)->with('categ_info')->with('added_by_info')->get();
        $response = ['status' => true, 'data' => $task_by_categ, 'msg' => 'Task By Categories'];
        return response($response, 200);
    }

    public function storeTask(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            "task_categ_id" => "required|exists:task_categories,id",
            "short_description" => 'required',
            'status' => 'required|in:0,1',
            'file_type' => 'sometimes|required|array|min:1',
            'file_path' => 'sometimes|required|array|min:1',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $created_task = Task::create([
            'task_categ_id' => $input['task_categ_id'],
            'short_description' => $input['short_description'],
            'status' => $input['status'],
            'file_path' => $input['file_path']??null,
            'file_type' => $input['file_type']??null,
            'added_by' => auth()->user()->id,
        ]);

        if($created_task) {
            $response = ['status' => true, 'data' => $created_task, 'msg' => 'Created Task'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 500);
        }
    }

    public function assignTask(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            "task_id" => "required|exists:task,id",
            "task_categ_id" => "required|exists:task_categories,id",
            "assign_to" => 'required|exists:users,id',
            'start_date' => 'sometimes|required|date_format:Y-m-d|after:yesterday',
            'end_date' => 'sometimes|required|date_format:Y-m-d|after_or_equal:start_date',
            'frequency' => 'required|in:daily,every_other_day,every_two_days,weekly,monthly,every_two_months',
            'duration' => 'required|numeric',
            'reminder_time' => 'sometimes|required|array|min:1',
            'file_path' => 'sometimes|required',
            'file_type' => 'sometimes|required|in:pdf,image,ppt,video,audio',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $created_task_info = TaskInfo::create([
            'task_id' => $input['task_id'],
            'task_categ_id' => $input['task_id'],
            'assign_to' => $input['assign_to'],
            'assign_by' => auth()->user()->id,
            'start_date' => $input['start_date']??null,
            'end_date' => $input['end_date']??null,
            'frequency' => $input['frequency']??null,
            'duration' => $input['duration']??null,
            'reminder_time' => $input['reminder_time']??null,
            'file_path' => $input['file_path']??null,
            'file_type' => $input['file_type']??null,
            'status' => 'upcomming',
        ]);

        if($created_task_info) {
            $response = ['status' => true, 'msg' => 'Task Assigned successfully'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 500);
        }

    }

    public function getPatientTasks(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            "type" => "required|in:upcomming,completed,ongoing",
        ]);

        $taskes = TaskInfo::where('assign_to', auth()->user()->id)->where('status', $input['type'])->with(['assign_by_info', 'task_details.categ_info'])->get();
        // $taskes = $taskes->groupBy('status');
        $response = ['status' => true, 'msg' => 'Tasks', 'data' => $taskes];
        return response($response, 200);
    }

    public function getAddedTasksByDoctor() {
    	$added_task = Task::where('added_by', auth()->user()->id)->with('categ_info')->get();
    	$response = ['status' => true, 'msg' => 'Tasks', 'data' => $added_task];
        return response($response, 200);
    }

    public function scheduleTask(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            "assigned_task_id" => "required|exists:task_info,id",
            'start_date' => 'sometimes|required|date_format:Y-m-d|after:yesterday',
            'end_date' => 'sometimes|required|date_format:Y-m-d|after_or_equal:start_date',
            'frequency' => 'required|in:daily,every_other_day,every_two_days,weekly,monthly,every_two_months',
            'reminder_time' => 'sometimes|required|array|min:1',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        if(!TaskInfo::whereId($input['assigned_task_id'])->exists()) {
            $response = ['status' => false, 'msg' => 'Sorry ! You are not authorize to edit this task.'];
            return response($response, 422);
        }

        $created_task_info = TaskInfo::whereId($input['assigned_task_id'])->update([
            'start_date' => $input['start_date']??null,
            'end_date' => $input['end_date']??null,
            'frequency' => $input['frequency']??null,
            'reminder_time' => $input['reminder_time']??null,
        ]);

        $response = ['status' => true, 'msg' => 'Task Updated successfully'];
        return response($response, 200);
    }

    public function getTaskByPatientId($patient_id) {
        $taskes = TaskInfo::where('assign_to', $patient_id)->with(['assign_by_info', 'task_details.categ_info'])->get();
        $response = ['status' => true, 'msg' => 'Tasks', 'data' => $taskes];
        return response($response, 200);
    }

    public function updateTaskStatus($assigned_task_id, Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'status' => 'required|completed',
        ]);

        if(!TaskInfo::whereId($assigned_task_id)->exists()) {
            $response = ['status' => false, 'msg' => 'Sorry ! You are not authorize to edit this task.'];
            return response($response, 422);
        }

        TaskInfo::whereId($assigned_task_id)->update([
            'status' => $input['status']
        ]);

        $response = ['status' => true, 'msg' => 'Task Status Updated successfully'];
        return response($response, 200);
    }

}