<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\PushNotificationController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\ChatRoom;
use App\Models\User;
use App\Models\Message;
use App\Models\Receiver;
// use App\Models\UserRelation;
use Carbon\Carbon;
use Validator;
use Auth;

class ChatRoomController extends Controller
{

    public static function date_compare($element1, $element2) {
        if(isset($element1['last_message']['created_at']) && isset($element2['last_message']['created_at'])) {
            $datetime1 = strtotime($element1['last_message']['created_at']);
            $datetime2 = strtotime($element2['last_message']['created_at']);
            return $datetime1 - $datetime2;
        } else {
            return null;
        }
    }

    public function getChatGroups(Request $request) {
        $chat_rooms = ChatRoom::where('user_ids', 'like', '%'.Auth::user()->id.'%')->orWhere('created_by', Auth::user()->id)->with(['last_message' => function($query) {
                $query->select('chat_room_id', 'created_at', 'message');
            }])->select('id', 'name', 'image', 'created_by', 'room_type', 'user_ids')->orderBy('last_msg_on', 'desc')->get();

        $total_unread_message_count = 0;
        $total_public_unread_message_count = 0;
        $total_private_unread_message_count = 0;


        if(!empty($chat_rooms)) {
            $chat_rooms = $chat_rooms->toArray();
            usort($chat_rooms, array($this, "date_compare"));
            $chat_rooms = array_slice(array_reverse($chat_rooms), 0, 25);

            foreach ($chat_rooms as $key => $chat_room) {
                $users_list = User::whereIn('id', $chat_room['user_ids'])->select('id', 'name')->get()->toArray();
                $chat_rooms[$key]['group_users'] = $users_list;
                $chat_rooms[$key]['unread_message_count'] = $this->getChatRoomUnreadMessageCount($chat_room['id'], auth()->user()->id);
                if($chat_room['room_type'] === 'group') {
                    $total_public_unread_message_count = $chat_rooms[$key]['unread_message_count'] + $total_public_unread_message_count;
                }
                if($chat_room['room_type'] === 'private') {
                    $total_private_unread_message_count = $chat_rooms[$key]['unread_message_count'] + $total_private_unread_message_count;
                }
                $total_unread_message_count = $chat_rooms[$key]['unread_message_count'] + $total_unread_message_count;
            }
        }

        $data['chat_group'] = $chat_rooms;
        $data['total_unread_message'] = $total_unread_message_count;
        $data['group_unread_message'] = $total_public_unread_message_count;
        $data['private_unread_message'] = $total_private_unread_message_count;
        $response = ['status' => true, 'data' => $chat_rooms, 'msg' => 'Chat Rooms List'];
        return response($response, 200);
    }


    public function getChatRoomUnreadMessageCount($chat_room_id, $user_id) {
        return $group_unread_message_count = Message::where('chat_room_id', $chat_room_id)->whereHas('receivers' , function($query) use($user_id){
                $query->where('receiver_id', $user_id);
                $query->whereNull('read_at');
        })->get()->count();
    }

	public function getChatGroupsById(Request $request) {
		$input = $request->all();
		$user = User::where(['id' => $input['patient_id']])->first();

		$chat_rooms = ChatRoom::whereRaw("find_in_set('".$user->id."', user_ids)")->orWhere('created_by', $user->id)->where('room_type', 'group')->with(['last_message' => function($query) {
                $query->select('chat_room_id', 'created_at', 'message');
            }])->select('id', 'name', 'image', 'room_type', 'user_ids')->get();

        if(!empty($chat_rooms)) {
            $chat_rooms = $chat_rooms->toArray();
            usort($chat_rooms, array($this, "date_compare"));
            $chat_rooms = array_reverse($chat_rooms);

            foreach ($chat_rooms as $key => $chat_room) {
                $user_id_array = explode(',', $chat_room['user_ids']);
                $users_list = User::whereIn('id', $user_id_array)->select('id', 'name', 'role_id')->with('doc_staff')->get()->toArray();
                $chat_rooms[$key]['group_users'] = $users_list;
            }
        }
        (new AnalyticsController)->saveAnalyticsData('chat_group_by_id');
		$data['status'] = 0;
        $data['msg'] = 'success';
        $data['chat_group'] = $chat_rooms;
        return response()->json($data);
    }

    public function createChatGroup(Request $request) {
    	$input = $request->all();
		$validator = Validator::make($request->all(), [
            'name' => 'required|unique:chat_rooms',
            'user_ids' => 'required|array|min:1',
            'user_ids.*' => 'exists:users,id',
            'room_type' => 'required|in:group,private',
        ]);

        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);
        }

        if(count($input['user_ids']) === 2 && $input['room_type'] === 'group') {
            $response = ['status' => false, 'error' => 'Group should have greater then 2 participants.', 'msg' => 'Invalid request parameters'];
            return response($response, 422);
        }

        array_push($input['user_ids'], auth()->user()->id);

        $created_chatroom = $this->createChatRoomGroup($input['name'], $input['user_ids'], $input['room_type']);
        if($created_chatroom) {
			$response = ['status' => true, 'data' => $created_chatroom, 'msg' => 'Chat Room Created successfully.'];
            return response($response, 200);
        } else {
        	$response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 500);
        }
    }


    public function sendFirstAutoMessageToGroup($room_id, $created_by, $room_name, $user_name) {
        $reciver_id = ChatRoom::whereId($room_id)->pluck('user_ids')->first();
        $receiver_ids = explode(',', $reciver_id);

        $message = new Message;
        $message->chat_room_id = $room_id;
        $message->sender_id = $created_by;
        $message->message = 'Welcome to '.$room_name.' group.';
        $message->file_url = null;
        $message->file_type = null;
        $message->message_date = Carbon::now();
        $message->save();

        $pat_array = array();
        $doc_array = array();

        foreach($receiver_ids as $receiver_id) {
            $reciver_role = User::whereId($receiver_id)->pluck('role_id')->first();
            if($reciver_role === self::$PATIENT_ROLE) {
                array_push($pat_array, $receiver_id);
            } else {
                array_push($doc_array, $receiver_id);
            }

            $receiver = new Receiver;
            $receiver->message_id = $message->id;
            $receiver->receiver_id = $receiver_id;
            if($receiver->receiver_id === auth()->user()->id) {
                $receiver->read_at = Carbon::now();
            }
            $receiver->save();
        }

        if(!empty($pat_array) && !empty($doc_array)) {
            foreach ($pat_array as $pat) {
                foreach ($doc_array as $doc) {
                    if(!UserRelation::where('pat_id', $pat)->where('user_id', $doc)->exists()) {
                        UserRelation::create([
                            'pat_id' => $pat,
                            'user_id' => $doc,
                        ]);
                    }
                }
            }
        }

        $message = Message::with('sender')->find($message->id);
        if($message) {
            (new AnalyticsController)->saveAnalyticsData('send_message');
            $data['status'] = 0 ;
            $data['message'] = $message;
            $data['msg'] = 'Message sent successfully.';
            $event = config('enums.notification_types.CHAT_MSG');
            $customData = ["room_id"=> $room_id, 'sender_name' => $user_name];
            foreach($receiver_ids as $receiver_id) {
                NotificationController::triggerPushNotification($event, $receiver_id, $customData);
            }
        } else {
            $data['status'] = 1 ;
            $data['msg'] = 'Something went wrong.';
        }
        return response()->json($data);
    }

    public function getChatGroupDetail(Request $request) {
    	$input = $request->all();
		$validator = Validator::make($request->all(), [
            'group_id' => 'required',
        ]);
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);
        }

        $group_detail = ChatRoom::whereId($input['group_id'])->get()->first();
        if($group_detail) {
        	$user_list = User::whereIn('id', $group_detail->user_ids)->select('name', 'id', 'profile_photo')->get()->toArray();
        	$data = $group_detail;
        	$data['group_users'] = $user_list;
            $response = ['status' => true, 'data' => $data, 'msg' => 'Group Details'];
            return response($response, 200);
        } else {
        	$response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 200);
        }
    }

    public function addUserToGroup(Request $request) {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|exists:chat_rooms,id',
            'user_ids' => 'required|array|min:1',
            'user_ids.*' => 'exists:users,id',
            'type' => 'required|in:add,remove',
        ]);

        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);
        }

        if(in_array(Auth::User()->id, $input['user_ids'])) {
            $data['status'] = false;
            $data['msg'] = 'Sorry ! You can not '.$input['type'].' yourself.';
            return response($data, 422);
        }

        foreach ($input['user_ids'] as $key => $user_id) {
            $result = $this->updateUserInChatRoom($input['group_id'], $user_id, $input['type']);
        }
        if($result['status']) {
            $data['status'] = true;
            $data['msg'] = $result['msg'];
            return response($data, 200);
        } else {
            $data['status'] = false;
            $data['msg'] = $result['msg'];
            return response($data, 422);
        }
    }

    public function updateUserInChatRoom($group_id, $user_id, $type) {
        $chat_room_detail = ChatRoom::whereId($group_id)->get()->first();
        if($type === 'add' ) {
            $check_user = array_search($user_id, $chat_room_detail->user_ids);
            if(false === $check_user) {
                $updated_user_ids = $chat_room_detail->user_ids;
                array_push($updated_user_ids, $user_id);
                if(ChatRoom::where('id', $group_id)->update(['user_ids' => $updated_user_ids])) {
                    // $this->updateUserRelation($group_id); /*User Relation for future */
                    $data['status'] = true;
                    $data['msg'] =  'User added successfully to the group.';
                } else {
                    $data['status'] = false;
                    $data['msg'] =  'Sorry ! Unable to add user into the group.';
                }
            } else {
                $data['status'] = false;
                $data['msg'] =  'User is already in this group';
            }
        } else {
            $user_id_array = $chat_room_detail->user_ids;
            $check_user = array_search($user_id, $user_id_array);
            if(false !== $check_user) {
                unset($user_id_array[$check_user]);
                sort($user_id_array);
                if(ChatRoom::where('id', $group_id)->update(['user_ids' => $user_id_array])) {
                    // $this->updateUserRelation($group_id); /*User Relation for future */
                    $data['status'] = true;
                    $data['msg'] =  'User removed successfully from the group.';
                } else {
                    $data['status'] = false;
                    $data['msg'] =  'Sorry ! Unable to remove user from the group.';
                }
            } else {
                $data['status'] = false;
                $data['msg'] =  'Sorry ! We are unable to find user in this group.';
            }
        }
        return $data;
    }

    public function updateUserRelation($group_id) {
        $reciver_id = ChatRoom::whereId($group_id)->pluck('user_ids')->first();
        $receiver_ids = explode(',', $reciver_id);

        $pat_array = array();
        $doc_array = array();

        foreach($receiver_ids as $receiver_id) {
            $reciver_role = User::whereId($receiver_id)->pluck('role_id')->first();
            if($reciver_role === self::$PATIENT_ROLE) {
                array_push($pat_array, $receiver_id);
            } else {
                array_push($doc_array, $receiver_id);
            }
        }

        if(!empty($pat_array) && !empty($doc_array)) {
            foreach ($pat_array as $pat) {
                foreach ($doc_array as $doc) {
                    if(!UserRelation::where('pat_id', $pat)->where('user_id', $doc)->exists()) {
                        UserRelation::create([
                            'pat_id' => $pat,
                            'user_id' => $doc,
                        ]);
                    }
                }
            }
        }
    }

    public function updateAllPatientLinkToDoctor() {
        $chat_rooms = ChatRoom::all();
        foreach ($chat_rooms as $chat_room) {
            $this->updateUserRelation($chat_room->id);
        }
    }

    public function getGroupRoomMessages(ChatRoom $chatroom, Request $request) {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|exists:chat_rooms,id',
        ]);
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);
        }

        $chatroom = ChatRoom::where('id', $input['group_id'])->get()->first();
        $receiver_ids = $chatroom->user_ids;
        $check_user = array_search(Auth::User()->id, $receiver_ids);
        if(false !== $check_user) {
            $data['status'] = true;
            $data['msg'] = 'Group Messages';
            $data['data'] = array_reverse($chatroom->messages->toArray());
            $this->updateReadAtTimeForReciver($chatroom->messages->toArray(), Auth::User()->id);
            return response($data, 200);
        } else {
            $data['status'] = false;
            $data['msg'] =  'Sorry ! You are not authorize to see this chat.';
            return response($data, 403);
        }
    }

    public function updateReadAtTimeForReciver($messages_array, $reciver_id) {
        if(!empty($messages_array)) {
            foreach ($messages_array as $message) {
                Receiver::where('message_id', $message['id'])->where('receiver_id', $reciver_id)->update(['read_at' => Carbon::now()]);
            }
        }
    }

    public function sendMessage(Request $request) {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|exists:chat_rooms,id',
            'message_type' => 'required|in:text,audio,video,image',
            'files' => 'required_if:message_type,audio|required_if:message_type,video|required_if:message_type,image|array|min:1',
            'message_date_time' => 'required',
            'message' => 'required_if:message_type,==,text|max:255',
        ]);
        if ($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        ChatRoom::whereId($input['group_id'])->update(['last_msg_on' => Carbon::now()]);

        $receiver_ids = ChatRoom::whereId($input['group_id'])->pluck('user_ids')->first();

        $message = new Message;
        $message->chat_room_id = $input['group_id'];
        $message->sender_id = Auth::User()->id;
        $message->parent_msg_id = $input['parent_msg_id']??null;
        $message->message = $input['message']??null;
        $message->file_url = $input['files']??null;
        $message->file_type = $input['message_type']??null;
        $message->message_date = $input['message_date_time'];
        $message->save();

        foreach($receiver_ids as $receiver_id) {
            $receiver = new Receiver;
            $receiver->message_id = $message->id;
            $receiver->receiver_id = $receiver_id;
            if($receiver->receiver_id === auth()->user()->id) {
                $receiver->read_at = Carbon::now();
            } else {
                $custom_data = ["group_id" => $input['group_id'], 'sender_name' => auth()->user()->name];
                if($receiver_id != auth()->user()->id) {
                    $user_info = User::whereId($receiver_id)->select('id', 'device_type', 'device_token')->first();
                    $notification_details = (new PushNotificationController)->sendAndroidPushNotification($user_info->device_type, $user_info->device_token, 'New Message from '.auth()->user()->name, 'New Message', $custom_data);
                }
            }
            $receiver->save();
        }

        $message = Message::with('sender')->find($message->id);
        if($message) {
            $data['sender_name'] = Auth::User()->name;
            $data['group_id'] = $input['group_id'];
            $data['message_date_time'] = $input['message_date_time'];
            $response = ['status' => true, 'msg' => 'Message Sent successfully.', 'data' => $data];
            return response($response, 200);
            // foreach($receiver_ids as $receiver_id) {
            //     NotificationController::triggerPushNotification($event, $receiver_id, $customData);
            // }
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 500);
        }

    }

    public function updateGroupImage(Request $request) {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|exists:chat_rooms,id',
            'image' => 'required|string',
        ]);
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);
        }

        if(ChatRoom::whereId($input['group_id'])->update(['image' => $input['image']])) {
            $response = ['status' => true, 'msg' => 'Image updated successfully.'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 500);
        }

    }

    public function checkChatRoomGroup(Request $request) {
        $user = $request->user();
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required',
            'doctor_id' => 'required'
        ]);
        if ($validator->fails()) {
            $data = [
                'status'     => "fail",
                'success'     => 0,
                'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                'msg'  => $validator->errors()->all()[0],
                'messages'  => $validator->errors()->all()
            ];
            return response()->json($data);
        }
        $user_id1 = $input['patient_id'].','.$input['doctor_id'];
        $user_id2 = $input['doctor_id'].','.$input['patient_id'];

        if(isset($input['room_type']) && null != $input['room_type']) {
            $chat_room = ChatRoom::where('created_by', $input['patient_id'])->where('room_type', $input['room_type'])->where('user_ids', $input['doctor_id'])->first();
        } else {
            $chat_room = ChatRoom::where('created_by', $input['patient_id'])->where('user_ids', $input['doctor_id'])->first();
        }

        if(empty($chat_room)) {
            if(isset($input['room_type']) && null != $input['room_type']) {
                $chat_room = ChatRoom::where('created_by', $input['doctor_id'])->where('room_type', $input['room_type'])->where('user_ids', $input['patient_id'])->first();
            } else {
                $chat_room = ChatRoom::where('created_by', $input['doctor_id'])->where('user_ids', $input['patient_id'])->first();
            }

            if(empty($chat_room)) {
                if(isset($input['room_type']) && null != $input['room_type']) {
                    $chat_room = ChatRoom::where('user_ids', $user_id1)->where('room_type', $input['room_type'])->orWhere('user_ids', $user_id2)->first();
                } else {
                    $chat_room = ChatRoom::where('user_ids', $user_id1)->orWhere('user_ids', $user_id2)->first();
                }
            }

        }
        (new AnalyticsController)->saveAnalyticsData('check_chat_room_group');
        //(new AnalyticsController)->saveAnalyticsData('check_chat_room_group');
        $data['status'] = 0;
        $data['chat_room'] = $chat_room;
        return response()->json($data);
    }

    public function createChatRoomGroup($chatroom_name, $user_ids, $room_type) {
        $chat_room['name'] = $chatroom_name;
        $chat_room['room_type'] = $room_type;
        $chat_room['user_ids'] = $user_ids;
        $chat_room['created_by'] = Auth::User()->id??null;
        return $created_chatroom = ChatRoom::create($chat_room);
    }
}
