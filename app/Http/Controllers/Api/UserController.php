<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\MailController;
use App\Http\Controllers\SmsNotificationController;
use App\Http\Controllers\Api\ChatRoomController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use App\Models\UserAddress;
use App\Models\Tip;
use App\Models\User;
use App\Models\ChatRoom;
use Laravel\Passport\HasApiTokens;
use App\Models\UserRelation;

class UserController extends Controller
{

    public function getLogout(Request $request) {
        User::whereId(auth()->user()->id)->update(['is_otp_verified' => false, 'device_token' => null, 'voip_token' => null]);
        $request->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        $response = ['status' => true, 'msg' => 'Logout successfully.'];
        return response($response, 200);
    }

    public function profileData(Request $request) {
        $user_data['name'] = Auth::user()->name;
        $user_data['email'] = Auth::user()->email;
        $user_data['phone'] = Auth::user()->phone;
        $user_data['country_code'] = Auth::user()->country_code;
        $user_data['dob'] = Auth::user()->dob;
        $user_data['status'] = Auth::user()->status;

        $response = ['status' => true, 'msg' => 'User data fetched successfully.', 'data' => $user_data];
        return response($response, 200);
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|string|min:3|max:255',
            'email' => 'required|email|unique:users,email',
            'country_code' => 'required',
            'phone' => 'required|numeric|unique:users,phone|digits:10',
            'password' => 'required|max:255',
            'device_token' => 'required',
            'dob' => 'sometimes|date_format:Y-m-d|before:today',
            'device_type' => 'required|in:android,ios',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }
        $input['password'] = bcrypt($input['password']);
        $input['otp'] = mt_rand(1000, 9999);

        $user = User::create($input);
        $user->assignRole('Patient');
        $token = $user->createToken('Anra')->accessToken;

        (new MailController)->sendBasicEmail($input['email'], 'Your Verification OTP :'.$input['otp'], 'Account Verification OTP');

        (new SmsNotificationController)->sendSms($input['country_code'], $input['phone'],'Your Verification OTP :'.$input['otp']);

        /*Need to move in another function*/
        $system_nurse = User::role("System Nurse")->where('status', true)->first();
        UserRelation::create([
            'patient_id' => $user->id,
            'doctor_id' => $system_nurse->id,
        ]);

        (new ChatRoomController)->createChatRoomGroup('Support-'.$input['name'], array($user->id, $system_nurse->id), 'private');

        $response = ['status' => true, 'msg' => 'Account registered successfully. Please verify your account by OTP.', 'token' => $token, 'is_otp_verified' => 0, 'have_active_package' => 0];
        return response($response, 200);
    }

    public function verifyOtp(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'otp' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        if(Auth::User()->otp != $input['otp']) {
            $response = ['status' => false, 'msg' => 'Please enter corrent OTP. ', 'have_active_package' => Auth::User()->have_active_package];
            return response($response, 422);
        } else {
            $user = Auth::User();
            $user_data['id'] = $user->id;
            $user_data['name'] = $user->name;
            $user_data['email'] = $user->email;
            $user_data['profile_photo'] = $user->profile_photo;
            $user_data['phone'] = $user->phone;
            $user_data['dob'] = $user->dob;
            $user_data['device_type'] = $user->device_type;
            $user_data['status'] = $user->status;
            $user_data['user_type'] = Auth::user()->roles[0]->slug;
            if(!Auth::user()->hasRole('Patient')) {
                $user_data['have_active_package'] = 1;
            } else {
                $user_data['have_active_package'] = $user->have_active_package;
            }
            $user_data['status'] = $user->status;

            User::whereId(Auth::user()->id)->update(['otp' => null, 'is_otp_verified' => true]);
            $response = ['status' => true, 'msg' => 'Verification successfully', 'have_active_package' => Auth::User()->have_active_package, 'data' => $user_data];
            return response($response, 200);
        }
    }

    public function searchUserByEmailOrPhone(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email' => 'required_without_all:phone|email',
            'phone' => 'required_without_all:email|numeric',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $query = User::where('status', true);

        $query->when(request('email') != '', function ($q) use($input){
            $q->whereRaw('LOWER(`email`) LIKE ? ',[trim(strtolower($input['email'])).'%']);
        });

        $query->when(request('phone') != '', function ($q) use($input){
            $q->whereRaw('LOWER(`phone`) LIKE ? ',[trim(strtolower($input['phone'])).'%']);
        });

        $searched_users = $query->select('id', 'name', 'email', 'phone', 'profile_photo')->get();
        $response = ['status' => true, 'msg' => 'Searched Users', 'data' => $searched_users];
        return response($response, 200);
    }

    public function getPatientProfileById($patient_id) {
        $user_info = User::whereId($patient_id)->select('id', 'name', 'email', 'gender', 'country_code', 'phone', 'dob', 'profile_photo', 'device_type', 'is_otp_verified', 'have_active_package', 'status')->first();
        if($user_info) {
            $response = ['status' => true, 'msg' => 'Patient Profile', 'data' => $user_info];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Invalid Patient '];
            return response($response, 422);
        }
    }

    public function updateDeviceToken(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'device_token' => 'required',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }
        $data['device_token'] = $input['device_token'];

        if($input['device_token'] === 'ios')
            $data['voip_token'] = $input['voip_token'];

        User::whereId(Auth::User()->id)->update($data);

        $response = ['status' => true, 'msg' => 'Token Update successfully'];
        return response($response, 200);
    }

    public function getSupportUsers() {
        // if(Auth::user()->hasRole('Patient')) {
            $related_users = UserRelation::where('patient_id', Auth::User()->id)->pluck('doctor_id')->toArray();
        // } else {
        //     $related_users = UserRelation::where('doctor_id', Auth::User()->id)->pluck('patient_id')->toArray();
        // }

        $users = User::role("System Nurse")->whereIn('id', $related_users)->select('id', 'name', 'email', 'gender', 'country_code', 'phone', 'dob', 'profile_photo', 'status')->get();

        $support_users = array();
        foreach ($users as $key => $user) {
            $chat_room = ChatRoom::where('user_ids', 'like', '%'.$user->id.'%' )->where('user_ids', 'like', '%'.Auth::User()->id.'%' )->whereNull('created_by')->first();
            if($chat_room) {
                $user['group_id'] = $chat_room->id;
                array_push($support_users, $user);
            }
        }
        $response = ['status' => true, 'msg' => 'Support Users', 'data' => $support_users];
        return response($response, 200);
    }

    public function getPatientList() {
        $patients = User::role("Patient")->select('id', 'name', 'email', 'gender', 'country_code', 'phone', 'dob', 'profile_photo', 'status', 'have_active_package', 'refrence_key')->get();
        $response = ['status' => true, 'msg' => 'Patient List', 'data' => $patients];
        return response($response, 200);
    }

    public function changePassword(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'old_password' => 'required|max:255',
            'new_password' => 'required|max:255',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $email = Auth::user()->email;

        if(Auth::guard('web')->attempt(['email' => $email, 'password' => $input['old_password']])){
            User::whereId(Auth::user()->id)->update([
                'password' => bcrypt($input['new_password']),
                'is_otp_verified' => false,
                'device_token' => null,
            ]);

            $request->user()->tokens->each(function ($token, $key) {
                $token->delete();
            });

            $response = ['status' => true, 'msg' => 'Password Changed successfully. Please login again to use app.'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'We are unable to verify your old password.'];
            return response($response, 422);
        }
    }

    public function forgotPassword(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email' => 'required|email|max:255',
            'phone' => 'required|numeric|digits:10',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }
        $temp_password = 'Anra@'.mt_rand(100000, 999999);

        $user_info = User::where('email', $input['email'])->where('phone', $input['phone'])->first();

        if($user_info) {
            User::whereId($user_info->id)->update([
                'password' => bcrypt($temp_password),
                'is_otp_verified' => false,
                'device_token' => null,
            ]);

            (new SmsNotificationController)->sendSms($user_info->country_code, $user_info->phone, 'Your Temporary Password :'.$temp_password);

            (new MailController)->sendBasicEmail($user_info->email, 'Your Account Temp Password :'.$temp_password, 'Account Temp Password');
        }
        $response = ['status' => true, 'msg' => 'temporary password is sent to your registered email and mobile.'];
        return response($response, 200);
    }

    public function addUserLocation(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'street' => 'required|max:255',
            'city' => 'required|max:255',
            'province' => 'required|max:255',
            'country' => 'required|max:255',
            'address' => 'required|max:255',
            'latitude' => 'required',
            'longitude' => 'required',
            'is_default' => 'required|in:0,1',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }
        $created_patient_location = UserAddress::create([
            'street' => $input['street'],
            'city' => $input['city'],
            'province' => $input['province'],
            'country' => $input['country'],
            'address' => $input['address'],
            'user_id' => Auth::User()->id,
            'latitude' => $input['latitude'],
            'longitude' => $input['longitude'],
            'is_default' => $input['is_default'],
        ]);

        if($created_patient_location->is_default) {
            UserAddress::where('user_id', Auth::user()->id)->where('id', '!=', $created_patient_location->id)->update([
                'is_default' => false,
            ]);
        }

        $response = ['status' => true, 'msg' => 'Location Added successfully.'];
        return response($response, 200);
    }

    public function getUserLocations() {
        $pat_locations = UserAddress::where('user_id', Auth::User()->id)->get();
        $response = ['status' => true, 'msg' => 'Patient Location', 'data' => $pat_locations];
        return response($response, 200);
    }

    public function editUserLocation($location_id) {
        $pat_location = UserAddress::whereId($location_id)->where('user_id', Auth::user()->id)->first();
        if(!$pat_location) {
            $response = ['status' => false, 'msg' => 'Sorry ! You are not autorize to edit this location. Please contact with admin.'];
            return response($response, 500);
        }

        $response = ['status' => true, 'msg' => 'Location Info.', 'data' => $pat_location];
        return response($response, 200);
    }

    public function updateLocation(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'location_id' => 'required|exists:user_addresses,id',
            'street' => 'required|max:255',
            'city' => 'required|max:255',
            'province' => 'required|max:255',
            'country' => 'required|max:255',
            'address' => 'required|max:255',
            'latitude' => 'required',
            'longitude' => 'required',
            'is_default' => 'required|in:0,1',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $pat_location = UserAddress::whereId($input['location_id'])->where('user_id', Auth::user()->id)->first();
        if(!$pat_location) {
            $response = ['status' => false, 'msg' => 'Sorry ! You are not autorize to edit this location. Please contact with admin.'];
            return response($response, 500);
        }

        UserAddress::whereId($input['location_id'])->where('user_id', Auth::user()->id)->update([
            'street' => $input['street'],
            'city' => $input['city'],
            'province' => $input['province'],
            'country' => $input['country'],
            'address' => $input['address'],
            'latitude' => $input['latitude'],
            'longitude' => $input['longitude'],
            'is_default' => $input['is_default'],
        ]);
        if($input['is_default']) {
            UserAddress::where('user_id', Auth::user()->id)->where('id', '!=', $input['location_id'])->update([
                'is_default' => false,
            ]);
        }

        $response = ['status' => true, 'msg' => 'Location Updated successfully.'];
        return response($response, 200);
    }

    public function updateMyProfile(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'dob' => 'required|date_format:Y-m-d',
            'profile_photo' => 'required|max:255',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        User::whereId(Auth::User()->id)->update([
            'name' => $input['name'],
            'email' => $input['email'],
            'dob' => $input['dob'],
            'profile_photo' => $input['profile_photo'],
        ]);

        $response = ['status' => true, 'msg' => 'Profile update successfully.'];
        return response($response, 200);
    }

    public function searhGlobally(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'search_for' => 'required|min:3|max:255',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        $data['doctors'] = User::role("Doctor")->where('name', 'like', '%'.$input['search_for'].'%')->select('id', 'name', 'email', 'profile_photo', 'dob', 'status')->get();
        $data['tips'] = Tip::where('title', 'like', '%'.$input['search_for'].'%')->orWhere('description', 'like', '%'.$input['search_for'].'%')->get();
        $response = ['status' => true, 'msg' => 'Searched Data', 'data' => $data];
        return response($response, 200);
    }
}
