<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FormCateg;
use App\Models\FieldType;
use App\Models\FieldAction;
use App\Models\FormQuestion;
use App\Models\UserFormAnswer;
use App\Models\FormInfo;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class IntakeFormController extends Controller
{

	public function getIntakeForm(Request $request) {
        $input = $request->all();
        if(isset($input['form_no'])) {
            $form_no = $input['form_no'];
            $next_form = $form_no + 1;
        } else {
            $form_no = 1;
            $next_form = $form_no + 1;
        }

        $form_info_from_seq_order = FormInfo::where('seq_order', $form_no)->first();

        if(!$form_info_from_seq_order) {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 200);    
        }

		$data['field_types'] = FieldType::where('status', true)->get();
        $data['field_action'] = FieldAction::where('status', true)->get();
        $data['form'] = FormCateg::where('status', true)->where('form_id', $form_info_from_seq_order->id)->with('form_questions.field_type_info')->with('form_questions.field_action_info')->with(['form_questions.answer_info' => function($q) {
                $q->where('user_id', Auth::User()->id);
            }])->orderBy('seq_order', 'asc')->get();
        if(count($data['form']) > 0) {
            if(FormInfo::where('seq_order', $next_form)->exists()) {
                $data['form_no'] = $next_form;
            } else {
                $data['form_no'] = null;
            }
        } else {
            $data['form_no'] = null;
        }
        $response = ['status' => true, 'data' => $data, 'msg' => 'Intake Form'];
        return response($response, 200);
	}

    public function getIntakeFormStatus($patient_id = null) {
        $final_array = array();
        $no_of_form_info = FormInfo::where('status', true)->get();
        foreach ($no_of_form_info as $key => $f_info) {

            $form_information = FormCateg::where('form_id', $f_info->id)->where('status', true)->orderBy('seq_order', 'asc')->get();
            foreach ($form_information as $key => $form) {
                $form_quesitons_id = FormQuestion::where('form_categ_id', $form->id)->pluck('id')->toArray();
                $count_of_user_ans = UserFormAnswer::where('user_id', $patient_id??Auth::User()->id)->whereIn('question_id', $form_quesitons_id)->count();
                if(count($form_quesitons_id) === $count_of_user_ans) {
                    $data['is_filled'] = true;
                } else {
                    $data['is_filled'] = false;
                    break;
                }
            }
            $data['form_title'] = $f_info->name;
            $data['form_no'] = $f_info->id;
            array_push($final_array, $data);
        }
        $response = ['status' => true, 'data' => $final_array, 'msg' => 'IntakForm Status'];
        return response($response, 200);
    }

	public function saveIntakeForm(Request $request) {
		$input = $request->all();
        $validator = Validator::make($input, [
            "form_no" => "required|numeric",
            "answers" => "array|required|min:1",
            "answers.*.question_id" => 'required|exists:form_questions,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        foreach ($input['answers'] as $key => $answer) {
            if(UserFormAnswer::where('user_id', Auth::User()->id)->where('question_id', $answer['question_id'])->exists()) {
                $created_user_form = UserFormAnswer::where('user_id', Auth::User()->id)->where('question_id', $answer['question_id'])->update([
                    'user_id' => Auth::User()->id,
                    'created_by' => Auth::User()->id,
                    'question_id' => $answer['question_id'],
                    'text_answer' => $answer['text_answer']??null,
                    'options_answers' => $answer['options_answers']??null,
                    'files' => $answer['files']??null,
                ]);
            } else {
                $created_user_form = UserFormAnswer::create([
                    'user_id' => Auth::User()->id,
                    'created_by' => Auth::User()->id,
                    'question_id' => $answer['question_id'],
                    'text_answer' => $answer['text_answer']??null,
                    'options_answers' => $answer['options_answers']??null,
                    'files' => $answer['files']??null,
                ]);
            }
        }

        if($created_user_form) {
            $data['form_no'] = $input['form_no'];
		    $response = ['status' => true, 'msg' => 'Saved Successfully', 'data' => $data];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 500);
        }
	}

    public function getPatientIntakeFormByPatientId($patient_id, Request $request) {
        $patient_info = User::whereId($patient_id)->first();
        if(!$patient_info) {
            return response(['msg' => 'Invalid Patient.', 'status' => false], 422);
        }

        if(!$patient_info->status) {
            return response(['msg' => 'Patient is not active. Please contact with admin for more informatin.', 'status' => false], 422);
        }

        $input = $request->all();
        if(isset($input['form_no'])) {
            $form_no = $input['form_no'];
            $next_form = $form_no + 1;
        } else {
            $form_no = 1;
            $next_form = $form_no + 1;
        }

        $data['field_types'] = FieldType::where('status', true)->get();
        $data['field_action'] = FieldAction::where('status', true)->get();
        $data['form'] = FormCateg::where('status', true)->where('form_id', $form_no)->with('form_questions.field_type_info')->with('form_questions.field_action_info')->with(['form_questions.answer_info' => function($q) use($patient_id){
                $q->where('user_id', $patient_id);
            }])->orderBy('seq_order', 'asc')->get();
        if(count($data['form']) > 0) {
            if(FormInfo::whereId($next_form)->exists()) {
                $data['form_no'] = $next_form;
            } else {
                $data['form_no'] = null;
            }
        } else {
            $data['form_no'] = null;
        }
        $response = ['status' => true, 'data' => $data, 'msg' => 'Intake Form'];
        return response($response, 200);
    }

    public function getIntakeFormStatusByPatientId($patient_id) {
        $patient_info = User::whereId($patient_id)->first();
        if(!$patient_info) {
            return response(['msg' => 'Invalid Patient.', 'status' => false], 422);
        }

        if(!$patient_info->status) {
            return response(['msg' => 'Patient is not active. Please contact with admin for more informatin.', 'status' => false], 422);
        }

        return $this->getIntakeFormStatus($patient_id);
    }

    public function saveIntakeFormByPatientId($patient_id, Request $request) {
        $patient_info = User::whereId($patient_id)->first();
        if(!$patient_info) {
            return response(['msg' => 'Invalid Patient.', 'status' => false], 422);
        }

        if(!$patient_info->status) {
            return response(['msg' => 'Patient is not active. Please contact with admin for more informatin.', 'status' => false], 422);
        }

        $input = $request->all();
        $validator = Validator::make($input, [
            "form_no" => "required|numeric",
            "answers" => "array|required|min:1",
            "answers.*.question_id" => 'required|exists:form_questions,id',
        ]);

        if($validator->fails()) {
            return response(['msg' => $validator->errors()->all()[0], 'status' => false], 422);
        }

        foreach ($input['answers'] as $key => $answer) {
            if(UserFormAnswer::where('user_id', $patient_id)->where('question_id', $answer['question_id'])->exists()) {
                $created_user_form = UserFormAnswer::where('user_id', $patient_id)->where('question_id', $answer['question_id'])->update([
                    'user_id' => $patient_id,
                    'created_by' => Auth::User()->id,
                    'question_id' => $answer['question_id'],
                    'text_answer' => $answer['text_answer']??null,
                    'options_answers' => $answer['options_answers']??null,
                    'files' => $answer['files']??null,
                ]);
            } else {
                $created_user_form = UserFormAnswer::create([
                    'user_id' => $patient_id,
                    'created_by' => Auth::User()->id,
                    'question_id' => $answer['question_id'],
                    'text_answer' => $answer['text_answer']??null,
                    'options_answers' => $answer['options_answers']??null,
                    'files' => $answer['files']??null,
                ]);
            }
        }

        if($created_user_form) {
            $data['form_no'] = $input['form_no'];
            $response = ['status' => true, 'msg' => 'Saved Successfully', 'data' => $data];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Oops ! Something went wrong.'];
            return response($response, 500);
        }
    }

}