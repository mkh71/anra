<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\User;
use App\Models\FavDoctorPatient;
use App\Models\DoctorEducation;
use App\Models\DoctorAward;
use App\Models\DoctorWorkExperience;
use App\Models\DoctorOtherWorkLocation;
use App\Models\DoctorAvailability;
use App\Models\Appointment;
use App\Models\DoctorSpecialities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Arr;


class DoctorController extends Controller
{
    /**
     * List doctors
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'keyword' => 'sometimes|min:3|max:50',
            'order_by' => 'sometimes|in:rating'
        ]);
        
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        $query = Doctor::select('doctors.*', 'categories.name as category')
            ->join('categories', 'categories.id', 'category_id');

        $query->when(request('keyword') != '', function ($q) {
            $q->where('doctors.name', 'LIKE', '%'.request('keyword').'%')
            ->orWhere('categories.name', 'LIKE', '%'.request('keyword').'%');
        });

        $query->when(request('order_by') != '', function ($q) {
            $q->orderBy('doctors.rating');
        });

        $doctors = $query->get();
        $response = ['status' => true, 'data' => $doctors, 'msg' => 'Doctors list found'];
        return response($response, 200);
    }

    public function getDoctorByCateg($categ_id) {
        $doctor_ids = DoctorSpecialities::where('categ_id', $categ_id)->pluck('doc_id')->toArray();
        $doctors = User::role('Doctor')->whereIn('id', $doctor_ids)->where('status', true)->select('id', 'name', 'email', 'country_code', 'phone', 'profile_photo', 'status')->get();
        $response = ['status' => true, 'data' => $doctors, 'msg' => 'Doctors list found'];
        return response($response, 200);
    }

    public function getDoctorProfile($doctor_id = null) {
        $doctor_info = User::role('Doctor')->whereId($doctor_id??Auth::User()->id)->with('doctor_basic_profile')->where('status', true)->select('id', 'name', 'email', 'country_code', 'phone', 'profile_photo', 'status')->first();
        if(!$doctor_info) {
            $response = ['status' => false, 'msg' => 'Sorry ! We are unable to find this doctor.'];
            return response($response, 404);
        }

        $response = ['status' => true, 'data' => $doctor_info, 'msg' => 'Doctor Profile'];
        return response($response, 200);
    }

    public function getEducationList($doctor_id = null) {

        $doc_education_list = DoctorEducation::where('doc_id', $doctor_id??Auth::User()->id)->get();
        $response = ['status' => true, 'data' => $doc_education_list, 'msg' => 'Doctor Edu List'];
        return response($response, 200);
    }

    public function getAwardsList($doctor_id = null) {

        $doc_awards_list = DoctorAward::where('doc_id', $doctor_id??Auth::User()->id)->get();
        $response = ['status' => true, 'data' => $doc_awards_list, 'msg' => 'Doctor Edu List'];
        return response($response, 200);
    }

    public function getWorkExperienceList($doctor_id = null) {

        $doc_works = DoctorWorkExperience::where('doc_id', $doctor_id??Auth::User()->id)->get();
        $response = ['status' => true, 'data' => $doc_works, 'msg' => 'Doctor Work Experience'];
        return response($response, 200);
    }

    public function getOtherWorkLocations($doctor_id = null) {

        $doc_other_work_location = DoctorOtherWorkLocation::where('doc_id', $doctor_id??Auth::User()->id)->with('location_availability')->get();
        $response = ['status' => true, 'data' => $doc_other_work_location, 'msg' => 'Doctor Other Work Locations'];
        return response($response, 200);
    }

    public function getAvailability($doctor_id = null) {

        $doc_availability = DoctorAvailability::where('doc_id', $doctor_id??Auth::User()->id)->get();
        $response = ['status' => true, 'data' => $doc_availability, 'msg' => 'Doctor Work Experience'];
        return response($response, 200);
    }

    public function getSpecialities($doctor_id = null) {
        $doc_specialities = DoctorSpecialities::where('doc_id', $doctor_id??Auth::User()->id)->with('speciality_details')->get();
        $response = ['status' => true, 'data' => $doc_specialities, 'msg' => 'Doctor Specialities'];
        return response($response, 200);
    }

    public function markDoctorAsFavourite(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'doctor_id' => 'required|exists:users,id',
        ]);
        
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        if(FavDoctorPatient::where('doctor_id', $input['doctor_id'])->where('patient_id', auth()->user()->id)->exists()) {
            $response = ['status' => false, 'msg' => 'Doctor is already in your favourite list.'];
            return response($response, 422);
        }

        $created_fav_doctor = FavDoctorPatient::create([
            'doctor_id' => $input['doctor_id'],
            'patient_id' => auth()->user()->id,
            'created_by' => auth()->user()->id,
        ]);

        if($created_fav_doctor) {
            $response = ['status' => true, 'msg' => 'Doctor added to favourite list successfully.'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Ooops Something went worng.'];
            return response($response, 500);
        }
    }

    public function getPatientFavouriteDoctorList() {
        $fav_doc_list = FavDoctorPatient::where('created_by', auth()->user()->id)->with('fav_doc_info')->get();
        $response = ['status' => true, 'data' => $fav_doc_list, 'msg' => 'Fav Doc List with Doctor Info'];
        return response($response, 200);
    }

    public function markDoctorAsUnfav(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'doctor_id' => 'required|exists:users,id',
        ]);
        
        if ($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        if(!FavDoctorPatient::where('doctor_id', $input['doctor_id'])->where('patient_id', auth()->user()->id)->exists()) {
            $response = ['status' => false, 'msg' => 'Doctor you selected is not available in your favourite list.'];
            return response($response, 422);
        }

        FavDoctorPatient::where('doctor_id', $input['doctor_id'])->where('patient_id', auth()->user()->id)->delete();
        $response = ['status' => true, 'msg' => 'Unfavourite successfully.'];
        return response($response, 200);
    }

    public function getDoctorAvailability(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'doctor_id' => 'required|exists:users,id',
            'date' => 'required|date_format:Y-m-d',
        ]);
        
        if($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        $day_no = date('w', strtotime($input['date']));

        $doc_available_time = DoctorAvailability::where('doc_id', $input['doctor_id'])->where('day_number', $day_no)->get();

        if($doc_available_time) {
            $data['time_slot'] = env('DEFAULT_TIME_SLOT', 30);
            $data['shifts'] = $doc_available_time;
            $data['booked_slots'] = Appointment::where('doctor_id', $input['doctor_id'])->whereDate('book_on', $input['date'])->select('id', 'start_time', 'end_time')->get();
            $response = ['status' => true, 'msg' => 'Doctor Availability', 'data' => $data];
            return response($response, 200);
        } else {
            $data['status'] = false;
            $data['msg'] = 'Sorry ! No data is available.';
            return response($response, 422);
        }
    }

    public function addDoctorAvailability(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'slots' => 'required|array|min:1',
            'slots.*.day_number' => 'required|in:0,1,2,3,4,5,6',
            'slots.*.start_time' => 'required|date_format:H:i',
            'slots.*.end_time' => 'required|date_format:H:i', 
        ]);
        
        if($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        foreach ($input['slots'] as $key => $slot) {
            $created_availability = DoctorAvailability::create([
                'doc_id' => Auth::User()->id,
                'day_number' => $slot['day_number'],
                'start_time' => $slot['start_time'],
                'end_time' => $slot['end_time'],
            ]);
        }

        if($created_availability) {
            $response = ['status' => true, 'msg' => 'Doctor Availability added successfully.'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Ooops Something went worng.'];
            return response($response, 500);
        }
    }

    public function addDoctorAvailabilityByDayNumber(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'day_number' => 'required|in:0,1,2,3,4,5,6',
        ]);
        
        if($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        $doc_day_availability = DoctorAvailability::where('doc_id', Auth::User()->id)->where('day_number', $input['day_number'])->get();
        $response = ['status' => true, 'msg' => 'Doctor Availability By Day', 'data' => $doc_day_availability];
        return response($response, 200);
    }

    public function deleteDoctorAvailability(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'availability_id' => 'required|exists:doc_availability,id',
        ]);
        
        if($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        if(!DoctorAvailability::whereId($input['availability_id'])->where('doc_id', Auth::user()->id)->exists()) {
            $response = ['status' => false, 'msg' => 'You are not authorize to delete this slot.'];
            return response($response, 403); 
        } else {
            DoctorAvailability::whereId($input['availability_id'])->delete();
            $response = ['status' => true, 'msg' => 'Deleted successfully.'];
            return response($response, 200);
        }
    }

    public function updateDoctorAvailability(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'slots' => 'required|array|min:1',
            'slots.*.id' => 'required|exists:doc_availability,id',
            'slots.*.day_number' => 'required|in:0,1,2,3,4,5,6',
            'slots.*.start_time' => 'required|date_format:H:i',
            'slots.*.end_time' => 'required|date_format:H:i', 
        ]);
        
        if($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        foreach ($input['slots'] as $key => $slot) {
            $created_availability = DoctorAvailability::whereId($slot['id'])->update([
                'day_number' => $slot['day_number'],
                'start_time' => $slot['start_time'],
                'end_time' => $slot['end_time'],
            ]);
        }

        if($created_availability) {
            $response = ['status' => true, 'msg' => 'Doctor Availability updated successfully.'];
            return response($response, 200);
        } else {
            $response = ['status' => false, 'msg' => 'Ooops Something went worng.'];
            return response($response, 500);
        }
    }

    public function searchPatient(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required_without_all:email,phone,chart_no,dob|min:3|max:255',
            'email' => 'required_without_all:name,phone,chart_no,dob|email|min:3|max:255',
            'phone' => 'required_without_all:name,email,chart_no,dob|numeric',
            'chart_no' => 'required_without_all:name,email,phone,dob|min:3',
            'dob' => 'required_without_all:name,email,phone,chart_no||date_format:Y-m-d', 
        ]);
        
        if($validator->fails()) {
            $response = ['status' => false, 'error' => $validator->errors()->all()[0], 'msg' => 'Invalid request parameters'];
            return response($response, 422);  
        }

        $query = User::role('Patient')->where('status', true);

        $query->when(request('name') != '', function ($q) {
            $q->where('name', 'LIKE', '%'.request('name').'%');
        });

        $query->when(request('email') != '', function ($q) {
            $q->where('email', 'LIKE', '%'.request('email').'%');
        });

        $query->when(request('phone') != '', function ($q) {
            $q->where('phone', 'LIKE', '%'.request('phone').'%');
        });

        $query->when(request('dob') != '', function ($q) {
            $q->where('dob', 'LIKE', '%'.request('dob').'%');
        });

        $query->when(request('chart_no') != '', function ($q) {
            $q->where('refrence_key', 'LIKE', '%'.request('chart_no').'%');
        });

        $searched_patient = $query->select('name', 'email', 'gender', 'phone', 'country_code', 'phone', 'dob', 'profile_photo', 'device_type', 'have_active_package', 'refrence_key', 'status', 'id')->get();

        $response = ['status' => true, 'msg' => 'Searched Patients', 'data' => $searched_patient];
        return response($response, 200);
    }
}