<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class SettingController extends Controller
{

    public function getChangePassword() {
        return view('doctor.change-password');
    }
    
    public function getProfileSettings() {
        return view('doctor.profile-settings');
    }

    public function getScheduleTimings() {
        return view('doctor.schedule-timings');
    }
 
}