<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\{User, Appointment, DoctorProfile};
use DB;
use Carbon\Carbon;

class DashboardController extends Controller
{

    public function getDashboard() {
        $records = [
            "doctor_count"  =>  User::role("Doctor")->count(),
            "patient_count" => User::role("Patient")->count(),
            "appointment_count" => Appointment::count(),
            "doctors" => User::role('Doctor')
                ->select('users.name', 'users.id', 'users.profile_photo', 'users.email')
                ->withCount('appointment')
                ->withCount('patient')
                ->with(['specialities' => function($query) {
                    $query->select('doctor_specialities.categ_id', 'categories.name', 'doctor_specialities.doc_id')
                    ->join('categories', 'categories.id', 'doctor_specialities.categ_id')
                    ->get();
                }])
                ->orderBy('created_at')
                ->limit(5)
                ->get(),
            "patients" => User::role("Patient")
                ->select('users.name', 'users.phone', 'users.profile_photo', 'users.created_at', 'users.id')
                ->orderBy('created_at')
                ->limit(5)
                ->get(),
            "appointments" => Appointment::select('users.id', 'users.name', 'users.profile_photo', 'appointment.book_on', 'appointment.start_time', 'appointment.end_time', 'appointment.type', 'appointment.status', 'doctors.name as doctor_name', 'doctors.profile_photo as doc_profile_photo','appointment.created_at', 'appointment.doctor_id')
                ->join('users', 'users.id', 'appointment.patient_id')
                ->join('users as doctors', 'doctors.id', 'appointment.doctor_id')
                ->get(),
        ];

        // echo "<pre>";
        // print_r($records);
        // die;
        return view('admin.dashboard', \compact('records'));
    }
    
 
}