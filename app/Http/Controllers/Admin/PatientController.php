<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreatePatient;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    User,
    UserAddress
};

class PatientController extends Controller
{

    public function getPatients() {
        $patients = User::role("Patient")
            ->with('lastAppointment')
            ->select('users.name', 'users.dob', 'users.phone', 'users.profile_photo', 'users.created_at', 'users.id', 'users.refrence_key')
            ->orderBy('created_at')
            ->get()
            ->toArray();
        return view('admin.patient.list', \compact('patients'));
    }

    public function getCreateForm() {
        return view('admin.patient.create');
    }

    public function createPatient(CreatePatient $request)
    {
        $input = $request->validated();

        $user = User::create([
			"email" => $input["email"],
            "name" => $input["name"],
            "phone" => $input["phone"],
            "gender" => $input["gender"],
            "dob" => $input["dob"],
            "country_code" => $input["country_code"],
            "password" => \bcrypt($input["password"]),
		]);
		$user->assignRole('Patient');

        UserAddress::create([
            "user_id" => $user->id,
            "street" => $input['address'],
            "city" => $input['city'],
            "country" => $input['country'],
            "province" => $input['province'],
            "country" => $input['country'],
        ]);

        return back()->with('message','Profile has been created.');
    }
 	
 	public function getPatientProfile($patient_id) {
        $patient = User::select('users.name', 'users.dob', 'users.phone', 'users.profile_photo', 'users.created_at', 'users.id', 'users.refrence_key', 'users.gender')
            ->with(['lastAppointment.doctor_info', 'lastAppointment.specialities.speciality_details', 'patientAppointment.doctor_info', 'patientAppointment.specialities.speciality_details'])
            ->where('id', $patient_id)
            ->orderBy('created_at')
            ->get()
            ->toArray();
 		return view('admin.patient.profile', compact('patient'));
 	}


    public function showPatientEdit($id)
    {
        $patient = User::select('users.email', 'users.name', 'users.dob', 'users.country_code', 'users.phone', 'users.profile_photo', 'users.created_at', 'users.id', 'users.refrence_key', 'users.gender')
            ->with(['user_address'])
            ->where('id', $id)
            ->get()
            ->toArray();
        return view('admin.patient.update', compact('patient'));
    }

    public function updatePatient($id, Request $request)
    {
        $input = $request->all();
        $user = User::where('id', $id)
        ->update([
			"email" => $input["email"],
            "name" => $input["name"],
            "phone" => $input["phone"],
            "gender" => $input["gender"],
            "dob" => $input["dob"],
            "country_code" => $input["country_code"],
		]);

        $user_address = UserAddress::where('user_id', $id)->exists();
        if ( $user_address) {
            UserAddress::where('user_id', $id)
            ->update([
                "address_line" => $input['address'],
                "street" => $input['street'],
                "state" => $input['state'],
                "city" => $input['city'],
                "country" => $input['country'],
                "province" => $input['province'],
                "country" => $input['country'],
            ]);
            
            return back()->with('message','Profile has been updated.');
        }

        UserAddress::create([
            "user_id" => $id,
            "address_line" => $input['address'],
            "street" => $input['street'],
            "state" => $input['state'],
            "city" => $input['city'],
            "country" => $input['country'],
            "province" => $input['province'],
            "country" => $input['country'],
        ]);
        return back()->with('message','Profile has been updated.');
    }
}