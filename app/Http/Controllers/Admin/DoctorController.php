<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Api\ChatRoomController;
use Illuminate\Http\Request;
use Auth;
use App\Models\{
	Appointment,
	Category,
	User,
	DoctorProfile,
	DoctorEducation,
	DoctorWorkExperience,
	DoctorAward,
    UserRelation,
	Role,
    Doctor,
    DoctorSpecialities,
};
use App\Http\Requests\{
    CreateDoctor,
};
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UpdateProfileSetting;

class DoctorController extends Controller
{

    public function getDoctors() {
		$doctors = User::role("Doctor")->get();
        return view('admin.doctor.list', \compact('doctors'));
    }
 	
 	public function getDoctorProfile($doctor_id) {
        $profile = (new Doctor)->profileInfo($doctor_id)->toArray();
        return view('admin.doctor.profile', compact('profile'));
 	}

    public function showEditProfile($doctor_id) {
        $categories = Category::all();
        $doctor_profile = (new Doctor)->profileInfo($doctor_id)->toArray();
        $selected = [];
        foreach($doctor_profile[0]["specialities"] as $specail) {
            $selected[] = $specail["categ_id"];
        }
        return view('admin.doctor.update', compact('doctor_profile', 'categories', 'selected'));

    }

 	public function getCreateForm() {
 		$categories = Category::all();
 		return view('admin.doctor.create', compact('categories'));
	 }
	 
	public function updateProfileSettings(CreateDoctor $request) {
		$input = $request->all();
        $temp_password = 'Anra@'.mt_rand(100000, 999999);

        $user = User::create([
			"email" => $input["email"],
            "name" => $input["name"],
            "phone" => $input["phone"],
            "gender" => $input["gender"],
            "dob" => $input["dob"],
            'password' => bcrypt($temp_password),
		]);
		$user->assignRole('Doctor');

        $p = DoctorProfile::create([
			"doctor_id" => $user->id,
            "about_me" => $input["about_me"],
            "address" => $input["address"],
            "city" => $input["city"],
            "state" => $input["state"],
            "country" => $input["country"],
            "postal_code" => $input["postal_code"],
        ]);

        $education = [];
        $counter = 0;
        foreach($input['degree'] as $degree) {
            $temp_education = [
                "id" => (string) Str::uuid(),
                "doc_id" => $user->id,
                "degree" => $degree,
                "college" => $input['college'][$counter],
                "starting_year" => $input['starting_year'][$counter],
                "completion_year" => $input['completion_year'][$counter],
            ];
            array_push($education, $temp_education);
            $counter++;
        }
        DoctorEducation::insert($education);

        $experience = [];
        $counter = 0;
        foreach($input['hospital_name'] as $name) {
            $temp_experience = [
                "id" => (string) Str::uuid(),
                "doc_id" => $user->id,
                "hospital_name" => $name,
                "started_from" => $input['started_from'][$counter],
                "end_to" => $input['end_to'][$counter],
                "designation" => $input['designation'][$counter],
            ];
            array_push($experience, $temp_experience);
            $counter++;
        }
        DoctorWorkExperience::insert($experience);

        $awards = [];
        $counter = 0;
        foreach($input['award_name'] as $award) {
            $temp_award = [
                "doc_id" => $user->id,
                "award_name" => $award,
                "award_year" => $input['award_year'][$counter],
            ];
            array_push($awards, $temp_award);
            $counter++;
        }
        DoctorAward::insert($awards);

        $system_nurse = User::role("System Nurse")->where('status', true)->first();
        UserRelation::create([
            'patient_id' => $user->id,
            'doctor_id' => $system_nurse->id,
        ]);

        (new ChatRoomController)->createChatRoomGroup('Support-Nurse', array($user->id, $system_nurse->id), 'private');
        (new MailController)->sendBasicEmail($input["email"], 'Your Account Temp Password :'.$temp_password, 'Account Temp Password');
        return back()->with('message','Profile has been created.');
    }


    public function updateDoctorProfileSettings(Request $request) {
        
        $input = $request->all();
        User::where('id', $input['id'])->update([
            "name" => $input["name"],
            "phone" => $input["phone"],
            "gender" => $input["gender"],
            "dob" => $input["dob"],
            "email" => $input["email"],
        ]);


        DoctorProfile::where('doctor_id', $input['id'])->update([
            "about_me" => $input["about_me"],
            "address" => $input["address"],
            "city" => $input["city"],
            "state" => $input["state"],
            "country" => $input["country"],
            "postal_code" => $input["postal_code"],
        ]);

        DoctorSpecialities::where("doc_id", $input['id'])->delete();
        DoctorSpecialities::create([
            "doc_id" => $input["id"],
            "categ_id" => $input["specialization"],
        ]);

        
        DoctorEducation::where("doc_id", $input['id'])->delete();
        $education = [];
        $counter = 0;
        foreach($input['degree'] as $degree) {
            $temp_education = [
                "id" => (string) Str::uuid(),
                "doc_id" => $input['id'],
                "degree" => $degree,
                "college" => $input['college'][$counter],
                "starting_year" => $input['starting_year'][$counter],
                "completion_year" => $input['completion_year'][$counter],
            ];
            array_push($education, $temp_education);
            $counter++;
        }
        DoctorEducation::insert($education);

        DoctorWorkExperience::where("doc_id", $input['id'])->delete();
        $experience = [];
        $counter = 0;
        foreach($input['hospital_name'] as $name) {
            $temp_experience = [
                "id" => (string) Str::uuid(),
                "doc_id" => $input['id'],
                "hospital_name" => $name,
                "started_from" => $input['started_from'][$counter],
                "end_to" => $input['end_to'][$counter],
                "designation" => $input['designation'][$counter],
            ];
            array_push($experience, $temp_experience);
            $counter++;
        }
        DoctorWorkExperience::insert($experience);

        DoctorAward::where("doc_id", $input['id'])->delete();
        $awards = [];
        $counter = 0;
        foreach($input['award_name'] as $award) {
            $temp_award = [
                "doc_id" => $input['id'],
                "award_name" => $award,
                "award_year" => $input['award_year'][$counter],
            ];
            array_push($awards, $temp_award);
            $counter++;
        }
        DoctorAward::insert($awards);

        return back()->with('message','Profile has been updated.');
    }
}