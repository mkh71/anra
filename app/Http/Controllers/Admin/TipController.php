<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\TipCateg;
use App\Models\Tip;
use Crypt;


class TipController extends Controller
{

    public function getCategoryList() {
        $tip_categ_list = TipCateg::all();
        return view('admin.tips.categ.list', compact('tip_categ_list'));
    }
    
    public function getCategoryCreateForm() {
        return view('admin.tips.categ.create');
    }

    public function saveOrUpdateCategory(Request $request) {
        $input = $request->all();
        echo '<pre>'; print_r($input); die;
    }

    public function getCategDetails($categ_id) {
        echo $categ_id; die;
    }
    
    public function getTipsList() {
        $tip_list = Tip::orderBy('created_at', 'asc')->get();
        return view('admin.tips.list', compact('tip_list'));
    }

    public function getTipCreateForm() {
    	$tip_categ_list = TipCateg::where('status', 1)->get();
        return view('admin.tips.create', compact('tip_categ_list'));
    }

     public function saveOrUpdateTip(Request $request) {
        $input = $request->all();
        if(!isset($input['id'])) {
	        $validator = Validator::make($input, [
		        'tip_categ' => 'required|exists:tips_categ,id',
		        // 'title' => 'required|max:255',
		        'description'=> 'required',
		        'file' => 'sometimes|required',
		    ]);
        	
        	if($validator->fails()) {
            	return redirect()->back()->withErrors($validator)->withInput();
	    	}

		   	$url = null;
	        if ($request->file('file')) {
	            $file = $request->file;
	            $path = $file->getClientOriginalName();
	            $name = time() . '-' . $path;
	            $filePath = 'images/' . $name;
	            $url = 'https://s3.' . config("aws.AWS_DEFAULT_REGION") . '.amazonaws.com/' . config("aws.AWS_BUCKET") . '/'.$filePath;
	            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
	        }

		    $created_tip = Tip::create([
		    	'title' => $input['title']??null,
		    	'tip_categ_id' => $input['tip_categ'],
		    	'description' => $input['description'],
		    	'file_url' => $url,
		    	'created_by' => Auth::User()->id,
		    ]);

		    if($created_tip) {
		    	return redirect('admin/tips/list')->with('success', 'Tip Added Successfully.');
		    } else {
		    	return redirect('admin/tips/list')->with('danger', 'Unable to add new tip.');
		    }
        } else {
        	$validator = Validator::make($input, [
        		'id' => 'required|exists:tips,id',
		        'tip_categ' => 'required|exists:tips_categ,id',
		        // 'title' => 'required|max:255',
		        'description'=> 'required',
		        'file' => 'sometimes|required',
		    ]);
        	
        	if($validator->fails()) {
            	return redirect()->back()->withErrors($validator)->withInput();
	    	}

	    	if(!isset($input['file'])) {
	    		$file_url = $input['old_img'];
	    	} else {
	    		if ($request->file('file')) {
		            $file = $request->file;
		            $path = $file->getClientOriginalName();
		            $name = time() . '-' . $path;
		            $filePath = 'images/' . $name;
		            $file_url = 'https://s3.' . config("aws.AWS_DEFAULT_REGION") . '.amazonaws.com/' . config("aws.AWS_BUCKET") . '/'.$filePath;
		            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
		        }
	    	}

	    	 $created_tip = Tip::whereId($input['id'])->update([
		    	'title' => $input['title']??null,
		    	'tip_categ_id' => $input['tip_categ'],
		    	'description' => $input['description'],
		    	'file_url' => $file_url,
		    	'created_by' => Auth::User()->id,
		    ]);

		    if($created_tip) {
		    	return redirect('admin/tips/list')->with('success', 'Tip Updated Successfully.');
		    } else {
		    	return redirect('admin/tips/list')->with('danger', 'Unable to update new tip.');
		    }
        }
    }

    public function getTipDetails($tip_id) {
        $tip_id = Crypt::decrypt($tip_id);
        $tip_info = Tip::whereId($tip_id)->first();
        $tip_categ_list = TipCateg::where('status', true)->get();
        return view('admin.tips.edit', compact('tip_info', 'tip_categ_list'));
    }
 
}