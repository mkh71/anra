<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\{
    Appointment,
    User,
    Category
};
use App\Http\Requests\CreateCategory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function getAllCategories() {
    	// $appointments = Appointment::where('doctor_id', Auth::User()->id)->with('patient_info')->get();

        $categories = Category::where('status', 1)->get();
        return view('admin.category', \compact('categories'));
    }

    public function store(CreateCategory $request)
    {
        $request_data = $request->all();
        $url = null;

        if ($request->file('image')) {
            $image = $request->image;
            $path = $image->getClientOriginalName();
            $name = time() . '-' . $path;
            $filePath = 'images/' . $name;
            $url = 'https://s3.' . config("aws.AWS_DEFAULT_REGION") . '.amazonaws.com/' . config("aws.AWS_BUCKET") . '/'.$filePath;
            Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');
        }

        unset($request_data['image']);
        Category::create([
            "name" => $request_data["name"],
            "image" => $url,
            "status" => 1,
        ]);
        return back()->with('message','Category has been created.');
    }
 
}