<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Appointment;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class AppointmentController extends Controller
{

    public function getAllAppointments() {
    	$appointments = Appointment::select([
            "appointment.id",
            "appointment.doctor_id",
            "doctors.name",
            "doctors.profile_photo",
            "appointment.patient_id",
            "patients.name as patient_name",
            "patients.profile_photo as patient_photo",
            "appointment.book_on",
            "appointment.start_time",
            "appointment.end_time",
            "appointment.status"
        ])
        ->leftJoin("users as doctors", "doctors.id", "appointment.doctor_id")
        ->leftJoin("users as patients", "patients.id", "appointment.patient_id")
        ->with('specialities.speciality_details')
        ->get()
        ->toArray();
        return view('admin.appointments', \compact('appointments'));
    }
 
}