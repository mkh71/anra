<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\Package;
use Crypt;


class PackageController extends Controller
{

    public function getPackages() {
        $packages = Package::all();
        return view('admin.package.list', compact('packages'));
    }
    
    public function getPackageDetailById($package_id) {
        $package_id = Crypt::decrypt($package_id);
        $package_info = Package::whereId($package_id)->first();
        if(!$package_info) {
            return back()->with('danger', 'Sorry ! Unanble to update.');
        }
        return view('admin.package.edit', compact('package_info'));
    }

    public function saveOrUpdatePackage(Request $request) {
        $input = $request->all();
        if(isset($input['id'])) {
            $url = $input['old_img'];
            if ($request->file('file')) {
                $file = $request->file;
                $path = $file->getClientOriginalName();
                $name = time() . '-' . $path;
                $filePath = 'images/' . $name;
                $url = 'https://s3.' . config("aws.AWS_DEFAULT_REGION") . '.amazonaws.com/' . config("aws.AWS_BUCKET") . '/'.$filePath;
                Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            }


            Package::whereId($input['id'])->update([
                'name' => $input['name'],
                'first_year_premium' => $input['first_year_premium'],
                'following_year_premium' => $input['following_year_premium'],
                'details' => $this->formatPackageDetailsArray($input['details']),
                'image' => $url,
                'valid_days' => $input['valid_days'],
                'status' => $input['status'],
            ]);
            return redirect()->back()->with('success', 'Updated Successfully.');
        } else {
            $url = null;
            if ($request->file('file')) {
                $file = $request->file;
                $path = $file->getClientOriginalName();
                $name = time() . '-' . $path;
                $filePath = 'images/' . $name;
                $url = 'https://s3.' . config("aws.AWS_DEFAULT_REGION") . '.amazonaws.com/' . config("aws.AWS_BUCKET") . '/'.$filePath;
                Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            }


            $created_package = Package::create([
                'name' => $input['name'],
                'first_year_premium' => $input['first_year_premium'],
                'following_year_premium' => $input['following_year_premium'],
                'details' => $this->formatPackageDetailsArray($input['details']),
                'image' => $url,
                'valid_days' => $input['valid_days'],
                'status' => $input['status'],
            ]);

            if(!$created_package) {
                return redirect()->back()->with('danger', 'Oops ! Something went wrong.');
            }
            return redirect()->back()->with('success', 'Package Added Successfully.');
        }


    }

    public function getAddPackageForm() {
        return view('admin.package.create');
    }
    
    public function formatPackageDetailsArray($details) {
        $details = str_replace("</p><p>",",", $details);
        $details = str_replace("<br> ",",", $details);
        $details = str_replace("<br>",",", $details);
        $details = str_replace("<p>","", $details);
        $details = str_replace("</p>","", $details);  
        return $details_array = explode(',', trim($details));
    }
 
}