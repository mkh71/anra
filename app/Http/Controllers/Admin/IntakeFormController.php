<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\FormCateg;
use App\Models\FieldType;
use App\Models\FieldAction;
use App\Models\FormQuestion;
use App\Models\UserFormAnswer;
use App\Models\FormInfo;
use Crypt;

class IntakeFormController extends Controller
{

    public function getIntakeFormsList() {
    	$available_forms = FormInfo::where('status', true)->withCount('form_categories')->orderBy('seq_order', 'asc')->get();
        return view('admin.intake.forms_list', compact('available_forms'));
    }

    public function getAddFormCategForm() {
        return view('admin.intake.create_form');
    }

    public function saveIntakeForm(Request $request) {
        $input = $request->all();
        if(isset($input['id'])) {
            $created_from = FormInfo::whereId($input['id'])->update([
                'name' => $input['name'],
                'status' => $input['status'],
            ]);
        } else {
            $created_from = FormInfo::create([
                'name' => $input['name'],
                'seq_order' => $input['seq_order']??null,
                'status' => $input['status'],
            ]);
        }
        if($created_from) {
            return redirect('admin/intake/forms')->with('success', 'Intake Form Added Successfully.');
        } else {
            return redirect('admin/intake/forms')->with('danger', 'Unable to add new form.');
        }
    }

    public function deleteIntakeForm($form_id) {
        $form_id = Crypt::decrypt($form_id);
        if(FormInfo::whereId($form_id)->delete()) {
            return redirect('admin/intake/forms')->with('success', 'Deleted Successfully.');
        } else {
            return redirect('admin/intake/forms')->with('danger', 'Sorry ! Unable to delete');
        }
    }

    public function getIntakeEditForm($from_id) {
        $form_id = Crypt::decrypt($from_id);
        $form_info = FormInfo::whereId($form_id)->first();
        return view('admin.intake.forms_edit', compact('form_info'));
    }

    public function getFormQuestionCategList($form_id) {
    	$form_id = Crypt::decrypt($form_id);
    	$form_categ = FormCateg::where('form_id', $form_id)->get();
        return view('admin.intake.form_categ', compact('form_categ', 'form_id'));
    }
    
    public function getAddQuestionForm() {
    	$data['available_forms'] = FormInfo::where('status', true)->orderBy('seq_order', 'asc')->get();
    	$data['field_types'] = FieldType::where('status', true)->get();
        $data['field_action'] = FieldAction::where('status', true)->get();

        return view('admin.intake.add_question_form', compact('data'));
    }

    public function storeIntakeQuestion(Request $request) {
        $input = $request->all();
        $question_options = array();
        if(count($input['question_options']) > 0) {
            foreach ($input['question_options'] as $key => $question) {
                $d['title'] = $question;
                $d['is_other'] = 0;
                $d['is_other_input'] = 0;
               $d['has_sub_question'] = $input['has_sub_question'][$key];
                $d['sub_question_id'] = 0;
                array_push($question_options, $d);
            }
        }

        $created_form_questions = FormQuestion::create([
            'form_categ_id' => $input['form_categ_id'],
            'field_type_id' => $input['field_type'],
            'field_action_id' => $input['field_action'],
            'slug' => $input['question'],
            'field_name' => $input['question'],
            'field_options' => $question_options,
            'status' => true,
        ]);

        if($created_form_questions) {
            return redirect('/admin/intake/forms/quesiton/add')->with('success', 'Question Added Successfully.');
        } else {
            return redirect('/admin/intake/forms/quesiton/add')->with('danger', 'Sorry ! Unable to delete');
        }
    }

    public function updateIntakeQuestion(Request $request) {
        $input = $request->all();
        $question_options = array();
        if(count($input['question_options']) > 0) {
            foreach ($input['question_options'] as $key => $question) {
                $d['title'] = $question;
                $d['is_other'] = 0;
                $d['is_other_input'] = 0;
                $d['has_sub_question'] = $input['has_sub_question'][$key];
                $d['sub_question_id'] = 0;
                array_push($question_options, $d);
            }
        }

        $created_form_questions = FormQuestion::whereId($input['id'])->update([
            'form_categ_id' => $input['form_categ_id'],
            'field_type_id' => $input['field_type'],
            'field_action_id' => $input['field_action'],
            'slug' => $input['question'],
            'field_name' => $input['question'],
            'field_options' => $question_options,
            'status' => true,
        ]);

        if($created_form_questions) {
            return redirect()->back()->with('success', 'Question updated Successfully.');
        } else {
            return redirect()->back()->with('danger', 'Sorry ! Unable to update');
        }
    }

    public function getCategFromFormId(Request $request) {
        $input = $request->all();
        $form_categ = FormCateg::where('form_id', $input['form_id'])->where('status', true)->get();
        return view('admin.ajax.form_categ', compact('form_categ'));
    }

    public function getIntakeCategFormCreate($form_id) {
        $form_id = Crypt::decrypt($form_id);
        return view('admin.intake.create_categ', compact('form_id'));
    }

    public function getIntakeCategEditForm($categ_id) {
        $categ_id = Crypt::decrypt($categ_id);
        $form_categ_info = FormCateg::whereId($categ_id)->first();
        return view('admin.intake.edit_categ', compact('form_categ_info'));
    }

    public function deleteCateg($categ_id) {
        $categ_id = Crypt::decrypt($categ_id);
        if(FormCateg::whereId($categ_id)->delete()) {
            return redirect()->back()->with('success', 'Deleted Successfully.');
        } else {
            return redirect()->back()->with('danger', 'Sorry ! Unable to delete');
        }
    }

    public function saveCateg(Request $request) {
        $input = $request->All();
        if(isset($input['id'])) {
            FormCateg::where('id', $input['id'])->update([
                'name' => $input['name'],
                'status' => $input['status'],
                'form_id' => $input['form_id'],
            ]);
            return redirect('admin/intake/form/'.Crypt::encrypt($input['form_id']).'/category/list')->with('success', 'Intake Form Category updated Successfully.');
        } else {
            $created_categ = FormCateg::create([
                'name' => $input['name'],
                'status' => $input['status'],
                'form_id' => $input['form_id'],
            ]);

            if($created_categ) {
                return redirect('admin/intake/form/'.Crypt::encrypt($input['form_id']).'/category/list')->with('success', 'Intake Form Category Added Successfully.');
            } else {
                return redirect('admin/intake/form/'.Crypt::encrypt($input['form_id']).'/category/list')->with('danger', 'Unable to add category form.');
            }
        }
    }

    public function getCategQuestions($categ_id) {
        $form_questions = FormQuestion::where('form_categ_id', Crypt::decrypt($categ_id))->get();
        return view('admin.intake.categ_question_list', compact('form_questions'));
    }

    public function getEditIntakeQuestionForm($question_id) {
        $data['available_forms'] = FormInfo::where('status', true)->orderBy('seq_order', 'asc')->get();
        $data['field_types'] = FieldType::where('status', true)->get();
        $data['field_action'] = FieldAction::where('status', true)->get();

        $form_question = FormQuestion::whereId(Crypt::decrypt($question_id))->first();
        $form_categ = FormCateg::whereId($form_question->form_categ_id)->where('status', true)->get();

        if(!$form_question) {
            return redirect()->back()->with('danger', 'Oops ! Something went wrong.');
        } else {
            return view('admin.intake.edit_question_form', compact('data', 'form_question', 'form_categ'));
        }
    }

    public function deleteIntakeQuestion($question_id) {
        if(FormQuestion::whereId(Crypt::decrypt($question_id))->delete()) {
            return redirect()->back()->with('success', 'Deleted Successfully.');
        } else {
            return redirect()->back()->with('danger', 'Unable to Delete Question.');
        }
    }
  
}