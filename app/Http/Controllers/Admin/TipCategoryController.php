<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\TipCateg;
use App\Models\Tip;
use Crypt;


class TipCategoryController extends Controller
{

    public function getCategoryList() {
        $tip_categ_list = TipCateg::all();
        return view('admin.tips.categ.list', compact('tip_categ_list'));
    }
    
    public function getCategoryCreateForm() {
        return view('admin.tips.categ.create');
    }

    public function saveCategory(Request $request) {
        $input = $request->all();
        TipCateg::create([
            "name" => $input["name"], 
            "status" => $input["status"]
        ]);
        return back()->with('message','Category has been created.');
    }

    public function getCategDetails($categ_id) {
        $category = TipCateg::where('id', $categ_id)->first();
        return view('admin.tips.categ.edit', \compact('category'));
    }
    
   

    public function getTipCreateForm() {
    	$tip_categ_list = TipCateg::where('status', true)->get();
        return view('admin.tips.create', compact('tip_categ_list'));
    }

     public function updateCatgegory($id, Request $request) {
        $input = $request->all();
        TipCateg::where('id', $id)->update([
            "name" => $input["name"],
            "status" => $input["status"],
        ]);
        return back()->with('message','Category has been updated.');
    }
 
}