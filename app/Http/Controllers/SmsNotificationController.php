<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use \Carbon\Carbon;
use Nexmo;

class SmsNotificationController extends Controller
{


	public function sendSms($country_code, $mobile_no, $message) {
		if($country_code === '+1') {
			$number = str_replace('+', '', $country_code).str_replace('-', '', $mobile_no);
			$status = Nexmo::message()->send([
			    'to'   => $number,
			    'from' => '12044001529',
			    'text' => $message
			]);
			return $status;
		}
	}


}