<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    User,
    UserAddress,
    Doctor,
    Category
};

class AppointmentController extends Controller
{

    public function appointment() {
        $doctor_profile = (new Doctor)->profileInfo()->toArray();
        $categories = Category::all();
        return view('patient.doctor_search', \compact('doctor_profile', 'categories'));
    }

    public function searchDoctor() {
        return view('patient.doctor_search');
    }

    public function booking() {
        return view('patient.booking');
    }
}