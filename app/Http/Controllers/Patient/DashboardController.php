<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class DashboardController extends Controller
{

    public function getDashboard() {
        return view('patient.dashboard');
    }
    
 
}