<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    User,
    UserAddress
};
use App\Http\Requests\{
    UpdatePassword,
};

class SettingController extends Controller
{

    public function getChangePassword() {
        return view('patient.change-password');
    }
    
    public function getProfileSettings() {
        $patient = User::select('users.email', 'users.name', 'users.dob', 'users.country_code', 'users.phone', 'users.profile_photo', 'users.created_at', 'users.id', 'users.refrence_key', 'users.gender')
            ->with(['user_address'])
            ->where('id', Auth::user()->id)
            ->first()
            ->toArray();
        return view('patient.profile-settings', \compact("patient"));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        User::where('id', $id)->update([
            "name" => $input["name"],
            "dob" => $input["dob"],
            "email" => $input["email"],
            "country_code" => $input["country_code"],
            "phone" => $input["phone"],
        ]);

        UserAddress::where("user_id", $id)
        ->update([
            "street" => $input["street"],
            "city" => $input["city"],
            "province" => $input["province"],
            "country" => $input["country"],
        ]);

        return back()->with('message','Profile has been updated.');
    }

    public function updatePassword(UpdatePassword $request) {
        $input = $request->validated();
        $id = Auth::user()->id;
        User::where('id', $id)->update([
            "password" => \bcrypt($input["new_password"]),
        ]);
        return back()->with('message','Password has been updated.');
    }
 
}