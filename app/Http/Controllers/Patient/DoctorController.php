<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class DoctorController extends Controller
{

    public function getPatientFavDoctors() {
        return view('patient.fav_doctors');
    }
    
 
}