<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class PatientController extends Controller
{

    public function getDependent() {
        return view('patient.dependent_list');
    }
 
}