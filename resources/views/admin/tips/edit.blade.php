@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Edit Tips</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Tips</a></li>
					<li class="breadcrumb-item active">Edit Tips</li>
				</ul>
			</div>

		</div>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			@include('status_message')			
		<form action="{{ url('/admin/tip/update') }}" method="POST" enctype="multipart/form-data" >
			@csrf
			<input type="hidden" name="id" value="{{ $tip_info->id }}">
			<input type="hidden" name="old_img" value="{{ $tip_info->file_url }}">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Tip Information</h4>
					<div class="row form-row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Tip Category <span class="text-danger">*</span></label>
								<select class="form-control" name="tip_categ" required="">
									<option>Please Select</option>
									@foreach($tip_categ_list as $tip_categ)
										<option value="{{ $tip_categ->id }}" @if($tip_info->tip_categ_id === $tip_categ->id ) selected @endif>{{ $tip_categ->name }}</option>
									@endforeach
								</select>
								@error('tip_categ')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Title <span class="text-danger">*</span></label>
								<input type="text" name="title" value="{{ $tip_info->title }}" class="form-control" >
								@error('title')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Description <span class="text-danger">*</span></label>
								<textarea name="description" id="summernote" style="height: 200px;" required="">{{ $tip_info->description }}</textarea>
								@error('description')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Additional File </label>
								<input type="file" name="file" class="form-control">
								@error('file')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>                     	
                        
					</div>
				</div>
			</div>

			
			<div class="submit-section submit-btn-bottom">
				<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
			</div>
		</form>	

		</div>
	</div>
@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.js') }}"></script>
<script>
	$(document).ready(function() {
  		$('#summernote').summernote({
	       height: 200,
	      maximumImageFileSize: 300*1024, // 500 KB
	      callbacks:{
	        onImageUploadError: function(msg){
	           alert(msg + ' (300 KB)');
	        }
	      }
	    });
	});
</script>	
@endsection