@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('assets/css/feathericon.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Tips</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Tips</li>
				</ul>
			</div>
			<div class="col-sm-5 col">
				<a href="{{ url('admin/tip/create') }}" class="btn btn-primary float-right mt-2">Add</a>
			</div>
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		@include('status_message')
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Details</th>
								<th>Attached File</th>
								<th class="text-right">Actions</th>
							</tr>
						</thead>
						<tbody>

						@foreach($tip_list as $key => $tip)
						<tr>
							<td>{{ $key+1 }}</td>
							
							<td>
								<h2 class="table-avatar">
									{{ $tip->title }}
								</h2>
							</td>

							<td width="20%">

								{!! substr($tip->description,0, 50) !!}	
							</td>

							<td>
								@if(null != $tip->file_url)
									<img src="{{ $tip->file_url }}" height="100px" width="100px"> 
								@else
									N/A
								@endif
							</td>
						
							<td class="text-right">
								<div class="actions">
									<a class="btn btn-sm bg-success-light" href="{{ url('/admin/tip/'.Crypt::encrypt($tip->id).'/detail') }}">
										<i class="fe fe-pencil"></i> Edit
									</a>
								</div>
							</td>
						</tr>

					
						@endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
	</div>			
</div>
@endsection