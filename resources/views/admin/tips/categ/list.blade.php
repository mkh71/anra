@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('assets/css/feathericon.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Tips</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Tips</li>
				</ul>
			</div>
			<div class="col-sm-5 col">
				<a href="{{ url('admin/tips/category/create') }}" class="btn btn-primary float-right mt-2">Add</a>
			</div>
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		@include('status_message')
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Status</th>
								<th class="text-right">Actions</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($tip_categ_list as $list)
						<tr>
							<td>{{ $list->id }}</td>
							
							<td>
								<h2 class="table-avatar">
									{{ $list->name }}
								</h2>
							</td>

							<td width="20%">
                            {{ ($list->status) ? "Active" : "In Active" }}
							</td>
						
							<td class="text-right">
								<div class="actions">
									<a class="btn btn-sm bg-success-light" href="{{ url('/admin/tips-category/'.$list->id.'/detail') }}">
										<i class="fe fe-pencil"></i> Edit
									</a>
								</div>
							</td>
						</tr>
                        @endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
	</div>			
</div>
@endsection