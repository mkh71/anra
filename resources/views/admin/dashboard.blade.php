@extends('layouts.admin')

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">Welcome Admin!</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item active">Dashboard</li>
				</ul>
			</div>
		</div>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-xl-3 col-sm-6 col-12">
			<div class="card">
				<div class="card-body">
					<div class="dash-widget-header">
						<span class="dash-widget-icon text-primary border-primary">
							<i class="fe fe-users"></i>
						</span>
						<div class="dash-count">
							<h3>{{ $records["doctor_count"] }}</h3>
						</div>
					</div>
					<div class="dash-widget-info">
						<h6 class="text-muted">Doctors</h6>
						<div class="progress progress-sm">
							<div class="progress-bar bg-primary w-50"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-sm-6 col-12">
			<div class="card">
				<div class="card-body">
					<div class="dash-widget-header">
						<span class="dash-widget-icon text-success">
							<i class="fe fe-credit-card"></i>
						</span>
						<div class="dash-count">
							<h3>{{ $records["patient_count"] }}</h3>
						</div>
					</div>
					<div class="dash-widget-info">
						
						<h6 class="text-muted">Patients</h6>
						<div class="progress progress-sm">
							<div class="progress-bar bg-success w-50"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-sm-6 col-12">
			<div class="card">
				<div class="card-body">
					<div class="dash-widget-header">
						<span class="dash-widget-icon text-danger border-danger">
							<i class="fe fe-money"></i>
						</span>
						<div class="dash-count">
							<h3>{{ $records["appointment_count"] }}</h3>
						</div>
					</div>
					<div class="dash-widget-info">
						
						<h6 class="text-muted">Appointment</h6>
						<div class="progress progress-sm">
							<div class="progress-bar bg-danger w-50"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-sm-6 col-12">
			<div class="card">
				<div class="card-body">
					<div class="dash-widget-header">
						<span class="dash-widget-icon text-warning border-warning">
							<i class="fe fe-folder"></i>
						</span>
						<div class="dash-count">
							<h3>$62523</h3>
						</div>
					</div>
					<div class="dash-widget-info">
						
						<h6 class="text-muted">Revenue</h6>
						<div class="progress progress-sm">
							<div class="progress-bar bg-warning w-50"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-lg-6">
		
			<!-- Sales Chart -->
			<div class="card card-chart">
				<div class="card-header">
					<h4 class="card-title">Revenue</h4>
				</div>
				<div class="card-body">
					<div id="morrisArea"></div>
				</div>
			</div>
			<!-- /Sales Chart -->
			
		</div>
		<div class="col-md-12 col-lg-6">
		
			<!-- Invoice Chart -->
			<div class="card card-chart">
				<div class="card-header">
					<h4 class="card-title">Status</h4>
				</div>
				<div class="card-body">
					<div id="morrisLine"></div>
				</div>
			</div>
			<!-- /Invoice Chart -->
			
		</div>	
	</div>
	<div class="row">
		<div class="col-md-6 d-flex">
			<div class="card card-table flex-fill">
				<div class="card-header">
					<h4 class="card-title">Top 5 Doctors List</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover table-center mb-0">
							<thead>
								<tr>
									<th>Doctor Name</th>
									<th>Email</th>
									<th>Total Appointments</th>
									<th>No of Patients</th>
								</tr>
							</thead>
							<tbody>
							@foreach($records["doctors"] as $doctor)
							<tr>
									<td>
										<h2 class="table-avatar">
											<a href="{{ url('/admin/doctor/'.$doctor['id'].'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ $doctor['profile_photo']??url('/assets/img/logo.png') }}" alt="User Image"></a>
											<a href="{{ url('/admin/doctor/'.$doctor['id'].'/profile') }}">{{ $doctor["name"] }}</a>
										</h2>
									</td>
									<td>
										{{ $doctor['email'] }}
									</td>
									<td>{{ $doctor["appointment_count"] }}</td>
									<td>{{ $doctor["patient_count"] }}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /Recent Orders -->
			
		</div>
		<div class="col-md-6 d-flex">
		
			<!-- Feed Activity -->
			<div class="card  card-table flex-fill">
				<div class="card-header">
					<h4 class="card-title">Recent Registerd Patients </h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover table-center mb-0">
							<thead>
								<tr>													
									<th>Patient Name</th>
									<th>Phone</th>
									<th>Registerd On</th>
									<th>Purchased Package</th>													
								</tr>
							</thead>
							<tbody>
							@foreach($records["patients"] as $patient)
								<tr>
									<td>
										<h2 class="table-avatar">
											<a href="{{ url('/admin/patient/'.$patient['id'].'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ $patient["profile_photo"]??'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png' }}" alt="User Image"></a>
											<a href="{{ url('/admin/patient/'.$patient['id'].'/profile') }}">{{ $patient["name"] }} </a>
										</h2>
									</td>
									<td>{{ $patient["phone"] }}</td>
									<td>{{ \Carbon\Carbon::parse($patient["created_at"])->format('F j, Y g:i a') }}</td>
									<td class="text-right">$100.00</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /Feed Activity -->
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		
			<!-- Recent Orders -->
			<div class="card card-table">
				<div class="card-header">
					<h4 class="card-title">Recent Booked Appointments</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover table-center mb-0">
							<thead>
								<tr>
									<th>Doctor Name</th>
									<th>Patient Name</th>
									<th>Apointment Time</th>
									<th>Apointment Type</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								@foreach($records["appointments"] as $appointment)
								<tr>
									<td>
										<h2 class="table-avatar">
											<a href="{{ url('/admin/doctor/'.$appointment['doctor_id'].'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ $appointment["doc_profile_photo"] }}" alt="User Image"></a>
											<a href="{{ url('/admin/doctor/'.$appointment['doctor_id'].'/profile') }}">{{ $appointment["doctor_name"] }}</a>
										</h2>
									</td>
									
									<td>
										<h2 class="table-avatar">
											<a href="{{ url('/admin/patient/'.$appointment['id'].'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ $appointment["profile_photo"] }}" alt="User Image"></a>
											<a href="{{ url('/admin/patient/'.$appointment['id'].'/profile') }}">{{ $appointment["name"] }}</a>
										</h2>
									</td>
									<td>{{ \Carbon\Carbon::parse($appointment["book_on"])->format('F j, Y') }}<span class="text-primary d-block">{{ \Carbon\Carbon::parse($appointment["start_time"])->format('g:i a') }} - {{ \Carbon\Carbon::parse($appointment["end_time"])->format('g:i a') }}</span></td>
									<td>
										<span class="btn-sm btn-warning">{{ $appointment["type"] }}</span>
									</td>
									<td>
										<span class="btn-sm btn-danger">{{ $appointment["status"] }}</span>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection