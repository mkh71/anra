@extends('layouts.admin')

@section('css')
<!-- Fancybox CSS -->
<link rel="stylesheet" href="{{ url('/assets/plugins/fancybox/jquery.fancybox.min.css') }}">

<!-- Main CSS -->
<link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ url('/new_assets/plugins/datatables/datatables.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Doctor Profile</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Doctor</a></li>
					<li class="breadcrumb-item active">Profile</li>
				</ul>
			</div>
			<!-- <div class="col-sm-5 col">
				<a href="add_doctor.html" class="btn btn-primary float-right mt-2">Add</a>
			</div> -->
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">
				<div class="doctor-widget">
					<div class="doc-info-left">
						<div class="doctor-img">
							<img src="{{ $profile[0]["profile_photo"] }}" class="img-fluid" alt="User Image">
						</div>
						<div class="doc-info-cont">
							<h4 class="doc-name">Dr. {{ $profile[0]["name"] }}</h4>
							<p class="doc-speciality">BDS, MDS - Oral & Maxillofacial Surgery</p>
							<p class="doc-department"><img src="/assets/img/specialities/specialities-05.png" class="img-fluid" alt="Speciality">Dentist</p>
							<div class="rating">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<span class="d-inline-block average-rating">(N/A)</span>
							</div>
							<!-- <div class="clinic-details">
								<p class="doc-location"><i class="fas fa-map-marker-alt"></i> {{ $profile[0]["address"] .", ". $profile[0]["city"]. ", ". $profile[0]["state"].", ". $profile[0]["country"]}} - <a href="javascript:void(0);">Get Directions</a></p>
								<ul class="clinic-gallery">
									<li>
										<a href="/assets/img/features/feature-01.jpg" data-fancybox="gallery">
											<img src="/assets/img/features/feature-01.jpg" alt="Feature">
										</a>
									</li>
									<li>
										<a href="/assets/img/features/feature-02.jpg" data-fancybox="gallery">
											<img  src="/assets/img/features/feature-02.jpg" alt="Feature Image">
										</a>
									</li>
									<li>
										<a href="/assets/img/features/feature-03.jpg" data-fancybox="gallery">
											<img src="/assets/img/features/feature-03.jpg" alt="Feature">
										</a>
									</li>
									<li>
										<a href="/assets/img/features/feature-04.jpg" data-fancybox="gallery">
											<img src="/assets/img/features/feature-04.jpg" alt="Feature">
										</a>
									</li>
								</ul>
							</div> -->
							<div class="clinic-services">
								<span>Dental Fillings</span>
								<span>Teeth Whitneing</span>
							</div>
						</div>
					</div>
					<div class="doc-info-right">
						<div class="clini-infos">
							<ul>
								<li><i class="fa fa-envelope"></i>{{ $profile[0]["email"] }}</li>
								<li><i class="fa fa-phone"></i> {{ $profile[0]["country_code"]." ". $profile[0]["phone"]}} </li>
								<li><i class="fas fa-map-marker-alt"></i>{{ $profile[0]["state"].", ". $profile[0]["country"] }}</li>
							</ul>
						</div>
						<div class="doctor-action">
							<a href="javascript:void(0)" class="btn btn-white fav-btn">
								<i class="far fa-heart"></i>
							</a>
							<a href="chat.html" class="btn btn-white msg-btn">
								<i class="far fa-comment-alt"></i>
							</a>
							<a href="javascript:void(0)" class="btn btn-white call-btn" data-toggle="modal" data-target="#voice_call">
								<i class="fas fa-phone"></i>
							</a>
							<a href="javascript:void(0)" class="btn btn-white call-btn" data-toggle="modal" data-target="#video_call">
								<i class="fas fa-video"></i>
							</a>
						</div>
						<div class="clinic-booking">
							<a class="apt-btn" href="booking.html">Book Appointment</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Doctor Widget -->
		
		<!-- Doctor Details Tab -->
		<div class="card">
			<div class="card-body pt-0">
			
				<!-- Tab Menu -->
				<nav class="user-tabs mb-4">
					<ul class="nav nav-tabs nav-tabs-bottom nav-justified">
						<li class="nav-item">
							<a class="nav-link active" href="#doc_overview" data-toggle="tab">Overview</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#doc_locations" data-toggle="tab">Locations</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link" href="#doc_business_hours" data-toggle="tab">Business Hours</a>
						</li>
					</ul>
				</nav>
				<!-- /Tab Menu -->
				
				<!-- Tab Content -->
				<div class="tab-content pt-0">
				
					<!-- Overview Content -->
					<div role="tabpanel" id="doc_overview" class="tab-pane fade show active">
						<div class="row">
							<div class="col-md-12 col-lg-9">
							
								<!-- About Details -->
								<div class="widget about-widget">
									<h4 class="widget-title">About Me</h4>
									<p>{{ $profile[0]["about_me"] }}</p>
								</div>
								<!-- /About Details -->
							
								<!-- Education Details -->
								<div class="widget education-widget">
									<h4 class="widget-title">Education</h4>
									<div class="experience-box">
										<ul class="experience-list">
											@foreach($profile[0]["educations"] as $education)
											<li>
												<div class="experience-user">
													<div class="before-circle"></div>
												</div>
												<div class="experience-content">
													<div class="timeline-content">
														<a href="#/" class="name">{{ $education["college"] }}</a>
														<div>{{ $education["degree"] }}</div>
														<span class="time">{{ $education["starting_year"] }} - {{ $education["completion_year"] }}</span>
													</div>
												</div>
											</li>
											@endforeach
										</ul>
									</div>
								</div>
								<!-- /Education Details -->
						
								<!-- Experience Details -->
								<div class="widget experience-widget">
									<h4 class="widget-title">Work & Experience</h4>
									<div class="experience-box">
										<ul class="experience-list">
										@foreach($profile[0]["experiences"] as $experience)
											<li>
												<div class="experience-user">
													<div class="before-circle"></div>
												</div>
												<div class="experience-content">
													<div class="timeline-content">
														<a href="#/" class="name">{{ $experience["hospital_name"] }}</a>
														<span class="time">{{ $experience["started_from"] }} - {{ $experience["end_to"] }}</span>
													</div>
												</div>
											</li>
											@endforeach
										</ul>
									</div>
								</div>
								<!-- /Experience Details -->
					
								<!-- Awards Details -->
								<div class="widget awards-widget">
									<h4 class="widget-title">Awards</h4>
									<div class="experience-box">
										<ul class="experience-list">
											@foreach($profile[0]["awards"] as $award)
											<li>
												<div class="experience-user">
													<div class="before-circle"></div>
												</div>
												<div class="experience-content">
													<div class="timeline-content">
														<p class="exp-year">{{ $award["award_year"] }}</p>
														<h4 class="exp-title">{{ $award["award_name"] }}</h4>
														<p>{{ $award["short_desc"] }}</p>
													</div>
												</div>
											</li>
											@endforeach
										</ul>
									</div>
								</div>
								<!-- /Awards Details -->
								
								<!-- Services List -->
								<div class="service-list">
									<h4>Services</h4>
									<ul class="clearfix">
										<li>Tooth cleaning </li>
										<li>Root Canal Therapy</li>
										<li>Implants</li>
										<li>Composite Bonding</li>
										<li>Fissure Sealants</li>
										<li>Surgical Extractions</li>
									</ul>
								</div>
								<!-- /Services List -->
								
								<!-- Specializations List -->
								<div class="service-list">
									<h4>Specializations</h4>
									<ul class="clearfix">
									@foreach($profile[0]["specialities"] as $speciality)
										<li>{{ $speciality["doctor_specialities"][0]["name"] }}</li>
									@endforeach
									</ul>
								</div>
								<!-- /Specializations List -->

							</div>
						</div>
					</div>
					<!-- /Overview Content -->
					
					<!-- Locations Content -->
					<div role="tabpanel" id="doc_locations" class="tab-pane fade">
					
						<!-- Location List -->
						<div class="location-list">
							<div class="row">
							
								<!-- Clinic Content -->
								<div class="col-md-6">
									<div class="clinic-content">
										<h4 class="clinic-name"><a href="#">Smile Cute Dental Care Center</a></h4>
										<p class="doc-speciality">MDS - Periodontology and Oral Implantology, BDS</p>
										<div class="rating">
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star"></i>
											<span class="d-inline-block average-rating">(4)</span>
										</div>
										<div class="clinic-details mb-0">
											<h5 class="clinic-direction"> <i class="fas fa-map-marker-alt"></i> 2286  Sundown Lane, Austin, Texas 78749, USA <br><a href="javascript:void(0);">Get Directions</a></h5>
											<ul>
												<li>
													<a href="/assets/img/features/feature-01.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-01.jpg" alt="Feature Image">
													</a>
												</li>
												<li>
													<a href="/assets/img/features/feature-02.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-02.jpg" alt="Feature Image">
													</a>
												</li>
												<li>
													<a href="/assets/img/features/feature-03.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-03.jpg" alt="Feature Image">
													</a>
												</li>
												<li>
													<a href="/assets/img/features/feature-04.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-04.jpg" alt="Feature Image">
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- /Clinic Content -->
								
								<!-- Clinic Timing -->
								<div class="col-md-4">
									<div class="clinic-timing">
										<div>
											<p class="timings-days">
												<span> Mon - Sat </span>
											</p>
											<p class="timings-times">
												<span>10:00 AM - 2:00 PM</span>
												<span>4:00 PM - 9:00 PM</span>
											</p>
										</div>
										<div>
										<p class="timings-days">
											<span>Sun</span>
										</p>
										<p class="timings-times">
											<span>10:00 AM - 2:00 PM</span>
										</p>
										</div>
									</div>
								</div>
								<!-- /Clinic Timing -->
								
								<div class="col-md-2">
									<div class="consult-price">
										$250
									</div>
								</div>
							</div>
						</div>
						<!-- /Location List -->
						
						<!-- Location List -->
						<div class="location-list">
							<div class="row">
							
								<!-- Clinic Content -->
								<div class="col-md-6">
									<div class="clinic-content">
										<h4 class="clinic-name"><a href="#">The Family Dentistry Clinic</a></h4>
										<p class="doc-speciality">MDS - Periodontology and Oral Implantology, BDS</p>
										<div class="rating">
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star"></i>
											<span class="d-inline-block average-rating">(4)</span>
										</div>
										<div class="clinic-details mb-0">
											<p class="clinic-direction"> <i class="fas fa-map-marker-alt"></i> 2883  University Street, Seattle, Texas Washington, 98155 <br><a href="javascript:void(0);">Get Directions</a></p>
											<ul>
												<li>
													<a href="/assets/img/features/feature-01.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-01.jpg" alt="Feature Image">
													</a>
												</li>
												<li>
													<a href="/assets/img/features/feature-02.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-02.jpg" alt="Feature Image">
													</a>
												</li>
												<li>
													<a href="/assets/img/features/feature-03.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-03.jpg" alt="Feature Image">
													</a>
												</li>
												<li>
													<a href="/assets/img/features/feature-04.jpg" data-fancybox="gallery2">
														<img src="/assets/img/features/feature-04.jpg" alt="Feature Image">
													</a>
												</li>
											</ul>
										</div>

									</div>
								</div>
								<!-- /Clinic Content -->
								
								<!-- Clinic Timing -->
								<div class="col-md-4">
									<div class="clinic-timing">
										<div>
											<p class="timings-days">
												<span> Tue - Fri </span>
											</p>
											<p class="timings-times">
												<span>11:00 AM - 1:00 PM</span>
												<span>6:00 PM - 11:00 PM</span>
											</p>
										</div>
										<div>
											<p class="timings-days">
												<span>Sat - Sun</span>
											</p>
											<p class="timings-times">
												<span>8:00 AM - 10:00 AM</span>
												<span>3:00 PM - 7:00 PM</span>
											</p>
										</div>
									</div>
								</div>
								<!-- /Clinic Timing -->
								
								<div class="col-md-2">
									<div class="consult-price">
										$350
									</div>
								</div>
							</div>
						</div>
						<!-- /Location List -->

					</div>
					<!-- /Locations Content -->
					
					
					<!-- Business Hours Content -->
					<div role="tabpanel" id="doc_business_hours" class="tab-pane fade">
						<div class="row">
							<div class="col-md-6 offset-md-3">
							
								<!-- Business Hours Widget -->
								<div class="widget business-widget">
									<div class="widget-content">
										<div class="listing-hours">
											<div class="listing-day current">
												<div class="day">Today <span>5 Nov 2019</span></div>
												<div class="time-items">
													<span class="open-status"><span class="badge bg-success-light">Open Now</span></span>
													<span class="time">07:00 AM - 09:00 PM</span>
												</div>
											</div>
											<div class="listing-day">
												<div class="day">Monday</div>
												<div class="time-items">
													<span class="time">07:00 AM - 09:00 PM</span>
												</div>
											</div>
											<div class="listing-day">
												<div class="day">Tuesday</div>
												<div class="time-items">
													<span class="time">07:00 AM - 09:00 PM</span>
												</div>
											</div>
											<div class="listing-day">
												<div class="day">Wednesday</div>
												<div class="time-items">
													<span class="time">07:00 AM - 09:00 PM</span>
												</div>
											</div>
											<div class="listing-day">
												<div class="day">Thursday</div>
												<div class="time-items">
													<span class="time">07:00 AM - 09:00 PM</span>
												</div>
											</div>
											<div class="listing-day">
												<div class="day">Friday</div>
												<div class="time-items">
													<span class="time">07:00 AM - 09:00 PM</span>
												</div>
											</div>
											<div class="listing-day">
												<div class="day">Saturday</div>
												<div class="time-items">
													<span class="time">07:00 AM - 09:00 PM</span>
												</div>
											</div>
											<div class="listing-day closed">
												<div class="day">Sunday</div>
												<div class="time-items">
													<span class="time"><span class="badge bg-danger-light">Closed</span></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /Business Hours Widget -->
						
							</div>
						</div>
					</div>
					<!-- /Business Hours Content -->
					
				</div>
			</div>
		</div>
		<!-- /Doctor Details Tab -->

	</div>
</div>
@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('/new_assets/plugins/datatables/datatables.min.js') }}"></script>
@endsection