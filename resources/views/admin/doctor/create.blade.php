@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('/assets/plugins/fancybox/jquery.fancybox.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ url('/new_assets/plugins/datatables/datatables.min.css') }}">
	<link rel="stylesheet" href="{{ url('/assets/plugins/select2/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ url('/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}">
	<link rel="stylesheet" href="{{ url('/assets/plugins/dropzone/dropzone.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Create Doctor</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Doctors</a></li>
					<li class="breadcrumb-item active">Create Doctor</li>
				</ul>
			</div>
			<!-- <div class="col-sm-5 col">
				<a href="add_doctor.html" class="btn btn-primary float-right mt-2">Add</a>
			</div> -->
		</div>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			@if(Session::has('message'))
				<p class="alert alert-success">{{ Session::get('message') }}</p>
			@endif			
		<form action="{{ url('/admin/create-doctor') }}" method="POST" enctype="multipart/form-data" >
			@csrf
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Basic Information</h4>
					<div class="row form-row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="change-avatar">
									<div class="profile-img">
										<img src="../assets/img/doctors/doctor-thumb-02.jpg" alt="User Image">
									</div>
									<div class="upload-img">
										<div class="change-photo-btn">
											<span><i class="fa fa-upload"></i> Upload Photo</span>
											<input type="file" class="upload">
										</div>
										<small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Email <span class="text-danger">*</span></label>
								<input type="email" name="email" value="" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Name <span class="text-danger">*</span></label>
								<input type="text" name="name" value="" class="form-control" required>
								@error('name')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Phone Number</label>
								<input type="text" class="form-control" name="phone" value="" required>
								@error('phone')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Gender</label>
								<select name="gender" class="form-control select">
									<option value="">Select</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
								@error('gender')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-0">
								<label>Date of Birth</label>
								<input type="date" class="form-control" name="dob" value="" required>
								@error('dob')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-body">
					<h4 class="card-title">About Me</h4>
					<div class="form-group mb-0">
						<label>Biography</label>
						<textarea class="form-control" name="about_me" rows="5"></textarea>
					</div>
				</div>
			</div>

			<div class="card contact-card">
				<div class="card-body">
					<h4 class="card-title">Contact Details</h4>
					<div class="row form-row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Address</label>
								<input type="text" name="address" value="" class="form-control">
								@error('address')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">City</label>
								<input type="text" name="city" value="" class="form-control">
								@error('city')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">State / Province</label>
								<input type="text" name="state" value="" class="form-control">
								@error('state')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Country</label>
								<input type="text" name="country" value="" class="form-control">
								@error('country')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Postal Code</label>
								<input type="text" name="postal_code" value="" class="form-control">
								@error('postal_code')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card services-card">
				<div class="card-body">
					<h4 class="card-title">Specialization</h4>
					<div class="form-group mb-0">
						<label>Specialization </label>
						<select name="" id="js-example-basic-hide-search-multi" class="form-control input-tags" multiple>
							@foreach($categories as $key => $category)
								<option value="">{{ $category->name }}</option>
							@endforeach
						</select>

					</div> 
				</div>              
			</div>

			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Education</h4>
					<div class="education-info">
						<div class="row form-row education-cont">
							<div class="col-12 col-md-10 col-lg-11">
								<div class="row form-row">
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>Degree</label>
												<input type="text" value="" name="degree[]" class="form-control" required>
											</div> 
										</div>
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>College/Institute</label>
												<input type="text" value="" name="college[]" class="form-control" required>
											</div> 
										</div>
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>Starting Year</label>
												<input type="text" value="" name="starting_year[]" class="form-control" required>
											</div> 
										</div>
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>Year of Completion</label>
												<input type="text" value="" name="completion_year[]" class="form-control" required>
											</div> 
										</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="add-more">
						<a href="javascript:void(0);" class="add-education"><i class="fa fa-plus-circle"></i> Add More</a>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Experience</h4>
					<div class="experience-info">
						<div class="row form-row experience-cont">
							<div class="col-12 col-md-10 col-lg-11">
								<div class="row form-row">
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>Hospital Name</label>
											<input type="text" value="" name="hospital_name[]" class="form-control" required>
										</div> 
									</div>
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>From</label>
											<input type="text" value="" name="started_from[]" class="form-control" required>
										</div> 
									</div>
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>To</label>
											<input type="text" value="" name="end_to[]" class="form-control" required>
										</div> 
									</div>
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>Designation</label>
											<input type="text" value="" name="designation[]" class="form-control" required>
										</div> 
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="add-more">
						<a href="javascript:void(0);" class="add-experience"><i class="fa fa-plus-circle"></i> Add More</a>
					</div>
				</div>
			</div>
			
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Awards</h4>
					<div class="awards-info">
					
						<div class="row form-row awards-cont">
							<div class="col-12 col-md-5">
								<div class="form-group">
									<label>Awards</label>
									<input type="text" value="" name="award_name[]" class="form-control" required>
								</div> 
							</div>
							<div class="col-12 col-md-5">
								<div class="form-group">
									<label>Year</label>
									<input type="text" value="" name="award_year[]" class="form-control" required>
								</div> 
							</div>
						</div>
					</div>
					<div class="add-more">
						<a href="javascript:void(0);" class="add-award"><i class="fa fa-plus-circle"></i> Add More</a>
					</div>
				</div>
			</div>
			
			<div class="submit-section submit-btn-bottom">
				<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
			</div>
		</form>	

		</div>
	</div>
@endsection

@section('javascript')
<script src="{{ url('/assets/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ url('/assets/plugins/dropzone/dropzone.min.js') }}"></script>
<script src="{{ url('/assets/js/profile-settings.js') }}"></script>

<script>
	$('#js-example-basic-hide-search-multi').select2();

	$('#js-example-basic-hide-search-multi').on('select2:opening select2:closing', function( event ) {
	    var $searchfield = $(this).parent().find('.select2-search__field');
	    $searchfield.prop('disabled', true);
	});
</script>
@endsection