@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('/new_assets/plugins/datatables/datatables.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">List of Doctors</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Users</a></li>
					<li class="breadcrumb-item active">Doctor</li>
				</ul>
			</div>
			<div class="col-sm-5 col">
				<a href="{{ url('/admin/doctor/create') }}" class="btn btn-primary float-right mt-2">Add</a>
			</div>
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Doctor Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Speciality</th>
								<th>Member Since</th>
								<th>Status</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tbody>
						@foreach($doctors as $doctor)
							<tr>
								<td>
									<h2 class="table-avatar">
										<a href="{{ url('/admin/doctor/'.$doctor->id.'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ $doctor['profile_photo']??url('/assets/img/logo.png') }}" alt="User Image"></a>
										<a href="{{ url('/admin/doctor/'.$doctor->id.'/profile') }}">{{ $doctor["name"] }}</a>
									</h2>
								</td>
								<td>{{ $doctor["email"] }}</td>
								<td>{{ $doctor["phone"] }}</td> 
								<td>
									@foreach($doctor['specialities'] as $key => $specialities)
										<span class="btn btn-rounded btn-primary btn-sm">{{ $specialities->speciality_details->name }}</span>
									@endforeach
								</td> 

								<td>{{ \Carbon\Carbon::parse($doctor->created_at)->format('F j, Y') }} <br><small>{{\Carbon\Carbon::parse($doctor->created_at)->format('g:i a')}}</small></td>
								@if($doctor['status'])
									<td><a href="#" class="btn-sm btn-danger">Deactive</a></td>
								@else
									<td><a href="#" class="btn-sm btn-success">Active</a></td>
								@endif
								<td>
									<p><a href="#" class="btn-sm btn-info">View</a></p>
									<p><a href="{{ url('/admin/update/'.$doctor->id.'/doctor') }}" class="btn-sm btn-warning">Edit</a></p>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>			
</div>
@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('/new_assets/plugins/datatables/datatables.min.js') }}"></script>
@endsection