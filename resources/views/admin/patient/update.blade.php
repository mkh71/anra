@extends('layouts.admin')

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Update Patient</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Patients</a></li>
					<li class="breadcrumb-item active">Update Patient</li>
				</ul>
			</div>
			<!-- <div class="col-sm-5 col">
				<a href="add_doctor.html" class="btn btn-primary float-right mt-2">Add</a>
			</div> -->
		</div>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			@if(Session::has('message'))
				<p class="alert alert-success">{{ Session::get('message') }}</p>
			@endif			
		<form action="{{ url('/admin/patient/'.$patient[0]["id"].'/update') }}" method="POST" enctype="multipart/form-data" >
			@csrf
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Basic Information</h4>
					<div class="row form-row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Email <span class="text-danger">*</span></label>
								<input type="email" name="email" value="{{ $patient[0]["email"] }}" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Name <span class="text-danger">*</span></label>
								<input type="text" name="name" value="{{ $patient[0]["name"] }}" class="form-control" required>
								@error('name')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

                        <div class="col-md-6">
							<div class="form-group">
								<label>Country Code (eg. +1, +91)<span class="text-danger">*</span></label>
								<input type="text" class="form-control floating @error('country_code') is-invalid @enderror" value="{{ $patient[0]["country_code"] }}" required="" name="country_code">
								@error('country_code')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Phone Number<span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="phone" value="{{ $patient[0]["phone"] }}" required>
								@error('phone')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Gender<span class="text-danger">*</span></label>
								<select name="gender" class="form-control select" required="">
									<option value="">Select</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
								@error('gender')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-0">
								<label>Date of Birth<span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="dob" value="{{ $patient[0]["dob"] }}" required>
								@error('dob')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
                        
					</div>
				</div>
			</div>

			<div class="card contact-card">
				<div class="card-body">
					<h4 class="card-title">Contact Details</h4>
					<div class="row form-row">
                    <div class="col-md-6">
							<div class="form-group">
								<label>Address<span class="text-danger">*</span></label>
								<input type="text" name="address" value="{{ $patient[0]["user_address"]["address_line"] }}" class="form-control" required>
								@error('address')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Street<span class="text-danger">*</span></label>
								<input type="text" name="street" value="{{ $patient[0]["user_address"]["street"] }}" class="form-control" required>
								@error('street')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">City<span class="text-danger">*</span></label>
								<input type="text" name="city" value="{{ $patient[0]["user_address"]["city"] }}"" class="form-control" required>
								@error('city')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

                        <div class="col-md-6">
							<div class="form-group">
								<label class="control-label">State<span class="text-danger">*</span></label>
								<input type="text" name="state" value="{{ $patient[0]["user_address"]["state"] }}"" class="form-control" required>
								@error('state')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Country<span class="text-danger">*</span></label>
								<input type="text" name="country" value="{{ $patient[0]["user_address"]["country"] }}" class="form-control" required>
								@error('country')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Province<span class="text-danger">*</span></label>
								<input type="text" name="province" value="{{ $patient[0]["user_address"]["province"] }}" class="form-control" required>
								@error('province')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="submit-section submit-btn-bottom">
				<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
			</div>
		</form>	

		</div>
	</div>
@endsection

@section('javascript')
<script>
    var filter = [];

    const keypadZero = 48;
    const numpadZero = 96;

    for(var i = 0; i <= 9; i++){
      filter.push(i + keypadZero);
      filter.push(i + numpadZero);  
    }

    filter.push(8);     //backspace
    filter.push(9);     //tab
    filter.push(46);    //delete
    filter.push(37);    //left arrow
    filter.push(39);    //right arrow

    function replaceAll(src,search,replace){
      return src.split(search).join(replace);
    }

    function formatPhoneText(value){
      value = this.replaceAll(value.trim(),"-","");
      
      if(value.length > 3 && value.length <= 6) 
        value = value.slice(0,3) + "-" + value.slice(3);
      else if(value.length > 6) 
        value = value.slice(0,3) + "-" + value.slice(3,6) + "-" + value.slice(6);
      
      return value;
    }

    function validatePhone(p){
      var phoneRe = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
      var digits = p.replace(/\D/g, "");
      return phoneRe.test(digits);
    }

    function onKeyDown(e){  
      if(filter.indexOf(e.keyCode) < 0){
        e.preventDefault();
        return false;
      }  
    }

    function onKeyUp(e){
      var input = e.target;
      var formatted = formatPhoneText(input.value);
      var isError = (validatePhone(formatted) || formatted.length == 0);
      var color =  (isError) ? "gray" : "red";
      var borderWidth =  (isError)? "1px" : "3px";
      input.style.borderColor = color;
      input.style.borderWidth = borderWidth;
      input.value = formatted;
    }

    function setupPhoneFields(className){
      var lstPhoneFields = document.getElementsByClassName(className);
      for(var i=0; i < lstPhoneFields.length; i++){
        var input = lstPhoneFields[i];
        if(input.type.toLowerCase() == "text"){
          input.placeholder = "Phone (XXX-XXX-XXXX)";
          input.addEventListener("keydown", onKeyDown);
          input.addEventListener("keyup", onKeyUp);
        }
      }
    } 

    setupPhoneFields("phoneNumber");
</script>
@endsection