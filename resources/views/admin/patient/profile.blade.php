@extends('layouts.admin')

@section('css')
<!-- Fancybox CSS -->
<link rel="stylesheet" href="{{ url('/assets/plugins/fancybox/jquery.fancybox.min.css') }}">

<!-- Main CSS -->
<link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ url('/new_assets/plugins/datatables/datatables.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Patient Profile</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Patient</a></li>
					<li class="breadcrumb-item active">Profile</li>
				</ul>
			</div>
			<!-- <div class="col-sm-5 col">
				<a href="add_doctor.html" class="btn btn-primary float-right mt-2">Add</a>
			</div> -->
		</div>
	</div>
@endsection

@section('content')

	<div class="row">
		<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar dct-dashbd-lft">
		
			<!-- Profile Widget -->
			<div class="card widget-profile pat-widget-profile">
				<div class="card-body">
					<div class="pro-widget-content">
						<div class="profile-info-widget">
							<a href="#" class="booking-doc-img">
								<img src="{{ $patient[0]["profile_photo"]??'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png' }}" alt="User Image">
							</a>
							<div class="profile-det-info">
								<h3>{{ $patient[0]["name"]}}</h3>
								
								<div class="patient-details">
									<h5><b>Patient ID :</b> {{ $patient[0]["chart_no"] }}</h5>
									<h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> Newyork, United States</h5>
								</div>
							</div>
						</div>
					</div>
					<div class="patient-info">
						<ul>
							<li>Phone <span>{{ $patient[0]["phone"] }}</span></li>
							<li>Age <span>{{ \Carbon\Carbon::parse($patient[0]["dob"])->diff(\Carbon\Carbon::now())->format('%y Years');}}, {{ $patient[0]["gender"]}}</span></li>
							<li>Blood Group <span>---</span></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Profile Widget -->
			
			<!-- Last Booking -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Last Booking</h4>
				</div>
				@if(count($patient[0]["last_appointment"]))
					<ul class="list-group list-group-flush">
						<li class="list-group-item">
							<div class="media align-items-center">
								<div class="mr-3">
									<img alt="Image placeholder" src="{{ $patient[0]["last_appointment"][0]["doctor_info"]["profile_photo"]}}" class="avatar  rounded-circle">
								</div>

								<div class="media-body">
									<h5 class="d-block mb-0">Dr. {{ $patient[0]["last_appointment"][0]["doctor_info"]["name"] }} </h5>
									<span class="d-block text-sm text-muted">Dentist</span>
									<span class="d-block text-sm text-muted">{{ \Carbon\Carbon::parse($patient[0]["last_appointment"][0]["book_on"])->format('F j, Y') }} {{ \Carbon\Carbon::parse($patient[0]["last_appointment"][0]["start_time"])->format('g:i a') }} </span>
								</div>
							</div>
						</li>
					</ul>
				@else
					<li class="list-group-item">
						<h5>
							No records!
						</h5>
					</li>
				@endif
			</div>
			<!-- /Last Booking -->
			
		</div>

		<div class="col-md-7 col-lg-8 col-xl-9 dct-appoinment">
			<div class="card">
				<div class="card-body pt-0">
					<div class="user-tabs">
						<ul class="nav nav-tabs nav-tabs-bottom nav-justified flex-wrap">
							<li class="nav-item">
								<a class="nav-link active" href="#pat_appointments" data-toggle="tab">Appointments</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#pres" data-toggle="tab"><span>Prescription</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#medical" data-toggle="tab"><span class="med-records">Medical Records</span></a>
							</li>
							
						</ul>
					</div>
					<div class="tab-content">
						
						<!-- Appointment Tab -->
						<div id="pat_appointments" class="tab-pane fade show active">
							<div class="card card-table mb-0">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>Doctor</th>
													<th>Appt Date</th>
													<th>Booking Date</th>
													<th>Status</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
											@if(count($patient[0]["patient_appointment"]))
												@foreach($patient[0]["patient_appointment"] as $appointment)
												<tr>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="{{ $appointment["doctor_info"]["profile_photo"] }}" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. {{ $appointment["doctor_info"]["name"] }} <span>Dental</span></a>
														</h2>
													</td>
													<td>{{ \Carbon\Carbon::parse($appointment["book_on"])->format('F j, Y') }}<span class="d-block text-info">{{ \Carbon\Carbon::parse($appointment["start_time"])->format('g:i a') }}</span></td>
													<td>{{ \Carbon\Carbon::parse($appointment["created_at"])->format('F j, Y') }}</td>
													<td><span class="badge badge-pill bg-success-light">{{ $appointment["status"]}}</span></td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-success-light">
																<i class="far fa-edit"></i> Edit
															</a>
														</div>
													</td>
												</tr>
												@endforeach
											@else
											<tr>
													<td colspan="5">
														<h2>
															No records!
														</h2>
													</td>
												</tr>
											@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- /Appointment Tab -->
						
						<!-- Prescription Tab -->
						<div class="tab-pane fade" id="pres">
							<div class="text-right">
								<a href="add-prescription.html" class="add-new-btn">Add Prescription</a>
							</div>
							<div class="card card-table mb-0">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>Date </th>
													<th>Name</th>									
													<th>Created by </th>
													<th></th>
												</tr>     
											</thead>
											<tbody>
												<tr>
													<td>14 Nov 2019</td>
													<td>Prescription 1</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-01.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Ruby Perrin <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>13 Nov 2019</td>
													<td>Prescription 2</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-02.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Darren Elder <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
															<a href="edit-prescription.html" class="btn btn-sm bg-success-light">
																<i class="fas fa-edit"></i> Edit
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
																<i class="far fa-trash-alt"></i> Delete
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>12 Nov 2019</td>
													<td>Prescription 3</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-03.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Deborah Angel <span>Cardiology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>11 Nov 2019</td>
													<td>Prescription 4</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-04.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Sofia Brient <span>Urology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>10 Nov 2019</td>
													<td>Prescription 5</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-05.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Marvin Campbell <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>9 Nov 2019</td>
													<td>Prescription 6</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-06.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Katharine Berthold <span>Orthopaedics</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>8 Nov 2019</td>
													<td>Prescription 7</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-07.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Linda Tobin <span>Neurology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>7 Nov 2019</td>
													<td>Prescription 8</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-08.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Paul Richard <span>Dermatology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>6 Nov 2019</td>
													<td>Prescription 9</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-09.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. John Gibbs <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>5 Nov 2019</td>
													<td>Prescription 10</td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-10.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Olga Barlow <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
											</tbody>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- /Prescription Tab -->

						<!-- Medical Records Tab -->
						<div class="tab-pane fade" id="medical">
							<div class="text-right">		
								<a href="#" class="add-new-btn" data-toggle="modal" data-target="#add_medical_records">Add Medical Records</a>
							</div>
							<div class="card card-table mb-0">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>ID</th>
													<th>Date </th>
													<th>Description</th>
													<th>Attachment</th>
													<th>Created</th>
													<th></th>
												</tr>     
											</thead>
											<tbody>
												<tr>
													<td><a href="javascript:void(0);">#MR-0010</a></td>
													<td>14 Nov 2019</td>
													<td>Dental Filling</td>
													<td><a href="#">dental-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-01.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Ruby Perrin <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0009</a></td>
													<td>13 Nov 2019</td>
													<td>Teeth Cleaning</td>
													<td><a href="#">dental-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-02.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Darren Elder <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
															<a href="edit-prescription.html" class="btn btn-sm bg-success-light" data-toggle="modal" data-target="#add_medical_records">
																<i class="fas fa-edit"></i> Edit
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
																<i class="far fa-trash-alt"></i> Delete
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0008</a></td>
													<td>12 Nov 2019</td>
													<td>General Checkup</td>
													<td><a href="#">cardio-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-03.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Deborah Angel <span>Cardiology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0007</a></td>
													<td>11 Nov 2019</td>
													<td>General Test</td>
													<td><a href="#">general-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-04.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Sofia Brient <span>Urology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0006</a></td>
													<td>10 Nov 2019</td>
													<td>Eye Test</td>
													<td><a href="#">eye-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-05.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Marvin Campbell <span>Ophthalmology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0005</a></td>
													<td>9 Nov 2019</td>
													<td>Leg Pain</td>
													<td><a href="#">ortho-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-06.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Katharine Berthold <span>Orthopaedics</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0004</a></td>
													<td>8 Nov 2019</td>
													<td>Head pain</td>
													<td><a href="#">neuro-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-07.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Linda Tobin <span>Neurology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0003</a></td>
													<td>7 Nov 2019</td>
													<td>Skin Alergy</td>
													<td><a href="#">alergy-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-08.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Paul Richard <span>Dermatology</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0002</a></td>
													<td>6 Nov 2019</td>
													<td>Dental Removing</td>
													<td><a href="#">dental-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-09.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. John Gibbs <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td><a href="javascript:void(0);">#MR-0001</a></td>
													<td>5 Nov 2019</td>
													<td>Dental Filling</td>
													<td><a href="#">dental-test.pdf</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
																<img class="avatar-img rounded-circle" src="/assets/img/doctors/doctor-thumb-10.jpg" alt="User Image">
															</a>
															<a href="doctor-profile.html">Dr. Olga Barlow <span>Dental</span></a>
														</h2>
													</td>
													<td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
																<i class="fas fa-print"></i> Print
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a>
														</div>
													</td>
												</tr>
											</tbody>  	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- /Medical Records Tab -->
						
						
								
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('/new_assets/plugins/datatables/datatables.min.js') }}"></script>
@endsection