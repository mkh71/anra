@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('/new_assets/plugins/datatables/datatables.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">List of Patient</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Users</a></li>
					<li class="breadcrumb-item active">Patient</li>
				</ul>
			</div>
			<div class="col-sm-5 col">
				<a href="{{ url('admin/patient/create')}}" class="btn btn-primary float-right mt-2">Add</a>
			</div>
		</div>
		@if(Session::has('message'))
			<p class="alert alert-success">{{ Session::get('message') }}</p>
		@endif
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Patient ID</th>
								<th>Patient Name</th>
								<th>Age</th>
								<th>Phone</th>
								<th>Last Visit</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($patients as $patient)
							<tr>
								<td>{{ $patient['chart_no'] }}</td>
								<td>
									<h2 class="table-avatar">
										<a href="{{ url('/admin/patient/'.$patient['id'].'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ $patient['profile_photo']??'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png' }}" alt="User Image"></a>
										<a href="{{ url('/admin/patient/'.$patient['id'].'/profile') }}">{{ $patient["name"] }}</a>
									</h2>
								</td>
								<td>{{ \Carbon\Carbon::parse($patient['dob'])->diff(\Carbon\Carbon::now())->format('%y years');}}</td>
								<td>{{ $patient["phone"] }} </td>
								<td>{{ (isset($patient['last_appointment'][0]['book_on'])) ? $patient['last_appointment'][0]['book_on'] : "N/A" }}</td>
								<td>
									<p><a href="{{ url('/admin/patient/'.$patient['id'].'/update') }}" class="btn-sm btn-warning">Edit</a></p>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>			
</div>

@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('/new_assets/plugins/datatables/datatables.min.js') }}"></script>
@endsection