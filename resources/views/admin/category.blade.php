@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('assets/css/feathericon.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Specialities</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item active">Specialities</li>
				</ul>
			</div>
			<div class="col-sm-5 col">
				<a href="#Add_Specialities_details" data-toggle="modal" class="btn btn-primary float-right mt-2">Add</a>
			</div>
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Specialities</th>
								<th class="text-right">Actions</th>
							</tr>
						</thead>
						<tbody>
						@php $count = 0; @endphp
						@foreach($categories as $category)
						<tr>
							<td>{{ ++$count }}</td>
							
							<td>
								<h2 class="table-avatar">
									<a href="#" class="avatar avatar-sm mr-2">
										<img class="avatar-img" src="{{ $category->image }}" alt="Speciality">
									</a>
									<a href="#">{{ $category->name }}</a>
								</h2>
							</td>
						
							<td class="text-right">
								<div class="actions">
									<a class="btn btn-sm bg-success-light" data-toggle="modal" href="#edit_specialities_details_{{$count}}">
										<i class="fe fe-pencil"></i> Edit
									</a>
									<a  data-toggle="modal" href="#delete_modal_{{$count}}" class="btn btn-sm bg-danger-light">
										<i class="fe fe-trash"></i> Delete
									</a>
								</div>
							</td>
						</tr>

						<!-- Edit Details Modal -->
							<div class="modal fade" id="edit_specialities_details_{{$count}}" aria-hidden="true" role="dialog">
								<div class="modal-dialog modal-dialog-centered" role="document" >
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title">Edit Specialities</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form>
												<div class="row form-row">
													<div class="col-12 col-sm-6">
														<div class="form-group">
															<label>Specialities</label>
															<input type="text" class="form-control" value="{{ $category->name }}" required="">
														</div>
													</div>
													<div class="col-12 col-sm-6">
														<div class="form-group">
															<label>Image</label>
															<input type="file"  class="form-control">
														</div>
													</div>
													
												</div>
												<button type="submit" class="btn btn-primary btn-block">Save Changes</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						<!-- /Edit Details Modal -->


						<!-- Delete Modal -->
						<div class="modal fade" id="delete_modal_{{$count}}" aria-hidden="true" role="dialog">
							<div class="modal-dialog modal-dialog-centered" role="document" >
								<div class="modal-content">
								<!--	<div class="modal-header">
										<h5 class="modal-title">Delete</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>-->
									<div class="modal-body">
										<div class="form-content p-2">
											<h4 class="modal-title">Delete</h4>
											<p class="mb-4">Are you sure want to delete <strong class="text-danger">{{ $category->name }}</strong> ?</p>
											<button type="button" class="btn btn-primary" onclick="deleteCategory()">Save </button>
											<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>			
</div>
@endsection

@section('popup')
<div class="modal fade" id="Add_Specialities_details" aria-hidden="true" role="dialog">
	@if(Session::has('message'))
		<p class="alert alert-success">{{ Session::get('message') }}</p>
	@endif
	<div class="modal-dialog modal-dialog-centered" role="document" >
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Specialities</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{ url('/admin/specialities') }}" method="POST" enctype="multipart/form-data" >
				@csrf
					<div class="row form-row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label>Specialities</label>
								<input type="text" name="name" class="form-control" required="">
								@error('name')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label>Image</label>
								<input type="file"  name="image" class="form-control" required="">
							</div>
						</div>
						
					</div>
					<button type="submit" class="btn btn-primary btn-block">Save Changes</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /ADD Modal -->


@endsection