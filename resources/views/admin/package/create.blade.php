@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Edit Package</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Package</a></li>
					<li class="breadcrumb-item active">Edit Package</li>
				</ul>
			</div>

		</div>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			@include('status_message')			
		<form action="{{ url('/admin/package/store') }}" method="POST" enctype="multipart/form-data" >
			@csrf
			
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Package Information</h4>
					<div class="row form-row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Package Name <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Package Name" name="name" value="{{ old('name') }}" required="">
								@error('name')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>First Year Premium <span class="text-danger">*</span></label>
								<input type="number" class="form-control @error('first_year_premium') is-invalid @enderror" placeholder="Package Name" name="first_year_premium" value="{{ old('first_year_premium') }}" required="">
								@error('first_year_premium')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Following Year Premium <span class="text-danger">*</span></label>
								<input type="number" class="form-control @error('following_year_premium') is-invalid @enderror" placeholder="Package Name" name="following_year_premium" value="{{ old('following_year_premium') }}" required="">
								@error('following_year_premium')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Details <span class="text-danger">*</span></label>
								<textarea name="details" id="summernote" style="height: 200px;" required=""></textarea>
								@error('details')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Valid Days <span class="text-danger">*</span></label>
								<input type="number" class="form-control @error('valid_days') is-invalid @enderror" placeholder="Package Name" name="valid_days" value="{{ old('valid_days') }}" required="">
								@error('valid_days')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Image </label>
								
								<input type="file" name="file" class="form-control" required="">
								@error('file')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>        

						<div class="col-md-12">
							<div class="form-group">
								<label>Status <span class="text-danger">*</span></label>
								<select name="status" class="form-control" required="">
									<option value="">Please Select</option>
									<option value="1" @if(old('status')) selected @endif>Active</option>
									<option value="0" @if(!old('status')) selected @endif>Deactive</option>
								</select>
								@error('status')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>             	
                        
					</div>
				</div>
			</div>

			
			<div class="submit-section submit-btn-bottom">
				<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
			</div>
		</form>	

		</div>
	</div>
@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.js') }}"></script>
<script>
	$(document).ready(function() {
  		$('#summernote').summernote({
	       height: 200,
	      maximumImageFileSize: 300*1024, // 500 KB
	      callbacks:{
	        onImageUploadError: function(msg){
	           alert(msg + ' (300 KB)');
	        }
	      }
	    });
	});
</script>	
@endsection