@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('assets/css/feathericon.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Packages</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Packages</li>
				</ul>
			</div>
			<div class="col-sm-5 col">
				<a href="{{ url('admin/package/add') }}" class="btn btn-primary float-right mt-2">Add</a>
			</div>
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		@include('status_message')
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>First Year Premium</th>
								<th>Following Year Premium</th>
								<th>Image</th>
								<th>Valid Days</th>
								<th>Status</th>
								<th class="text-right">Actions</th>
							</tr>
						</thead>
						<tbody>

						@foreach($packages as $key => $package)
						<tr>
							<td>{{ $key+1 }}</td>
							
							<td>
								<h2 class="table-avatar">
									{{ $package->name }}
								</h2>
							</td>

							<td width="20%">
								${{ $package->first_year_premium }}	
							</td>

							<td width="20%">
								${{ $package->following_year_premium }}	
							</td>

							<td>
								@if(null != $package->image)
									<img src="{{ $package->image }}" height="100px" width="100px"> 
								@else
									N/A
								@endif
							</td>

							<td width="20%">
								{{ $package->valid_days }}	
							</td>


							<td width="20%">
								@if($package->status)
									Active
								@else
									Deactive
								@endif 
	
							</td>
						
							<td class="text-right">
								<div class="actions">
									<a class="btn btn-sm bg-success-light" href="{{ url('/admin/package/'.Crypt::encrypt($package->id).'/edit') }}">
										<i class="fe fe-pencil"></i> Edit
									</a>
								</div>
							</td>
						</tr>

					
						@endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
	</div>			
</div>
@endsection