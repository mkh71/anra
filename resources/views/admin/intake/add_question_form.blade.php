@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Question</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Intake Form</a></li>
					<li class="breadcrumb-item active">Add Question</li>
				</ul>
			</div>

		</div>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			@include('status_message')			
		<form action="{{ url('/admin/intake/question/create') }}" method="POST" enctype="multipart/form-data" >
			@csrf
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Add Question</h4>
					<div class="row form-row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Select Form <span class="text-danger">*</span></label>
								<select class="form-control form_select" name="form_id" required="">
									<option value="">Please Select</option>
									@foreach($data['available_forms'] as $form)
										<option value="{{ $form->id }}" @if(old('form_id') == $form->id ) selected @endif>{{ $form->name }}</option>
									@endforeach
								</select>
								@error('form')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Select Form Category<span class="text-danger">*</span></label>
								<select class="form-control form_categ_select" name="form_categ_id" required="" readonly>
									
								</select>
								@error('form')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Question <span class="text-danger">*</span></label>
								<input type="text" name="question" value="{{ old('title') }}" class="form-control" >
								@error('title')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-12">
							<h4>Question Options</h4>
						</div><br><br>
						<div class="col-md-6">
							<div class="form-group">
								<label>Field Type <span class="text-danger">*</span></label>
								<select class="form-control" name="field_type" required="">
									<option value="">Please Select</option>
									@foreach($data['field_types'] as $field_type)
										<option value="{{ $field_type->id }}" @if(old('form_id') == $field_type->id ) selected @endif>{{ $field_type->name }}</option>
									@endforeach
								</select>
								@error('description')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Field Action <span class="text-danger">*</span></label>
								<select class="form-control" name="field_action" required="">
									<option value="">Please Select</option>
									@foreach($data['field_action'] as $field_action)
										<option value="{{ $field_action->id }}" @if(old('form_id') == $field_action->id ) selected @endif>{{ $field_action->name }}</option>
									@endforeach
								</select>
								@error('description')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>      


						<div class="col-md-12 question_options_div">
							<div class="row question-option-count">
								<div class="col-md-6">
									<div class="form-group">
										<label>Option <span class="text-danger">*</span></label>
										<input type="text" value="" name="question_options[]" class="form-control">	
									</div>
								</div> 

								<div class="col-md-2">
									<div class="form-group">
										<label>Has Sub Form <span class="text-danger">*</span></label>
										<select name="has_sub_question[]" class="form-control">
											<option value="0" selected>No</option>
											<option value="1">Yes</option>
										</select>	
									</div>
								</div> 
							</div>	
						</div>                     	
							

						<div class="add-more">
							<a href="javascript:void(0);" class="add-question-option"><i class="fa fa-plus-circle"></i> Add More</a>
						</div>
						
                        
					</div>
				</div>
			</div>

			
			<div class="submit-section submit-btn-bottom">
				<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
			</div>
		</form>	

		</div>
	</div>
@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.js') }}"></script>
<script>
	$(document).ready(function() {
  		$('#summernote').summernote({
	       height: 200,
	      maximumImageFileSize: 300*1024, // 500 KB
	      callbacks:{
	        onImageUploadError: function(msg){
	           alert(msg + ' (300 KB)');
	        }
	      }
	    });
	});
</script>	

<script>
	$(".question_options_div").on('click','.trash', function () {
		$(this).closest('.question-option-count').remove();
		return false;
    });

    $(".add-question-option").on('click', function () {
		
		var educationcontent = '<div class="row question-option-count"><div class="col-md-6"><div class="form-group"><label>Option <span class="text-danger">*</span></label><input type="text" value="" name="question_options[]" class="form-control"></div></div>' +
			'<div class="col-md-2">'+
									'<div class="form-group">'+
										'<label>Has Sub Form <span class="text-danger">*</span></label>'+
										'<select name="has_sub_question[]" class="form-control">'+
											'<option value="0" selected>No</option>'+
											'<option value="1">Yes</option>'+
										'</select>'+	
									'</div>'+
								'</div>'
			+'<div class="col-6 col-md-6 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>' +
		'</div>';
		
        $(".question_options_div").append(educationcontent);
        return false;
    });

    $(document).ready(function(){
		$(".form_select").change(function(){
			var form_id = $(this).val();
			if(form_id != '') {
				getFormCategories(form_id);
			}
		});


  	});

    function getFormCategories(form_id) {
		$.ajax({   
	  		type: 'GET',
	  		url: '/admin/intake/form/categories?form_id='+form_id,
	  		success: function (data) {
	  			$('.form_categ_select').html(data);
	  			$('.form_categ_select').attr("readonly", false);
	  		},
	  		error: function() { 
	    		console.log(data);
	  		}
		});
	}

</script>

@endsection