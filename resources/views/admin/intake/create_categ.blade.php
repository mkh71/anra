@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Intake Category</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javascript:(0);">Create</a></li>
					<li class="breadcrumb-item active">Create Category</li>
				</ul>
			</div>

		</div>
	</div>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			@if(Session::has('message'))
				<p class="alert alert-success">{{ Session::get('message') }}</p>
			@endif			
		<form action="{{ url('/admin/intake/form/category/save') }}" method="POST" enctype="multipart/form-data" >
			@csrf
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Intake Form Category</h4>
					<div class="row form-row">
						<input type="hidden" value="{{ $form_id }}" name="form_id">
						<div class="col-md-12">
							<div class="form-group">
								<label>Name <span class="text-danger">*</span></label>
								<input type="text" name="name" value="{{ old('name') }}" class="form-control" required>
								@error('name')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Status </label>
								<select name="status" class="form-control" required="">
									<option value="">Please Select</option>
									<option value="1">Active</option>
									<option value="0">Deactive</option>
								</select>
								@error('status')
									<div class="text-danger text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>                     	
                        
					</div>
				</div>
			</div>

			
			<div class="submit-section submit-btn-bottom">
				<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
			</div>
		</form>	

		</div>
	</div>
@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/summernote/dist/summernote-bs4.js') }}"></script>
<script>
	$(document).ready(function() {
  		$('#summernote').summernote({
	       height: 200,
	      maximumImageFileSize: 300*1024, // 500 KB
	      callbacks:{
	        onImageUploadError: function(msg){
	           alert(msg + ' (300 KB)');
	        }
	      }
	    });
	});
</script>	
@endsection