@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="{{ url('assets/css/feathericon.min.css') }}">
@endsection

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-7 col-auto">
				<h3 class="page-title">Intake</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Category Questions</li>
				</ul>
			</div>
			<!-- <div class="col-sm-5 col">
				<a href="{{ url('admin/intake/form/create') }}" class="btn btn-primary float-right mt-2">Add</a>
			</div> -->
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		@include('status_message')
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Question</th>
								<th>Field Options.</th>
								<th>Status</th>
								<th class="text-right">Actions</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($form_questions as $question)
						<tr>
							<td>{{ $question->id }}</td>
							<td>
								<h2 class="table-avatar">
									{{ $question->field_name }}
								</h2>
							</td>

							<td>
								@if(null != $question->field_options && null != $question->field_options[0]['title'])
									@foreach($question->field_options as $field_option)
										{{ $field_option['title'] }}<br>
									@endforeach
								@else
									N/A
								@endif
							</td>

							
							<td width="20%">
                            {{ ($question->status) ? "Active" : "In Active" }}
							</td>
						
							<td class="text-right">
								<div class="actions">
									<a class="btn btn-sm bg-success-light" href="{{ url('/admin/intake/question/'.Crypt::encrypt($question->id).'/edit') }}">
										<i class="fe fe-pencil"></i> Edit
									</a>

									<a class="btn btn-sm bg-danger-light" href="{{ url('/admin/intake/question/'.Crypt::encrypt($question->id).'/delete') }}">
										<i class="fe fe-trash"></i> Delete
									</a>
								</div>
							</td>
						</tr>
                        @endforeach
						</tbody> 
					</table>
				</div>
			</div>
		</div>
	</div>			
</div>
@endsection