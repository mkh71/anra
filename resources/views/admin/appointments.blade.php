@extends('layouts.admin')

@section('breadcrumb')
	<div class="page-header">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">Appointments</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item active">Appointments</li>
				</ul>
			</div>
		</div>
	</div>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
	
		<!-- Recent Orders -->
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Doctor Name</th>
								<th>Speciality</th>
								<th>Patient Name</th>
								<th>Apointment Time</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						@foreach($appointments as $app)
						<tr>
								<td>
									<h2 class="table-avatar">
										<a href="{{ url('/admin/doctor/'.$app['doctor_id'].'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src={{ $app["profile_photo"] }} alt="User Image"></a>
										<a href="{{ url('/admin/doctor/'.$app['doctor_id'].'/profile') }}">{{ $app["name"] }}</a>
									</h2>
								</td>
								<td>
									@foreach($app['specialities'] as $key => $specialities)
										<span class="btn btn-rounded btn-primary btn-sm">{{ $specialities["speciality_details"]["name"] }}</span>
									@endforeach
								</td> 
								<td>
									<h2 class="table-avatar">
										<a href="{{ url('/admin/patient/'.$app['patient_id'].'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src={{ $app["patient_photo"] }} alt="User Image"></a>
										<a href="{{ url('/admin/patient/'.$app['patient_id'].'/profile') }}">{{ $app["patient_name"] }} </a>
									</h2>
								</td>
								<td>{{ \Carbon\Carbon::parse($app["book_on"])->format('F j, Y') }}<span class="text-primary d-block">{{ \Carbon\Carbon::parse($app["start_time"])->format('g:i a') }} - {{ \Carbon\Carbon::parse($app["end_time"])->format('g:i a') }}</span></td>
								<td>
									<div class="status-toggle">
										<input type="checkbox" id="status_1" class="check" checked>
										<label for="status_1" class="checktoggle">checkbox</label>
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /Recent Orders -->
		
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ url('/new_assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('/new_assets/plugins/datatables/datatables.min.js') }}"></script>
@endsection