@extends('layouts.doctor')

@section('breadcrumb')
<div class="breadcrumb-bar">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-12 col-12">
				<nav aria-label="breadcrumb" class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Appointments</li>
					</ol>
				</nav>
				<h2 class="breadcrumb-title">Appointments</h2>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="col-md-7 col-lg-8 col-xl-9">
	<div class="appointments">
		@if(count($appointments->toArray()) > 0) 
			@foreach($appointments as $appointment)
				<div class="appointment-list">
					<div class="profile-info-widget">
						<a href="{{ url('/doctor/patient/'.$appointment->patient_id.'/profile') }}" class="booking-doc-img">
							<img src="{{ $appointment->patient_info->profile_photo }}" alt="User Image">
						</a>
						<div class="profile-det-info">
							<h3><a href="{{ url('/doctor/patient/'.$appointment->patient_id.'/profile') }}">{{ $appointment->patient_info->name }}</a></h3>
							<div class="patient-details">
								<h5><i class="far fa-clock"></i> {{ \Carbon\Carbon::parse($appointment->book_on)->format('F j, Y') }}, {{ \Carbon\Carbon::parse($appointment->start_time)->format('g:i a') }}</h5>
								<h5><i class="fas fa-envelope"></i> {{ $appointment->patient_info->email }}</h5>
								<h5 class="mb-0"><i class="fas fa-phone"></i> {{ $appointment->patient_info->phone }}</h5>
							</div>
						</div>
					</div>
					<div class="appointment-action">
						<a href="#" class="btn btn-sm bg-info-light" data-toggle="modal" data-target="#appt_details">
							<i class="far fa-eye"></i> View
						</a>
						<a href="javascript:void(0);" class="btn btn-sm bg-success-light">
							<i class="fas fa-check"></i> Accept
						</a>
						<a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
							<i class="fas fa-times"></i> Cancel
						</a>
					</div>
				</div>
			@endforeach
		@else
			<h3 class="text-center">Sorry ! Nothing Found.</h3>
		@endif	
	</div>
</div>
@endsection