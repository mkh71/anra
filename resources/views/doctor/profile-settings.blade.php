@extends('layouts.doctor')

@section('css')
	<link rel="stylesheet" href="{{ url('/assets/plugins/select2/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ url('/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}">
	<link rel="stylesheet" href="{{ url('/assets/plugins/dropzone/dropzone.min.css') }}">
	<link rel="stylesheet" href="{{ url('/assets/plugins/daterangepicker/daterangepicker.css') }}">
@endsection

@section('breadcrumb')
<div class="breadcrumb-bar">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-12 col-12">
				<nav aria-label="breadcrumb" class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Profile Settings</li>
					</ol>
				</nav>
				<h2 class="breadcrumb-title">Profile Settings</h2>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
	<div class="col-md-7 col-lg-8 col-xl-9">
		@if(Session::has('message'))
			<p class="alert alert-success">{{ Session::get('message') }}</p>
		@endif			
		<form action="{{ url('/doctor/update-profile') }}" method="POST">
			@csrf
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Basic Information</h4>
					<div class="row form-row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="change-avatar">
									<div class="profile-img">
										<img src="{{ Auth::User()->profile_photo }}" alt="User Image">
									</div>
									<div class="upload-img">
										<div class="change-photo-btn">
											<span><i class="fa fa-upload"></i> Upload Photo</span>
											<input type="file" class="upload">
										</div>
										<small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Email <span class="text-danger">*</span></label>
								<input type="email" name="email" value='{{ $doctor_profile[0]["email"] }}' class="form-control" readonly>
								<input type="hidden" name="id" value='{{ $doctor_profile[0]["id"] }}' >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Name <span class="text-danger">*</span></label>
								<input type="text" name="name" value='{{ $doctor_profile[0]["name"] }}' class="form-control" required>
								@error('name')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Phone Number</label>
								<input type="text" class="form-control" name="phone" value='{{ $doctor_profile[0]["phone"] }}' required>
								@error('phone')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Gender</label>
								<select name="gender" class="form-control select">
									<option value="">Select</option>
									<option value="male"
										@if ($doctor_profile[0]["gender"] == "male")
             								{{'selected="selected"'}}
           								@endif
									>
									Male
									</option>
									<option value="female"
										@if ($doctor_profile[0]["gender"] == "female")
             								{{'selected="selected"'}}
           								@endif
									>
									Female
									</option>
								</select>
								@error('gender')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-0">
								<label>Date of Birth</label>
								<input type="text" class="form-control" id="datepicker" name="dob" value='{{ $doctor_profile[0]["dob"] }}' required="">

								<!-- <input type="date" class="form-control" name="dob" value='' required> -->
								@error('dob')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /Basic Information -->
			
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">About Me</h4>
					<div class="form-group mb-0">
						<label>Biography</label>
						<textarea class="form-control" name="about_me" rows="5">@php echo $doctor_profile[0]["about_me"]; @endphp</textarea>
					</div>
				</div>
			</div>
		
			<div class="card contact-card">
				<div class="card-body">
					<h4 class="card-title">Contact Details</h4>
					<div class="row form-row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Address</label>
								<input type="text" name="address" value='{{ $doctor_profile[0]["address"] }}' class="form-control">
								@error('address')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">City</label>
								<input type="text" name="city" value='{{ $doctor_profile[0]["city"] }}' class="form-control">
								@error('city')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">State / Province</label>
								<input type="text" name="state" value='{{ $doctor_profile[0]["state"] }}' class="form-control">
								@error('state')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Country</label>
								<input type="text" name="country" value='{{ $doctor_profile[0]["country"] }}' class="form-control">
								@error('country')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Postal Code</label>
								<input type="text" name="postal_code" value='{{ $doctor_profile[0]["postal_code"] }}' class="form-control">
								@error('postal_code')
									<div class="text-red-300 text-xs mt-4">{{ $message }}</div>
								@enderror
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card services-card">
				<div class="card-body">
					<h4 class="card-title">Specialization</h4>
					
					<div class="form-group mb-0">
						<label>Specialization </label>
						<select name="specialization" id="js-example-basic-hide-search-multi" class="form-control input-tags" multiple>
							@foreach($categories as $key => $category)
								<option value="{{ $category->id }}" @if(in_array($category->id, $selected)) selected @endif>{{ $category->name }}</option>
							@endforeach
						</select>
						<small class="form-text text-muted">Note : Type & Press  enter to add new specialization</small>
					</div> 
				</div>              
			</div>
			
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Education</h4>
					<div class="education-info">
						<div class="row form-row education-cont">
							<div class="col-12 col-md-10 col-lg-11">
								<div class="row form-row">
									@foreach($doctor_profile[0]['educations'] as $education)
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>Degree</label>
												<input type="text" value={{ $education['degree'] }} name="degree[]" class="form-control" required>
											</div> 
										</div>
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>College/Institute</label>
												<input type="text" value={{ $education['college'] }} name="college[]" class="form-control" required>
											</div> 
										</div>
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>Starting Year</label>
												<input type="text" value={{ $education['starting_year'] }} name="starting_year[]" class="form-control" required>
											</div> 
										</div>
										<div class="col-12 col-md-6 col-lg-3">
											<div class="form-group">
												<label>Year of Completion</label>
												<input type="text" value={{ $education['completion_year'] }} name="completion_year[]" class="form-control" required>
											</div> 
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="add-more">
						<a href="javascript:void(0);" class="add-education"><i class="fa fa-plus-circle"></i> Add More</a>
					</div>
				</div>
			</div>
			<!-- /Education -->
		
			<!-- Experience -->
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Experience</h4>
					<div class="experience-info">
						<div class="row form-row experience-cont">
							<div class="col-12 col-md-10 col-lg-11">
							@foreach($doctor_profile[0]['experiences'] as $experience)
								<div class="row form-row">
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>Hospital Name</label>
											<input type="text" value={{ $experience['hospital_name'] }} name="hospital_name[]" class="form-control" required>
										</div> 
									</div>
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>From</label>
											<input type="text" value={{ $experience['started_from'] }} name="started_from[]" class="form-control" required>
										</div> 
									</div>
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>To</label>
											<input type="text" value={{ $experience['end_to'] }} name="end_to[]" class="form-control" required>
										</div> 
									</div>
									<div class="col-12 col-md-6 col-lg-3">
										<div class="form-group">
											<label>Designation</label>
											<input type="text" value={{ $experience['designation'] }} name="designation[]" class="form-control" required>
										</div> 
									</div>
								</div>
							@endforeach
							</div>
						</div>
					</div>
					<div class="add-more">
						<a href="javascript:void(0);" class="add-experience"><i class="fa fa-plus-circle"></i> Add More</a>
					</div>
				</div>
			</div>
			<!-- /Experience -->
			
			<!-- Awards -->
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Awards</h4>
					<div class="awards-info">
					@foreach($doctor_profile[0]['awards'] as $award)
						<div class="row form-row awards-cont">
							<div class="col-12 col-md-5">
								<div class="form-group">
									<label>Awards</label>
									<input type="text" value={{ $award["award_name"] }} name="award_name[]" class="form-control" required>
								</div> 
							</div>
							<div class="col-12 col-md-5">
								<div class="form-group">
									<label>Year</label>
									<input type="text" value={{ $award["award_year"] }} name="award_year[]" class="form-control" required>
								</div> 
							</div>
						</div>
					@endforeach
					</div>
					<div class="add-more">
						<a href="javascript:void(0);" class="add-award"><i class="fa fa-plus-circle"></i> Add More</a>
					</div>
				</div>
			</div>
			<!-- /Awards -->
			
			
			<div class="submit-section submit-btn-bottom">
				<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
			</div>
		</form>	
	</div>

@endsection

@section('javascript')
<script src="{{ url('/assets/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ url('/assets/plugins/dropzone/dropzone.min.js') }}"></script>
<script src="{{ url('/assets/js/profile-settings.js') }}"></script>
<script src="{{ url('/assets/plugins/moment/moment.js') }}"></script> 
<script src="{{ url('/assets/plugins/daterangepicker/daterangepicker.js') }}"></script> 

<script>
	$("#datepicker").daterangepicker({
	    autoclose: true,
	    todayBtn: false,
	    timePicker: false,
	    singleDatePicker: true, //<==HERE
	    // minDate:new Date(),
	    locale: {
	      format: 'YYYY-MM-DD'
	    }
	});

	$('#js-example-basic-hide-search-multi').select2();

	$('#js-example-basic-hide-search-multi').on('select2:opening select2:closing', function( event ) {
	    var $searchfield = $(this).parent().find('.select2-search__field');
	    $searchfield.prop('disabled', true);
	});
</script>
@endsection