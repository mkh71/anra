@extends('layouts.doctor')

@section('breadcrumb')
<div class="breadcrumb-bar">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-12 col-12">
				<nav aria-label="breadcrumb" class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Schedule Timings</li>
					</ol>
				</nav>
				<h2 class="breadcrumb-title">Schedule Timings</h2>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
	<div class="col-md-7 col-lg-8 col-xl-9">
		@if(Session::has('message'))
			<p class="alert alert-success">{{ Session::get('message') }}</p>
		@endif				 
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Schedule Timings</h4>
						<div class="profile-box">
							<!-- <div class="row">

								<div class="col-lg-4">
									<div class="form-group">               
										<label>Timing Slot Duration</label>
										<select class="select form-control" id="time_slot" required>
											<option value="">--Select--</option>
											<option value="15 minutes">15 mins</option>
											<option value="30 minutes">30 mins</option>  
											<option value="45 minutes">45 mins</option>
											<option value="1 Hour">1 Hour</option>
										</select>
									</div>
								</div>

							</div>      -->
							<div class="row">
								<div class="col-md-12">
									<div class="card schedule-widget mb-0">
									
										<!-- Schedule Header -->
										<div class="schedule-header">
										
											<!-- Schedule Nav -->
											<div class="schedule-nav">
												<ul class="nav nav-tabs nav-justified">
													<li class="nav-item">
														<a class="nav-link active day_number_selector" data-value="0" data-toggle="tab" href="#slot_sunday">Sunday</a>
													</li>
													<li class="nav-item">
														<a class="nav-link day_number_selector" data-value="1" data-toggle="tab" href="#slot_monday">Monday</a>
													</li>
													<li class="nav-item">
														<a class="nav-link day_number_selector" data-value="2" data-toggle="tab" href="#slot_tuesday">Tuesday</a>
													</li>
													<li class="nav-item">
														<a class="nav-link day_number_selector" data-value="3" data-toggle="tab" href="#slot_wednesday">Wednesday</a>
													</li>
													<li class="nav-item">
														<a class="nav-link day_number_selector" data-value="4" data-toggle="tab" href="#slot_thursday">Thursday</a>
													</li>
													<li class="nav-item">
														<a class="nav-link day_number_selector" data-value="5" data-toggle="tab" href="#slot_friday">Friday</a>
													</li>
													<li class="nav-item">
														<a class="nav-link day_number_selector" data-value="6" data-toggle="tab" href="#slot_saturday">Saturday</a>
													</li>
												</ul>
											</div>
											<!-- /Schedule Nav -->
											
										</div>
										<!-- /Schedule Header -->
										
										<!-- Schedule Content -->
										<div class="tab-content schedule-cont">

										<script>
											var doctorAvailability = {!! ! empty($day_availability) ? json_encode($day_availability) : "{}" !!};
										</script>
										
											<!-- Sunday Slot -->
											<div id="slot_sunday" class="tab-pane fade">
												<h4 class="card-title d-flex justify-content-between">
													<span>Time Slots</span> 
													@if(count($day_availability[0]))
														<a class="edit-link" data-toggle="modal" href="#edit_sunday_time_slot"><i class="fa fa-plus-circle"></i>Edit Slot</a>
													@else
														<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
													@endif
												</h4>
												<div class="doc-times">
													@foreach($day_availability[0] as $slot)
															<div class="doc-slot-list">
																{{ $slot["start_time"] }} - {{ $slot["end_time"] }}
																<a href="javascript:void(0)" class="delete_schedule">
																	<i class="fa fa-times"></i>
																</a>
															</div>
													@endforeach
												</div>
												@if(!count($day_availability[0]))
													<p class="text-muted mb-0">Not Available</p>
												@endif
											</div>
											<!-- /Sunday Slot -->
											<!-- Monday Slot -->
											<div id="slot_monday" class="tab-pane fade">
												<h4 class="card-title d-flex justify-content-between">
													<span>Time Slots</span> 
													@if(count($day_availability[1]))
														<a class="edit-link" data-toggle="modal" href="#edit_monday_time_slot"><i class="fa fa-plus-circle"></i>Edit Slot</a>
													@else
														<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
													@endif
												</h4>
												<div class="doc-times">
													@foreach($day_availability[1] as $slot)
															<div class="doc-slot-list">
																{{ $slot["start_time"] }} - {{ $slot["end_time"] }}
																<a href="javascript:void(0)" class="delete_schedule">
																	<i class="fa fa-times"></i>
																</a>
															</div>
													@endforeach
												</div>
												@if(!count($day_availability[1]))
													<p class="text-muted mb-0">Not Available</p>
												@endif
											</div>
											<!-- /Monday Slot -->
											<!-- Tuesday Slot -->
											<div id="slot_tuesday" class="tab-pane fade">
												<h4 class="card-title d-flex justify-content-between">
													<span>Time Slots</span> 
													@if(count($day_availability[2]))
														<a class="edit-link" data-toggle="modal" href="#edit_tuesday_time_slot"><i class="fa fa-plus-circle"></i>Edit Slot</a>
													@else
														<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
													@endif
												</h4>
												<div class="doc-times">
													@foreach($day_availability[2] as $slot)
															<div class="doc-slot-list">
																{{ $slot["start_time"] }} - {{ $slot["end_time"] }}
																<a href="javascript:void(0)" class="delete_schedule">
																	<i class="fa fa-times"></i>
																</a>
															</div>
													@endforeach
												</div>
												@if(!count($day_availability[2]))
													<p class="text-muted mb-0">Not Available</p>
												@endif
											</div>
											<!-- /Tuesday Slot -->
											<!-- Wednesday Slot -->
											<div id="slot_wednesday" class="tab-pane fade">
												<h4 class="card-title d-flex justify-content-between">
													<span>Time Slots</span> 
													@if(count($day_availability[3]))
														<a class="edit-link" data-toggle="modal" href="#edit_wednesday_time_slot"><i class="fa fa-plus-circle"></i>Edit Slot</a>
													@else
														<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
													@endif
												</h4>
												<div class="doc-times">
													@foreach($day_availability[3] as $slot)
															<div class="doc-slot-list">
																{{ $slot["start_time"] }} - {{ $slot["end_time"] }}
																<a href="javascript:void(0)" class="delete_schedule">
																	<i class="fa fa-times"></i>
																</a>
															</div>
													@endforeach
												</div>
												@if(!count($day_availability[3]))
													<p class="text-muted mb-0">Not Available</p>
												@endif
											</div>
											<!-- /Wednesday Slot -->
											<!-- Thursday Slot -->
											<div id="slot_thursday" class="tab-pane fade">
												<h4 class="card-title d-flex justify-content-between">
													<span>Time Slots</span> 
													@if(count($day_availability[4]))
														<a class="edit-link" data-toggle="modal" href="#edit_thursday_time_slot"><i class="fa fa-plus-circle"></i>Edit Slot</a>
													@else
														<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
													@endif
												</h4>
												<div class="doc-times">
													@foreach($day_availability[4] as $slot)
															<div class="doc-slot-list">
																{{ $slot["start_time"] }} - {{ $slot["end_time"] }}
																<a href="javascript:void(0)" class="delete_schedule">
																	<i class="fa fa-times"></i>
																</a>
															</div>
													@endforeach
												</div>
												@if(!count($day_availability[4]))
													<p class="text-muted mb-0">Not Available</p>
												@endif
											</div>
											<!-- /Thursday Slot -->
											<!-- Friday Slot -->
											<div id="slot_friday" class="tab-pane fade">
												<h4 class="card-title d-flex justify-content-between">
													<span>Time Slots</span> 
													@if(count($day_availability[5]))
														<a class="edit-link" data-toggle="modal" href="#edit_friday_time_slot"><i class="fa fa-plus-circle"></i>Edit Slot</a>
													@else
														<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
													@endif
												</h4>
												<div class="doc-times">
													@foreach($day_availability[5] as $slot)
															<div class="doc-slot-list">
																{{ $slot["start_time"] }} - {{ $slot["end_time"] }}
																<a href="javascript:void(0)" class="delete_schedule">
																	<i class="fa fa-times"></i>
																</a>
															</div>
													@endforeach
												</div>
												@if(!count($day_availability[5]))
													<p class="text-muted mb-0">Not Available</p>
												@endif
											</div>
											<!-- /Friday Slot -->
											<!-- Saturday Slot -->
											<div id="slot_saturday" class="tab-pane fade">
												<h4 class="card-title d-flex justify-content-between">
													<span>Time Slots</span> 
													@if(count($day_availability[6]))
														<a class="edit-link" data-toggle="modal" href="#edit_saturday_time_slot"><i class="fa fa-plus-circle"></i>Edit Slot</a>
													@else
														<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
													@endif
												</h4>
												<div class="doc-times">
													@foreach($day_availability[6] as $slot)
															<div class="doc-slot-list">
																{{ $slot["start_time"] }} - {{ $slot["end_time"] }}
																<a href="javascript:void(0)" class="delete_schedule">
																	<i class="fa fa-times"></i>
																</a>
															</div>
													@endforeach
												</div>
												@if(!count($day_availability[6]))
													<p class="text-muted mb-0">Not Available</p>
												@endif
											</div>
											<!-- /Saturday Slot -->
										</div>
										<!-- /Schedule Content -->
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('popup')
	<!-- Add Time Slot Modal -->
		<div class="modal fade custom-modal" id="add_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Add Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
								<div class="row form-row hours-cont">
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" id="start_time" class="form-control" name="start_time[]" required>
												</div> 
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>													
													<input type="time" id="end_time" class="form-control" name="end_time[]" required>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" id="time_slot_duration" name="time_slot_duration" value="" />
							<input type="hidden" id="day_number" name="day_number" value="0" />
							
							<div class="add-more mb-3">
								<a href="javascript:void(0);" class="add-hours"><i class="fa fa-plus-circle"></i> Add More</a>
							</div>
							<div class="submit-section text-center">
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<!-- /Add Time Slot Modal -->
		
		<!-- Edit Sunday Time Slot Modal -->
		<div class="modal fade custom-modal" id="edit_sunday_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
							<p class="alert" id='day-0-msg'></p>
							@foreach($day_availability[0] as $slot)
								<div class="row form-row hours-cont" id={{ $slot["id"] }}>
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" class="form-control" name="start_time[]" value={{ $slot["start_time"] }} readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>
													<input type="time" class="form-control" name="end_time[]" value={{ $slot["end_time"] }} readonly>
												</div>
											</div>
										</div>
										<div class="col-12"><span class="error-message" id={{ $slot["id"]."msg" }}></span></div>
									</div>
									<div class="col-12 col-md-2">
										<label class="d-md-block d-sm-none d-none">&nbsp;</label>
											<a href="#" 
												data-scheduleId={{ $slot["id"] }}
												data-startTime={{ $slot["start_time"] }}
												data-endTime={{ $slot["end_time"] }}
												data-dayNumber="0"
												class="btn btn-danger trash delete-schedule"
											>
												<i class="far fa-trash-alt"></i>
											</a>
									</div>
								</div>
							@endforeach
							<input type="hidden" name="day_number" value="0" />
							</div>
							<div class="submit-section text-center">
								<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Edit Sunday Time Slot Modal -->

		<!-- Edit Monday Time Slot Modal -->
		<div class="modal fade custom-modal" id="edit_monday_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
							<p class="alert" id='day-1-msg'></p>
							@foreach($day_availability[1] as $slot)
								<div class="row form-row hours-cont" id={{ $slot["id"] }}>
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" id="start_time" class="form-control" name="start_time[]" value={{ $slot["start_time"] }} required>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>													
													<input type="time" id="end_time" class="form-control" name="end_time[]" value={{ $slot["end_time"] }} required>
												</div>
											</div>
										</div>
										<div class="col-12"><span class="error-message" id={{ $slot["id"] }}></span></div>
									</div>
									<div class="col-12 col-md-2">
										<label class="d-md-block d-sm-none d-none">&nbsp;</label>
											<a href="#" 
												data-scheduleId={{ $slot["id"] }}
												data-startTime={{ $slot["start_time"] }}
												data-endTime={{ $slot["end_time"] }}
												data-dayNumber="0"
												class="btn btn-danger trash delete-schedule"
											>
												<i class="far fa-trash-alt"></i>
											</a>
									</div>
								</div>
							@endforeach
							<input type="hidden" name="day_number" value="1" />
							</div>
							<div class="submit-section text-center">
								<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Edit Monday Time Slot Modal -->

		<!-- Edit Tuesday Time Slot Modal -->
		<div class="modal fade custom-modal" id="edit_tuesday_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
							<p class="alert" id='day-2-msg'></p>
							@foreach($day_availability[2] as $slot)
								<div class="row form-row hours-cont" id={{ $slot["id"] }}>
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" id="start_time" class="form-control" name="start_time[]" value={{ $slot["start_time"] }} readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>
													<input type="time" id="end_time" class="form-control" name="end_time[]" value={{ $slot["end_time"] }} readonly>
												</div>
											</div>
										</div>
										<div class="col-12"><span class="error-message"></span></div>
									</div>
									<div class="col-12 col-md-2">
										<label class="d-md-block d-sm-none d-none">&nbsp;</label>
											<a href="#" 
												data-scheduleId={{ $slot["id"] }}
												data-startTime={{ $slot["start_time"] }}
												data-endTime={{ $slot["end_time"] }}
												data-dayNumber="2"
												class="btn btn-danger trash delete-schedule"
											>
												<i class="far fa-trash-alt"></i>
											</a>
									</div>
								</div>
							@endforeach
							<input type="hidden" name="day_number" value="2" />
							</div>
							<div class="submit-section text-center">
								<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Edit Tuesday Time Slot Modal -->

		<!-- Edit wednesday Time Slot Modal -->
		<div class="modal fade custom-modal" id="edit_wednesday_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
							<p class="alert" id='day-3-msg'></p>
							@foreach($day_availability[3] as $slot)
								<div class="row form-row hours-cont" id={{ $slot["id"] }}>
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" class="form-control" name="start_time[]" value={{ $slot["start_time"] }} readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>
													<input type="time" class="form-control" name="end_time[]" value={{ $slot["end_time"] }} readonly>
												</div>
											</div>
										</div>
										<div class="col-12"><span class="error-message"></span></div>
									</div>
									<div class="col-12 col-md-2">
										<label class="d-md-block d-sm-none d-none">&nbsp;</label>
											<a href="#" 
												data-scheduleId={{ $slot["id"] }}
												data-startTime={{ $slot["start_time"] }}
												data-endTime={{ $slot["end_time"] }}
												data-dayNumber="3"
												class="btn btn-danger trash delete-schedule"
											>
												<i class="far fa-trash-alt"></i>
											</a>
									</div>
								</div>
							@endforeach
							<input type="hidden" name="day_number" value="3" />
							</div>							
							<div class="submit-section text-center">
								<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Edit wednesday Time Slot Modal -->

		<!-- Edit thursday Time Slot Modal -->
		<div class="modal fade custom-modal" id="edit_thursday_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
							<p class="alert" id='day-4-msg'></p>
							@foreach($day_availability[4] as $slot)
								<div class="row form-row hours-cont" id={{ $slot["id"] }}>
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" class="form-control" name="start_time[]" value={{ $slot["start_time"] }} readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>
													<input type="time" class="form-control" name="end_time[]" value={{ $slot["end_time"] }} readonly>
												</div>
											</div>
										</div>
										<div class="col-12"><span class="error-message"></span></div>
									</div>
									<div class="col-12 col-md-2">
										<label class="d-md-block d-sm-none d-none">&nbsp;</label>
											<a href="#" 
												data-scheduleId={{ $slot["id"] }}
												data-startTime={{ $slot["start_time"] }}
												data-endTime={{ $slot["end_time"] }}
												data-dayNumber="4"
												class="btn btn-danger trash delete-schedule"
											>
												<i class="far fa-trash-alt"></i>
											</a>
									</div>
								</div>
							@endforeach
							<input type="hidden" name="day_number" value="4" />
							</div>
							<div class="submit-section text-center">
								<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Edit thursday Time Slot Modal -->

		<!-- Edit friday Time Slot Modal -->
		<div class="modal fade custom-modal" id="edit_friday_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
							<p class="alert" id='day-5-msg'></p>
							@foreach($day_availability[5] as $slot)
								<div class="row form-row hours-cont" id={{ $slot["id"] }}>
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" class="form-control" name="start_time[]" value={{ $slot["start_time"] }} readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>
													<input type="time" class="form-control" name="end_time[]" value={{ $slot["end_time"] }} readonly>
												</div>
											</div>
										</div>
										<div class="col-12"><span class="error-message"></span></div>
									</div>
									<div class="col-12 col-md-2">
										<label class="d-md-block d-sm-none d-none">&nbsp;</label>
											<a href="#" 
												data-scheduleId={{ $slot["id"] }}
												data-startTime={{ $slot["start_time"] }}
												data-endTime={{ $slot["end_time"] }}
												data-dayNumber="5"
												class="btn btn-danger trash delete-schedule"
											>
												<i class="far fa-trash-alt"></i>
											</a>
									</div>
								</div>
							@endforeach
							<input type="hidden" name="day_number" value="5" />
							</div>							
							<div class="submit-section text-center">
								<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Edit friday Time Slot Modal -->

		<!-- Edit saturday Time Slot Modal -->
		<div class="modal fade custom-modal" id="edit_saturday_time_slot">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit Time Slots</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{ url('/doctor/schedule-timings') }}" method="POST">
							@csrf
							<div class="hours-info">
							<p class="alert" id='day-6-msg'></p>
							@foreach($day_availability[6] as $slot)
								<div class="row form-row hours-cont" id={{ $slot["id"] }}>
									<div class="col-12 col-md-10">
										<div class="row form-row">
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" class="form-control" name="start_time[]" value={{ $slot["start_time"] }} readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>End Time</label>
													<input type="time" class="form-control" name="end_time[]" value={{ $slot["end_time"] }} readonly>
												</div>
											</div>
										</div>
										<div class="col-12"><span class="error-message"></span></div>
									</div>
									<div class="col-12 col-md-2">
										<label class="d-md-block d-sm-none d-none">&nbsp;</label>
											<a href="#" 
												data-scheduleId={{ $slot["id"] }}
												data-startTime={{ $slot["start_time"] }}
												data-endTime={{ $slot["end_time"] }}
												data-dayNumber="6"
												class="btn btn-danger trash delete-schedule"
											>
												<i class="far fa-trash-alt"></i>
											</a>
									</div>
								</div>
							@endforeach
							<input type="hidden" name="day_number" value="6" />
							</div>							
							<div class="submit-section text-center">
								<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i>Add Slot</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Edit saturday Time Slot Modal -->
		
@endsection