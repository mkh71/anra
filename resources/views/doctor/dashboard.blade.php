@extends('layouts.doctor')

@section('breadcrumb')
<div class="breadcrumb-bar">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-12 col-12">
				<nav aria-label="breadcrumb" class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
					</ol>
				</nav>
				<h2 class="breadcrumb-title">Dashboard</h2>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="col-md-7 col-lg-8 col-xl-9">
	<div class="row">
		<div class="col-md-12">
			<div class="card dash-card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="dash-widget dct-border-rht">
								<div class="circle-bar circle-bar1">
									<div class="circle-graph1" data-percent="75">
										<img src="{{ url('/assets/img/icon-01.png') }}" class="img-fluid" alt="patient">
									</div>
								</div>
								<div class="dash-widget-info">
									<h6>Total Patient</h6>
									<h3>1500</h3>
									<p class="text-muted">Till Today</p>
								</div>
							</div>
						</div>
						
						<div class="col-md-12 col-lg-4">
							<div class="dash-widget dct-border-rht">
								<div class="circle-bar circle-bar2">
									<div class="circle-graph2" data-percent="65">
										<img src="{{ url('/assets/img/icon-02.png') }}" class="img-fluid" alt="Patient">
									</div>
								</div>
								<div class="dash-widget-info">
									<h6>Today Patient</h6>
									<h3>160</h3>
									<p class="text-muted">06, Nov 2019</p>
								</div>
							</div>
						</div>
						
						<div class="col-md-12 col-lg-4">
							<div class="dash-widget">
								<div class="circle-bar circle-bar3">
									<div class="circle-graph3" data-percent="50">
										<img src="{{ url('/assets/img/icon-03.png') }}" class="img-fluid" alt="Patient">
									</div>
								</div>
								<div class="dash-widget-info">
									<h6>Appoinments</h6>
									<h3>85</h3>
									<p class="text-muted">06, Apr 2019</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<h4 class="mb-4">Patient Appoinment</h4>
			<div class="appointment-tab">
			
				<!-- Appointment Tab -->
				<ul class="nav nav-tabs nav-tabs-solid nav-tabs-rounded">
					<li class="nav-item">
						<a class="nav-link active" href="#upcoming-appointments" data-toggle="tab">Upcoming</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#today-appointments" data-toggle="tab">Today</a>
					</li> 
				</ul>
				<!-- /Appointment Tab -->
				
				<div class="tab-content">
				
					<!-- Upcoming Appointment Tab -->
					<div class="tab-pane show active" id="upcoming-appointments">
						<div class="card card-table mb-0">
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-hover table-center mb-0">
										<thead>
											<tr>
												<th>Patient Name</th>
												<th>Appointment Date</th>
												<th>Start Time</th>
												<th>End Time</th>
												<th>Type</th>
												<th class="text-center">Action</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											@if(count($appointments['upcomming']->toArray()) > 0) 
												@foreach($appointments['upcomming'] as $upcomming_appointments)
													<tr>
														<td>
															<h2 class="table-avatar">
																<a href="{{ url('/doctor/patient/'.$upcomming_appointments->patient_id.'/profile') }}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ $upcomming_appointments->patient_info->profile_photo }}" alt="User Image"></a>
																<a href="{{ url('/doctor/patient/'.$upcomming_appointments->patient_id.'/profile') }}">{{ $upcomming_appointments->patient_info->name }} </a>
															</h2>
														</td>
														<td>{{ \Carbon\Carbon::parse($upcomming_appointments->book_on)->format('F j, Y') }} </td>
														<td><span class="d-block text-info">{{ \Carbon\Carbon::parse($upcomming_appointments->start_time)->format('g:i a') }}</span></td>
														<td><span class="d-block text-info">{{ \Carbon\Carbon::parse($upcomming_appointments->end_time)->format('g:i a') }}</span></td>
														<td class="text-center">{{ ucfirst($upcomming_appointments->type) }}</td>
														<td class="text-right">
															<div class="table-action">
																<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																	<i class="far fa-eye"></i> View
																</a>
																
																<a href="javascript:void(0);" class="btn btn-sm bg-success-light">
																	<i class="fas fa-check"></i> Accept
																</a>
																<a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
																	<i class="fas fa-times"></i> Cancel
																</a>
															</div>
														</td>
													</tr>
												@endforeach

											@else
												<tr>
													<td colspan="6" class="text-center">Sorry ! Nothing Found</td>
												</tr>
											@endif
										</tbody>
									</table>		
								</div>
							</div>
						</div>
					</div>
					<!-- /Upcoming Appointment Tab -->
			   
					<!-- Today Appointment Tab -->
					<div class="tab-pane" id="today-appointments">
						<div class="card card-table mb-0">
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-hover table-center mb-0">
										<thead>
											<tr>
												<th>Patient Name</th>
												<th>Appt Date</th>
												<th>Purpose</th>
												<th>Type</th>
												<th class="text-center">Paid Amount</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											@if(count($appointments['today']->toArray()) > 0) 
												@foreach($appointments['today'] as $today_appointments)
													<tr>
														<td>
															<h2 class="table-avatar">
																<a href="patient-profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="../assets/img/patients/patient6.jpg" alt="User Image"></a>
																<a href="patient-profile.html">Elsie Gilley <span>#PT0006</span></a>
															</h2>
														</td>
														<td>14 Nov 2019 <span class="d-block text-info">6.00 PM</span></td>
														<td>Fever</td>
														<td>Old Patient</td>
														<td class="text-center">$300</td>
														<td class="text-right">
															<div class="table-action">
																<a href="javascript:void(0);" class="btn btn-sm bg-info-light">
																	<i class="far fa-eye"></i> View
																</a>
																
																<a href="javascript:void(0);" class="btn btn-sm bg-success-light">
																	<i class="fas fa-check"></i> Accept
																</a>
																<a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
																	<i class="fas fa-times"></i> Cancel
																</a>
															</div>
														</td>
													</tr>
												@endforeach
											@else
												<tr>
													<td colspan="6" class="text-center">Sorry ! Nothing Found</td>
												</tr>
											@endif
											
											
										</tbody>
									</table>		
								</div>	
							</div>	
						</div>	
					</div>
					<!-- /Today Appointment Tab -->
					
				</div>
			</div>
		</div>
	</div>

</div>
@endsection