@extends('layouts.user')

@section('breadcrumb')
<div class="breadcrumb-bar">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-12 col-12">
				<nav aria-label="breadcrumb" class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
					</ol>
				</nav>
				<h2 class="breadcrumb-title">Appointment</h2>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')    
    <!-- Page Content -->
    <div class="container">
            
    
        <div class="row">
            <div class="col-12 search-doctor">
                <div class="row">
                    <div class="col-sm">
                        <input type="text" name="keyword" class="form-control" placeholder="Doctor Name">
                    </div>
                    <div class="col-sm">
                        <select name="specialization" class="form-control">
                            <option value="">--Choose Option--</option>
                            @foreach($categories as $key => $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm">
                        <button type="submit" class="btn btn-primary submit-btn">Search</button>
                    </div>
                </div>
            </div>
            <div class="col-12">
                
                <!-- Schedule Widget -->
                <div class="card booking-schedule schedule-widget">
                    @foreach($doctor_profile as $doctor)
                    <div class="card">
                        <div class="card-body">
                            <div class="doctor-widget">
                                <div class="doc-info-left">
                                    <div class="doctor-img">
                                        <a href="doctor-profile.html">
                                            <img src={{ $doctor['profile_photo'] }} class="img-fluid" alt="User Image">
                                        </a>
                                    </div>
                                    <div class="doc-info-cont">
                                        <h4 class="doc-name"><a href="doctor-profile.html">{{ $doctor['name'] }}</a></h4>
                                        <p class="doc-speciality">
                                        @foreach($doctor['educations'] as $edu)
                                            {{ $edu["degree"] }}
                                        @endforeach
                                        </p>
                                        <!-- <h5 class="doc-department"><img src="../assets/img/specialities/specialities-05.png" class="img-fluid" alt="Speciality">Dentist</h5> -->

                                        <!-- @foreach($doctor['specialities'] as $specialities)
                                            <span class="doc-department">{{ $specialities['doctor_specialities'][0]['name'] }}</span>
                                        @endforeach -->
                                        
                                        
                                        <div class="rating">
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star"></i>
                                            <span class="d-inline-block average-rating">(17)</span>
                                        </div>
                                        <div class="clinic-details">
                                            <p class="doc-location"><i class="fas fa-map-marker-alt"></i> {{ $doctor["city"] }}, {{ $doctor["state"] }}</p>
                                            <ul class="clinic-gallery">
                                                <li>
                                                    <a href="../assets/img/features/feature-01.jpg" data-fancybox="gallery">
                                                        <img src="../assets/img/features/feature-01.jpg" alt="Feature">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../assets/img/features/feature-02.jpg" data-fancybox="gallery">
                                                        <img src="../assets/img/features/feature-02.jpg" alt="Feature">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../assets/img/features/feature-03.jpg" data-fancybox="gallery">
                                                        <img src="../assets/img/features/feature-03.jpg" alt="Feature">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../assets/img/features/feature-04.jpg" data-fancybox="gallery">
                                                        <img src="../assets/img/features/feature-04.jpg" alt="Feature">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clinic-services">
                                            @foreach($doctor['specialities'] as $specialities)
                                                <span>{{ $specialities['doctor_specialities'][0]['name'] }}</span>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="doc-info-right">
                                    <div class="clini-infos">
                                        <ul>
                                            <li><i class="far fa-thumbs-up"></i> 98%</li>
                                            <li><i class="far fa-comment"></i> 17 Feedback</li>
                                            <li><i class="fas fa-map-marker-alt"></i> {{ $doctor["city"] }}, {{ $doctor["state"] }}</li>
                                            <li><i class="far fa-money-bill-alt"></i> $300 - $1000 <i class="fas fa-info-circle" data-toggle="tooltip" title="" data-original-title="Lorem Ipsum"></i> </li>
                                        </ul>
                                    </div>
                                    <div class="clinic-booking">
                                        <a class="view-pro-btn" href="doctor-profile.html">View Profile</a>
                                        <a class="apt-btn" href={{ url('/patient/booking')}}>Book Appointment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                    @endforeach
                </div>
                <!-- /Schedule Widget -->
                
                <!-- Submit Section -->
                <!-- <div class="submit-section proceed-btn text-right">
                    <a href="checkout.html" class="btn btn-primary submit-btn">Proceed to Pay</a>
                </div> -->
                <!-- /Submit Section -->
                
            </div>
        </div>
    </div>		
    <!-- /Page Content -->
@endsection