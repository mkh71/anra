@extends('layouts.user')

@section('breadcrumb')
<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.html">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Profile Settings</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title">Profile Settings</h2>
						</div>
					</div>
				</div>
			</div>
@endsection

@section('content')
<div class="col-md-7 col-lg-8 col-xl-9">
							@if(Session::has('message'))
								<p class="alert alert-success">{{ Session::get('message') }}</p>
							@endif
							<div class="card">
								<div class="card-body">
									
									<!-- Profile Settings Form -->
									<form action="{{ url('/patient/profile/'.$patient["id"].'/update') }}" method="POST">
										@csrf
										<div class="row form-row">
											<div class="col-12 col-md-12">
												<div class="form-group">
													<div class="change-avatar">
														<div class="profile-img">
															<img src="../assets/img/patients/patient.jpg" alt="User Image">
														</div>
														<div class="upload-img">
															<div class="change-photo-btn">
																<span><i class="fa fa-upload"></i> Upload Photo</span>
																<input type="file" class="upload">
															</div>
															<small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
														</div>
													</div>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Name</label>
													<input type="text" class="form-control" name="name" value="{{ $patient["name"] }}">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Date of Birth</label>
													<div class="cal-icon">
														<input type="date" name="dob" value="{{ $patient["dob"] }}" class="form-control datetimepicker" value="24-07-1983">
													</div>
												</div>
											</div>											
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Email ID</label>
													<input type="email" name="email" value="{{ $patient["email"] }}" class="form-control" value="richard@example.com">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Country Code</label>
													<input type="text" name="country_code" value="{{ $patient["country_code"] }}" class="form-control">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Mobile</label>
													<input type="text" value="{{ $patient["phone"] }}" name="phone" class="form-control">
												</div>
											</div>
											<div class="row form-row">						
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Street</label>
													<input type="text" class="form-control" value="{{ $patient["user_address"]["street"] }}" name="street">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>City</label>
													<input type="text" class="form-control" value="{{ $patient["user_address"]["city"] }}" name="city">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Province</label>
													<input type="text" class="form-control" value="{{ $patient["user_address"]["province"] }}" name="province" >
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Country</label>
													<input type="text" class="form-control" value="{{ $patient["user_address"]["country"] }}" name="country">
												</div>
											</div>
											</div>
										</div>
										<div class="submit-section">
											<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
										</div>
									</form>
									<!-- /Profile Settings Form -->
									
								</div>
							</div>
						</div>
@endsection