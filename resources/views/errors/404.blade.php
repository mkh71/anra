<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Anra | Page not found</title>
		
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('/assets/img/favicon.png') }}">
        <link rel="stylesheet" href="{{ url('/assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('/assets/css/font-awesome.min.css') }}">

        <link rel="stylesheet" href="{{ url('/assets/css/feathericon.min.css') }}">
		

        <link rel="stylesheet" href="{{ url('/new_assets/css/style.css') }}">
		
		<!--[if lt IE 9]>
			<script src="{{ url('/assets/js/html5shiv.min.js') }}"></script>
			<script src="{{ url('/assets/js/respond.min.js') }}"></script>
		<![endif]-->
    </head>
    <body class="error-page">

        <div class="main-wrapper">
			
			<div class="error-box">
				<h1>404</h1>
				<h3 class="h2 mb-3"><i class="fa fa-warning"></i> Oops! Page not found!</h3>
				<p class="h4 font-weight-normal">The page you requested was not found.</p>
				<a href="{{ url('/home') }}" class="btn btn-primary">Back to Home</a>
			</div>
		
        </div>

        <script src="{{ url('/assets/js/jquery-3.2.1.min.js') }}"></script>
	
        <script src="{{ url('/assets/js/popper.min.js') }}"></script>
        <script src="{{ url('/assets/js/bootstrap.min.js') }}"></script>
		
	
		<script  src="{{ url('/assets/js/script.js') }}"></script>
		
    </body>
</html>