<!DOCTYPE html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<title>{{ config('app.name', 'Laravel') }}</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<link href="{{ url('/assets/img/favicon.png') }}" rel="icon">
		<link rel="stylesheet" href="{{ url('/assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ url('/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ url('/assets/plugins/fontawesome/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ url('/assets/css/style.css') }}">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="{{ url('/assets/js/html5shiv.min.js') }}"></script>
			<script src="{{ url('/assets/js/respond.min.js') }}"></script>
		<![endif]-->
		@yield('css')
	
	</head>

	<body @if(request()->is('patient/chat')) class="chat-page" @endif>
		<div class="main-wrapper">
			
			<header class="header">
				<nav class="navbar navbar-expand-lg header-nav">
					<div class="navbar-header">
						<a id="mobile_btn" href="javascript:void(0);">
							<span class="bar-icon">
								<span></span>
								<span></span>
								<span></span>
							</span>
						</a>
						<a href="{{ url('/') }}" class="navbar-brand logo">
							<img src="{{ url('assets/img/logo.png') }}" class="img-fluid" alt="Logo">
						</a>
					</div>
					<div class="main-menu-wrapper">
						<div class="menu-header">
							<a href="index.html" class="menu-logo">
								<img src="{{ url('assets/img/logo.png') }}" class="img-fluid" alt="Logo">
							</a>
							<a id="menu_close" class="menu-close" href="javascript:void(0);">
								<i class="fas fa-times"></i>
							</a>
						</div>
						<ul class="main-nav">
							<li class="has-submenu">
								<a href="#">Home</a>
							</li>
							
							<li class="login-link">
								<a href="login.html">Login / Signup</a>
							</li>
						</ul>
					</div>		 
					<ul class="nav header-navbar-rht">
						<li class="nav-item contact-item">
							<div class="header-contact-img">
								<i class="far fa-hospital"></i>							
							</div>
							<div class="header-contact-detail">
								<p class="contact-header">Contact</p>
								<p class="contact-info-header"> +1 315 369 5943</p>
							</div>
						</li>
						
						<!-- User Menu -->
						<li class="nav-item dropdown has-arrow logged-item">
							<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
								<span class="user-img">
									<img class="rounded-circle" src="{{ Auth::User()->profile_photo }} " width="31" alt="{{ Auth::User()->name }}">
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="user-header">
									<div class="avatar avatar-sm">
										<img src="{{ url('assets/img/patients/patient.jpg') }}" alt="User Image" class="avatar-img rounded-circle">
									</div>
									<div class="user-text">
										<h6>Richard Wilson</h6>
										<p class="text-muted mb-0">Patient</p>
									</div>
								</div>
								<a class="dropdown-item" href="patient-dashboard.html">Dashboard</a>
								<a class="dropdown-item" href="profile-settings.html">Profile Settings</a>
								<a class="dropdown-item" href="login.html">Logout</a>
							</div>
						</li>
					</ul>
				</nav>
			</header>
			@yield('breadcrumb')

			@if(!request()->is('patient/chat'))
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
							<div class="profile-sidebar">
								<div class="widget-profile pro-widget-content">
									<div class="profile-info-widget">
										<a href="#" class="booking-doc-img">
											<img src="{{ Auth::User()->profile_photo }}" alt="User Image">
										</a>
										<div class="profile-det-info">
											<h3>{{ Auth::User()->name }}</h3>
											<div class="patient-details">
												<h5><i class="fas fa-birthday-cake"></i>{{ \Carbon\Carbon::parse(Auth::user()->dob)->diff(\Carbon\Carbon::now())->format('%y years');}}</h5>
												<h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> Newyork, USA</h5>
											</div>
										</div>
									</div>
								</div>
								<div class="dashboard-widget">
									<nav class="dashboard-menu">
										<ul>
											<li @if(request()->is('patient/dashboard')) class="active" @endif>
												<a href="{{ url('/patient/dashboard') }}">
													<i class="fas fa-columns"></i>
													<span>Dashboard</span>
												</a>
											</li>
											<li @if(request()->is('patient/appointment')) class="active" @endif>
												<a href="{{ url('/patient/appointment') }}">
													<i class="fa fa-list" aria-hidden="true"></i>
													<span>Book Appointment</span>
												</a>
											</li>
											<li @if(request()->is('patient/favourite/doctors')) class="active" @endif>
												<a href="{{ url('/patient/favourite/doctors') }}">
													<i class="fas fa-bookmark"></i>
													<span>Favourite Doctors</span>
												</a>
											</li> 
											<li @if(request()->is('patient/dependent')) class="active" @endif>
												<a href="{{ url('/patient/dependent') }}">
													<i class="fas fa-users"></i>
													<span>Dependent</span>
												</a>
											</li> 
											<li @if(request()->is('patient/chat')) class="active" @endif>
												<a href="{{ url('/patient/chat') }}">
													<i class="fas fa-comments"></i>
													<span>Message</span>
													<small class="unread-msg">23</small>
												</a>
											</li>
											<li @if(request()->is('patient/profile-settings')) class="active" @endif>
												<a href="{{ url('/patient/profile-settings') }}">
													<i class="fas fa-user-cog"></i>
													<span>Profile Settings</span>
												</a>
											</li>
											<li @if(request()->is('patient/change-password')) class="active" @endif>
												<a href="{{ url('/patient/change-password') }}">
													<i class="fas fa-lock"></i>
													<span>Change Password</span>
												</a>
											</li>
											<li>
												<a href="{{ route('logout') }}" onclick="event.preventDefault(); 
            document.getElementById('logout-form').submit();">
													<i class="fas fa-sign-out-alt"></i>
													<span>Logout</span>
												</a>
												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
											</li>
										</ul>
									</nav>
								</div>

							</div>
						</div>
						@yield('content')
					</div>
				</div>		
			</div>
			@endif

		</div>	

		<script src="{{ url('/assets/js/jquery.min.js') }}"></script>
		<script src="{{ url('/assets/js/popper.min.js') }}"></script>
		<script src="{{ url('/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('/assets/plugins/theia-sticky-sidebar/ResizeSensor.js') }}"></script>
        <script src="{{ url('/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js') }}"></script>
		<script src="{{ url('/assets/js/script.js') }}"></script>
		@yield('javascript')
	</body>	
</html>	