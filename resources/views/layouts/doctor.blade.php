<!DOCTYPE html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<title>{{ config('app.name', 'Laravel') }}</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<link href="{{ url('/assets/img/favicon.png') }}" rel="icon">
		<link rel="stylesheet" href="{{ url('/assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ url('/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ url('/assets/plugins/fontawesome/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ url('/assets/plugins/select2/css/select2.min.css') }}">
		<link rel="stylesheet" href="{{ url('/assets/css/style.css') }}">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="{{ url('/assets/js/html5shiv.min.js') }}"></script>
			<script src="{{ url('/assets/js/respond.min.js') }}"></script>
		<![endif]-->
		@yield('css')
	
	</head>

	<body @if(request()->is('doctor/chat')) class="chat-page" @endif>
		<div class="main-wrapper">
			
			<header class="header">
				<nav class="navbar navbar-expand-lg header-nav">
					<div class="navbar-header">
						<a id="mobile_btn" href="javascript:void(0);">
							<span class="bar-icon">
								<span></span>
								<span></span>
								<span></span>
							</span>
						</a>
						<a href="{{ url('/') }}" class="navbar-brand logo">
							<img src="{{ url('assets/img/logo.png') }}" class="img-fluid" alt="Logo">
						</a>
					</div>
					<div class="main-menu-wrapper">
						<div class="menu-header">
							<a href="index.html" class="menu-logo">
								<img src="{{ url('assets/img/logo.png') }}" class="img-fluid" alt="Logo">
							</a>
							<a id="menu_close" class="menu-close" href="javascript:void(0);">
								<i class="fas fa-times"></i>
							</a>
						</div>
						<ul class="main-nav">
							<li class="has-submenu">
								<a href="#">Home</a>
							</li>
							
							<li class="login-link">
								<a href="login.html">Login / Signup</a>
							</li>
						</ul>
					</div>		 
					<ul class="nav header-navbar-rht">
						<li class="nav-item contact-item">
							<div class="header-contact-img">
								<i class="far fa-hospital"></i>							
							</div>
							<div class="header-contact-detail">
								<p class="contact-header">Contact</p>
								<p class="contact-info-header"> +1 315 369 5943</p>
							</div>
						</li>
						
						<!-- User Menu -->
						<li class="nav-item dropdown has-arrow logged-item">
							<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
								<span class="user-img">
									<img class="rounded-circle" src="{{ Auth::User()->profile_photo }} " width="31" alt="{{ Auth::User()->name }}">
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="user-header">
									<div class="avatar avatar-sm">
										<img src="{{ url('assets/img/patients/patient.jpg') }}" alt="User Image" class="avatar-img rounded-circle">
									</div>
									<div class="user-text">
										<h6>Richard Wilson</h6>
										<p class="text-muted mb-0">Patient</p>
									</div>
								</div>
								<a class="dropdown-item" href="patient-dashboard.html">Dashboard</a>
								<a class="dropdown-item" href="profile-settings.html">Profile Settings</a>
								<a class="dropdown-item" href="login.html">Logout</a>
							</div>
						</li>
					</ul>
				</nav>
			</header>
			@yield('breadcrumb')

			@if(!request()->is('doctor/chat'))
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
							<div class="profile-sidebar">
								<div class="widget-profile pro-widget-content">
									<div class="profile-info-widget">
										<a href="#" class="booking-doc-img">
											<img src="{{ Auth::User()->profile_photo }}" alt="User Image">
										</a>
										<div class="profile-det-info">
											<h3>{{ Auth::User()->name }}</h3>
											@php $doc_categs = \App\Models\DoctorSpecialities::where('doc_id', Auth::user()->id)->pluck('categ_id')->toArray(); @endphp

											@if(count($doc_categs) > 0) 
												@php $categs = \App\Models\Category::whereIn('id', $doc_categs)->pluck('name')->toArray(); @endphp									
												@php  $doctor_categ = implode(", ",$categs); @endphp
											@else
												@php  $doctor_categ = Auth::User()->email @endphp
											@endif

											<div class="patient-details">
												<h5 class="mb-0">{{ $doctor_categ }}</h5>
											</div>
										</div>
									</div>
								</div>
								<div class="dashboard-widget">
									<nav class="dashboard-menu">
										<ul>
											<li @if(request()->is('doctor/dashboard')) class="active" @endif>
												<a href="{{ url('/doctor/dashboard') }}">
													<i class="fas fa-columns"></i>
													<span>Dashboard</span>
												</a>
											</li>
											<li @if(request()->is('doctor/appointments')) class="active" @endif>
												<a href="{{ url('/doctor/appointments') }}">
													<i class="fas fa-calendar-check"></i>
													<span>Appointments</span>
												</a>
											</li>
											<li @if(request()->is('doctor/my-patients')) class="active" @endif>
												<a href="{{ url('/doctor/my-patients') }}">
													<i class="fas fa-user-injured"></i>
													<span>My Patients</span>
												</a>
											</li>
											<li @if(request()->is('doctor/schedule-timings')) class="active" @endif>
												<a href="{{ url('/doctor/schedule-timings') }}">
													<i class="fas fa-hourglass-start"></i>
													<span>Schedule Timings</span>
												</a>
											</li>
											
											
											<li @if(request()->is('doctor/chat')) class="active" @endif>
												<a href="{{ url('/doctor/chat') }}">
													<i class="fas fa-comments"></i>
													<span>Message</span>
													<small class="unread-msg">23</small>
												</a>
											</li>
											<li @if(request()->is('doctor/profile-settings')) class="active" @endif>
												<a href="{{ url('/doctor/profile-settings') }}">
													<i class="fas fa-user-cog"></i>
													<span>Profile Settings</span>
												</a>
											</li>
											
											<li @if(request()->is('doctor/change-password')) class="active" @endif>
												<a href="{{ url('/doctor/change-password') }}">
													<i class="fas fa-lock"></i>
													<span>Change Password</span>
												</a>
											</li>


											<li>
												<a href="{{ route('logout') }}" onclick="event.preventDefault(); 
            document.getElementById('logout-form').submit();">
													<i class="fas fa-sign-out-alt"></i>
													<span>Logout</span>
												</a>
												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>

						@yield('content')
					</div>
				</div>
			</div>
			@endif


			<footer class="footer">
                <div class="footer-bottom">
					<div class="container-fluid">
						<div class="copyright">
							<div class="row">
								<div class="col-md-6 col-lg-6">
									<div class="copyright-text">
										<p class="mb-0">&copy; 2021 Anra. All rights reserved.</p>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="copyright-menu">
										<ul class="policy-menu">
											<li><a href="{{ url('/term-condition') }}">Terms and Conditions</a></li>
											<li><a href="{{ url('/privacy-policy') }}">Policy</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>		
		</div>	
		@yield('popup')

		<script src="{{ url('/assets/js/jquery.min.js') }}"></script>
		<script src="{{ url('/assets/js/popper.min.js') }}"></script>
		<script src="{{ url('/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('/assets/plugins/theia-sticky-sidebar/ResizeSensor.js') }}"></script>
        <script src="{{ url('/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js') }}"></script>
		<script src="{{ url('/assets/js/circle-progress.min.js') }}"></script>
		<script src="{{ url('/assets/plugins/select2/js/select2.min.js') }}"></script>
		<script src="{{ url('/assets/js/script.js') }}"></script>
		<script type="text/javascript">
			var base_url = <?php echo "'".url('/')."';"; ?>
			$(document).ready(function() {
				console.log(doctorAvailability, "sdasd");
				$('#time_slot').on('change', function() {
					var time_slot = this.value
					$('#time_slot_duration').val(time_slot);
				});

				$('.day_number_selector').on('click', function() {
					var day_number = $(this).attr("data-value");
					$('#day_number').val(day_number);
				});

				$(".edit-link").on('click', function() {
					$('.close').click();
				});

				$(document).on('click', '.delete-schedule', function(e) {					
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
        			e.preventDefault();
					var scheduleId = $(this).attr("data-scheduleId");
					var startTime = $(this).attr("data-startTime");
					var endTime = $(this).attr("data-endTime");
					var dayNumber = $(this).attr("data-dayNumber");

					if (!scheduleId) {
						$(this).closest('.hours-cont').remove();
						return false;
					}

					$.ajax({
						url: base_url+"/doctor/delete-schedule",
						type: "DELETE",
						data: { id: scheduleId, start_time: startTime, end_time: endTime, day: dayNumber },
						dataType: 'json',
						success: function(response) {
							console.log(response.message);
							$("#"+scheduleId).remove();
							document.getElementById('day-'+dayNumber+'-msg').innerHTML = response.message;
							// window.location.reload();

						},
						error: function (data) {
							console.log(data);
						}
					});
				})
			});
		</script>
		@yield('javascript')
	</body>	
</html>		