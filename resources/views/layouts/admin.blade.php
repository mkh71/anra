<!DOCTYPE html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<title>{{ config('app.name', 'Laravel') }}</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<link href="{{ url('/new_assets/img/favicon.png') }}" rel="icon">
		<link rel="stylesheet" href="{{ url('/new_assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ url('/new_assets/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ url('/new_assets/plugins/fontawesome/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ url('/new_assets/css/feathericon.min.css') }}">
		<link rel="stylesheet" href="{{ url('/new_assets/plugins/morris/morris.css') }}">
		<link rel="stylesheet" href="{{ url('/new_assets/css/style.css') }}">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="{{ url('/new_assets/js/html5shiv.min.js') }}"></script>
			<script src="{{ url('/new_assets/js/respond.min.js') }}"></script>
		<![endif]-->
		@yield('css')
	</head>

	<body>
        <div class="main-wrapper">
            <div class="header">
                <div class="header-left">
                    <a href="{{ url('/') }}" class="logo">
						<img src="{{ url('assets/img/logo.png') }}" alt="Logo">
					</a>
					<a href="index.html" class="logo logo-small">
						<img src="{{ url('assets/img/logo.png') }}" alt="Logo" width="30" height="30">
					</a>
                </div>
				
				<a href="javascript:void(0);" id="toggle_btn">
					<i class="fe fe-text-align-left"></i>
				</a>
				
				<a class="mobile_btn" id="mobile_btn">
					<i class="fa fa-bars"></i>
				</a>

				<ul class="nav user-menu">
					<li class="nav-item dropdown noti-dropdown">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
							<i class="fe fe-bell"></i> <span class="badge badge-pill">3</span>
						</a>
						<div class="dropdown-menu notifications">
							<div class="topnav-dropdown-header">
								<span class="notification-title">Notifications</span>
								<a href="javascript:void(0)" class="clear-noti"> Clear All </a>
							</div>
							<div class="noti-content">
								<ul class="notification-list">
									<li class="notification-message">
										<a href="#">
											<div class="media">
												<span class="avatar avatar-sm">
													<img class="avatar-img rounded-circle" alt="User Image" src="assets/img/doctors/doctor-thumb-01.jpg">
												</span>
												<div class="media-body">
													<p class="noti-details"><span class="noti-title">Dr. Ruby Perrin</span> Schedule <span class="noti-title">her appointment</span></p>
													<p class="noti-time"><span class="notification-time">4 mins ago</span></p>
												</div>
											</div>
										</a>
									</li>
									<li class="notification-message">
										<a href="#">
											<div class="media">
												<span class="avatar avatar-sm">
													<img class="avatar-img rounded-circle" alt="User Image" src="assets/img/patients/patient1.jpg">
												</span>
												<div class="media-body">
													<p class="noti-details"><span class="noti-title">Charlene Reed</span> has booked her appointment to <span class="noti-title">Dr. Ruby Perrin</span></p>
													<p class="noti-time"><span class="notification-time">6 mins ago</span></p>
												</div>
											</div>
										</a>
									</li>
									<li class="notification-message">
										<a href="#">
											<div class="media">
												<span class="avatar avatar-sm">
													<img class="avatar-img rounded-circle" alt="User Image" src="assets/img/patients/patient2.jpg">
												</span>
												<div class="media-body">
												<p class="noti-details"><span class="noti-title">Travis Trimble</span> sent a amount of $210 for his <span class="noti-title">appointment</span></p>
												<p class="noti-time"><span class="notification-time">8 mins ago</span></p>
												</div>
											</div>
										</a>
									</li>
									<li class="notification-message">
										<a href="#">
											<div class="media">
												<span class="avatar avatar-sm">
													<img class="avatar-img rounded-circle" alt="User Image" src="assets/img/patients/patient3.jpg">
												</span>
												<div class="media-body">
													<p class="noti-details"><span class="noti-title">Carl Kelly</span> send a message <span class="noti-title"> to his doctor</span></p>
													<p class="noti-time"><span class="notification-time">12 mins ago</span></p>
												</div>
											</div>
										</a>
									</li>
								</ul>
							</div>
							<div class="topnav-dropdown-footer">
								<a href="#">View all Notifications</a>
							</div>
						</div>
					</li>
					
					<li class="nav-item dropdown has-arrow">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
							<span class="user-img"><img class="rounded-circle" src="{{ Auth::User()->profile_photo }}" width="31" alt="Ryan Taylor"></span>
						</a>
						<div class="dropdown-menu">
							<div class="user-header">
								<div class="avatar avatar-sm">
									<img src="{{ Auth::User()->profile_photo }}" alt="User Image" class="avatar-img rounded-circle">
								</div>
								<div class="user-text">
									<h6>{{ Auth::User()->name }}</h6>
									<p class="text-muted mb-0">Administrator</p>
								</div>
							</div>
							<a class="dropdown-item" href="{{ url('/admin/profile') }}">My Profile</a>
							<a class="dropdown-item" href="{{ url('/admin/settings') }}">Settings</a>
							<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
						</div>
					</li>
					<!-- /User Menu -->
					
				</ul>
            </div>
			
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
					<div id="sidebar-menu" class="sidebar-menu">
						<ul>
							<li class="menu-title"> 
								<span>Main</span>
							</li>
							<li @if(request()->is('admin/dashboard')) class="active" @endif> 
								<a href="{{ url('/admin/dashboard') }}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
							</li>
							<li @if(request()->is('admin/appointments')) class="active" @endif> 
								<a href="{{ url('/admin/appointments') }}"><i class="fe fe-layout"></i> <span>Appointments</span></a>
							</li>
							<li @if(request()->is('admin/specialities')) class="active" @endif> 
								<a href="{{ url('/admin/specialities') }}"><i class="fe fe-users"></i> <span>Specialities</span></a>
							</li>
							<li @if(request()->is('admin/doctors') || request()->is('admin/doctor/*/profile') || request()->is('admin/doctor/create')) class="active" @endif> 
								<a href="{{ url('/admin/doctors') }}"><i class="fe fe-user-plus"></i> <span>Doctors</span></a>
							</li>
							<li @if(request()->is('admin/patients')) class="active" @endif> 
								<a href="{{ url('/admin/patients') }}"><i class="fe fe-user"></i> <span>Patients</span></a>
							</li>

							<li class="submenu">
								<a href="#" @if(request()->is('admin/intake/forms') || request()->is('admin/intake/forms/quesiton/add') || request()->is('admin/intake/form/*/category/list')) class="subdrop" @endif>
									<i class="fe fe-document"></i> 
										<span>Intake Form</span> <span class="menu-arrow"></span></a>
								<ul @if(request()->is('admin/intake/forms') || request()->is('admin/intake/forms/quesiton/add') || request()->is('admin/intake/form/*/category/list')) style="display: block" @else style="display: none" @endif>
									<li @if(request()->is('admin/intake/forms') || request()->is('admin/intake/form/*/category/list')) class="active" @endif><a href="{{ url('/admin/intake/forms') }}" >Forms</a></li>
									<li @if(request()->is('admin/intake/forms/quesiton/add')) class="active" @endif><a href="{{ url('/admin/intake/forms/quesiton/add') }}">Questions</a></li>
								</ul>
							</li>

							<li class="submenu">
								<a href="#"><i class="fe fe-star"></i> <span>Tips</span> <span class="menu-arrow"></span></a>
								<ul style="display: none">
									<li><a href="{{ url('/admin/tips/categories') }}">Category</a></li>
									<li><a href="{{ url('/admin/tips/list') }}">List</a></li>
								</ul>
							</li>
							
							<li @if(request()->is('admin/packages')) class="active" @endif> 
								<a href="{{ url('/admin/packages') }}"><i class="fe fe-cart"></i> <span>Packages</span></a>
							</li>

							<li @if(request()->is('admin/website_settings')) class="active" @endif> 
								<a href="{{ url('/admin/website_settings') }}"><i class="fe fe-vector"></i> <span>Website Settings</span></a>
							</li>

							
						</ul>
					</div>
                </div>
            </div>

            <div class="page-wrapper">			
                <div class="content container-fluid">
					@yield('breadcrumb')
					@yield('content')
				</div>			
			</div>
			@yield('popup')
        </div>
		

		<script src="{{ url('/new_assets/js/jquery-3.2.1.min.js') }}"></script>
		<script src="{{ url('/new_assets/js/popper.min.js') }}"></script>
		<script src="{{ url('/new_assets/js/bootstrap.min.js') }}"></script>
	    <script src="{{ url('/new_assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	    <script src="{{ url('/new_assets/plugins/raphael/raphael.min.js') }}"></script>
		<script src="{{ url('/new_assets/plugins/morris/morris.min.js') }}"></script>
		<script src="{{ url('/new_assets/js/chart.morris.js') }}"></script>
		<script src="{{ url('/new_assets/js/script.js') }}"></script>
		<script src="{{ url('/js/deleteCategory.js') }}"></script>
		@yield('javascript')
	</body>
</html>