<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('welcome');
});

Route::get('/term-condition', function () {
    return view('term-condition');
});
Route::get('/privacy-policy', function () {
    return view('privacy-policy');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function () {
	
	/*Admin Routes*/
	Route::prefix('admin')->group(function () {
		Route::group(['middleware' => ['check_admin']], function () { 
			Route::get('dashboard', 'Admin\DashboardController@getDashboard');
			Route::get('appointments', 'Admin\AppointmentController@getAllAppointments');
			Route::get('specialities', 'Admin\CategoryController@getAllCategories');
			Route::post('specialities', 'Admin\CategoryController@store');
			Route::get('doctors', 'Admin\DoctorController@getDoctors');
			Route::get('patients', 'Admin\PatientController@getPatients');
			Route::get('patient/{id}/profile', 'Admin\PatientController@getPatientProfile');	
			Route::get('doctor/{id}/profile', 'Admin\DoctorController@getDoctorProfile');
			Route::get('doctor/create', 'Admin\DoctorController@getCreateForm');
			Route::get('patient/create', 'Admin\PatientController@getCreateForm');
			Route::get('patient/{id}/update', 'Admin\PatientController@showPatientEdit');
			Route::post('patient/{id}/update', 'Admin\PatientController@updatePatient');
			Route::get('website_settings', 'Admin\WebsiteSettingsController@getSettings');
			Route::post('create-doctor', 'Admin\DoctorController@updateProfileSettings');
			Route::get('update/{id}/doctor', 'Admin\DoctorController@showEditProfile');
			Route::post('update-doctor', 'Admin\DoctorController@updateDoctorProfileSettings');
			Route::post('create-patient', 'Admin\PatientController@createPatient');
			/* Tips Categ */
			Route::get('/tips/categories', 'Admin\TipCategoryController@getCategoryList');	
			Route::get('/tips/category/create', 'Admin\TipCategoryController@getCategoryCreateForm');
			Route::post('/tips-category/create', 'Admin\TipCategoryController@saveCategory');
			Route::get('/tips-category/{categ_id}/detail', 'Admin\TipCategoryController@getCategDetails');
			Route::post('/tips-category/{categ_id}/update', 'Admin\TipCategoryController@updateCatgegory');
			/* Tips  */
			Route::get('/tips/list', 'Admin\TipController@getTipsList');
			Route::get('/tip/create', 'Admin\TipController@getTipCreateForm');
			Route::post('/tip/create', 'Admin\TipController@saveOrUpdateTip');
			Route::get('/tip/{tip_id}/detail', 'Admin\TipController@getTipDetails');
			Route::post('/tip/update', 'Admin\TipController@saveOrUpdateTip');

			/*Intake Form*/
			Route::get('/intake/forms', 'Admin\IntakeFormController@getIntakeFormsList');
			Route::get('/intake/form/create', 'Admin\IntakeFormController@getAddFormCategForm');
			Route::post('/intake/form/store', 'Admin\IntakeFormController@saveIntakeForm');
			Route::get('/intake/form/{id}/edit', 'Admin\IntakeFormController@getIntakeEditForm');
			Route::get('/intake/form/{id}/delete', 'Admin\IntakeFormController@deleteIntakeForm');

			Route::get('/intake/form/{form_id}/category/list', 'Admin\IntakeFormController@getFormQuestionCategList');
			Route::get('/intake/forms/quesiton/add', 'Admin\IntakeFormController@getAddQuestionForm');
			
			Route::post('/intake/question/create', 'Admin\IntakeFormController@storeIntakeQuestion');
			Route::post('/intake/question/update', 'Admin\IntakeFormController@updateIntakeQuestion');


			Route::get('/intake/form/categories', 'Admin\IntakeFormController@getCategFromFormId');
			Route::get('/intake/form/{form_id}/category/create', 'Admin\IntakeFormController@getIntakeCategFormCreate');
			Route::get('/intake/form/category/{id}/edit', 'Admin\IntakeFormController@getIntakeCategEditForm');
			Route::get('/intake/form/category/{id}/delete', 'Admin\IntakeFormController@deleteCateg');
			Route::post('/intake/form/category/save', 'Admin\IntakeFormController@saveCateg');

			Route::get('/intake/form/category/{id}/questions', 'Admin\IntakeFormController@getCategQuestions');

			Route::get('/intake/question/{id}/edit', 'Admin\IntakeFormController@getEditIntakeQuestionForm');
			Route::get('/intake/question/{id}/delete', 'Admin\IntakeFormController@deleteIntakeQuestion');

			Route::get('/packages', 'Admin\PackageController@getPackages');
			Route::get('/package/{id}/edit', 'Admin\PackageController@getPackageDetailById');
			Route::get('/package/add', 'Admin\PackageController@getAddPackageForm');
			Route::post('/package/store', 'Admin\PackageController@saveOrUpdatePackage');

		});
	});

	/*Doctor Routes*/
	Route::prefix('doctor')->group(function () {
		Route::group(['middleware' => ['check_doctor']], function () { 
			Route::get('dashboard', 'Doctor\DashboardController@getDashboard');
			Route::get('appointments', 'Doctor\AppointmentController@getAppointments');
			Route::get('my-patients', 'Doctor\PatientController@getPatientListByDoctors');
			Route::get('schedule-timings', 'Doctor\SettingController@getScheduleTimings');
			Route::post('schedule-timings', 'Doctor\SettingController@addScheduleTimings');
			Route::delete('delete-schedule', 'Doctor\SettingController@deleteSchedule');
			Route::get('chat', 'Doctor\ChatController@getChats');
			Route::get('profile-settings', 'Doctor\SettingController@getProfileSettings');
			Route::get('change-password', 'Doctor\SettingController@getChangePassword');
			Route::post('update-profile', 'Doctor\SettingController@updateProfileSettings');
			Route::post('update-password', 'Doctor\SettingController@updatePassword');
			
		});
	});

	/*Patient Routes*/
	Route::prefix('patient')->group(function () {
		Route::group(['middleware' => ['check_patient']], function () { 
			Route::get('dashboard', 'Patient\DashboardController@getDashboard');
			Route::get('favourite/doctors', 'Patient\DoctorController@getPatientFavDoctors');
			Route::get('dependent', 'Patient\PatientController@getDependent');
			Route::get('chat', 'Patient\ChatController@getChats');
			Route::get('profile-settings', 'Patient\SettingController@getProfileSettings');
			Route::post('profile/{id}/update', 'Patient\SettingController@update');
			Route::get('change-password', 'Patient\SettingController@getChangePassword');
			Route::post('update-password', 'Patient\SettingController@updatePassword');
			Route::get('appointment', 'Patient\AppointmentController@appointment');
			Route::get('booking', 'Patient\AppointmentController@booking');
			Route::get('search-doctor', 'Patient\AppointmentController@searchDoctor');
			
		});
	});
});
