<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\RegisterController@login');

Route::group(['prefix' => 'patient'], function () {
	Route::post('register', 'Api\UserController@store');
});

Route::post('forgot_password', 'Api\UserController@forgotPassword');

Route::group(['middleware' => ['auth:api']], function () {
	Route::post('verify_otp', 'Api\UserController@verifyOtp');
	Route::get('resend_otp', 'Api\RegisterController@resendOTP');

	Route::group(['middleware' => ['is_otp_verified']], function () { /*OTP Verfied or Not*/
		Route::group(['middleware' => ['check_patient']], function () { /*Check user is Patient and have active status*/
			Route::get('packages', 'Api\PackageController@index'); 
			Route::post('purchase/package', 'Api\PackageController@purchase'); 

			Route::post('pur/package', 'Api\PackageController@purchasePackageWithPaymentIntent'); 


			Route::group(['middleware' => ['check_patient_package']], function () {/*Check Patient have active package*/ 
				Route::get('doctor/{doctor_id}/profile', 'Api\DoctorController@getDoctorProfile');
				Route::get('doctor/{doctor_id}/education', 'Api\DoctorController@getEducationList');
				Route::get('doctor/{doctor_id}/awards', 'Api\DoctorController@getAwardsList');
				Route::get('doctor/{doctor_id}/work_experience', 'Api\DoctorController@getWorkExperienceList');
				Route::get('doctor/{doctor_id}/other_work_locations', 'Api\DoctorController@getOtherWorkLocations');
				Route::get('doctor/{doctor_id}/availability', 'Api\DoctorController@getAvailability');
				Route::get('doctor/{doctor_id}/specialities', 'Api\DoctorController@getSpecialities');

				Route::get('categories', 'Api\CategoryController@index');
				Route::get('category/{categ_id}/doctors', 'Api\DoctorController@getDoctorByCateg');

				Route::get('doctors', 'Api\DoctorController@index');
				Route::post('book_appointment', 'Api\AppointmentController@bookAppointment');
				Route::get('appointments', 'Api\AppointmentController@getPatientAppointments');

				Route::post('favourite/doctor', 'Api\DoctorController@markDoctorAsFavourite');
				Route::get('favourite/doctors', 'Api\DoctorController@getPatientFavouriteDoctorList');
				Route::post('unfavourite/doctor', 'Api\DoctorController@markDoctorAsUnfav');

				Route::post('vital', 'Api\VitalController@addVital');
				Route::post('vitals', 'Api\VitalController@getVitalsByDate');
				Route::get('vital/last_sync', 'Api\VitalController@getLastSyncDate');
				Route::post('vital/auto_sync', 'Api\VitalController@vitalSyncFromSDK');

				Route::get('tips', 'Api\TipController@getTips');

				Route::post('favourite/tips', 'Api\TipController@markTipAsFavourite');
				Route::get('favourite/tips', 'Api\TipController@getFavouriteTips');

				Route::post('save/tips', 'Api\TipController@markTipAsSaved');
				Route::get('saved/tips', 'Api\TipController@getSavedTips');

				Route::post('location', 'Api\UserController@addUserLocation');
				Route::get('locations', 'Api\UserController@getUserLocations');
				Route::get('location/{location_id}/edit', 'Api\UserController@editUserLocation');
				Route::post('location/update', 'Api\UserController@updateLocation');

				Route::get('notes', 'Api\NotesController@getPatientNotes');

				Route::get('doctor/appointment/available_time', 'Api\DoctorController@getDoctorAvailability');

				Route::get('intake/form', 'Api\IntakeFormController@getIntakeForm');
				Route::get('intake/form/status', 'Api\IntakeFormController@getIntakeFormStatus');
				Route::post('intake/form', 'Api\IntakeFormController@saveIntakeForm');

				Route::get('tasks', 'Api\TaskController@getPatientTasks');
				Route::post('task/{task_id}/schedule', 'Api\TaskController@scheduleTask');
				Route::post('task/{task_id}/status', 'Api\TaskController@updateTaskStatus');

				Route::post('profile', 'Api\UserController@updateMyProfile');

				//Route::get('doctor/task/category/{categ_id}', 'Api\TaskController@getTaskByCategId');

				Route::post('search', 'Api\UserController@searhGlobally');

				Route::prefix('medication')->group(function () {
					Route::post('/create', 'Api\MedicationController@addMedication');

					Route::get('/reminders', 'Api\MedicationController@getAllReminders');
					Route::get('/reminder/{reminder_id}/edit', 'Api\MedicationController@getReminderInfo');
					Route::post('/reminder/update', 'Api\MedicationController@updateMedicationReminder');


					Route::get('/prescription', 'Api\MedicationController@getPatientPrescription');
					// Route::get('prescription', 'Api\MedicineController@getPatientPrescription');
				});

			});
		});

		Route::group(['middleware' => ['check_doctor']], function () {
			Route::get('doctor/profile', 'Api\DoctorController@getDoctorProfile');
			Route::get('doctor/education', 'Api\DoctorController@getEducationList');
			Route::get('doctor/awards', 'Api\DoctorController@getAwardsList');
			Route::get('doctor/work_experience', 'Api\DoctorController@getWorkExperienceList');
			Route::get('doctor/other_work_locations', 'Api\DoctorController@getOtherWorkLocations');
			Route::get('doctor/availability', 'Api\DoctorController@getAvailability');
			Route::get('doctor/specialities', 'Api\DoctorController@getSpecialities');

			Route::get('doctor/appointments', 'Api\AppointmentController@getDoctorAppointments');
			Route::get('doctor/patient/{patient_id}/appointment', 'Api\AppointmentController@getPatientAppointmentsByPatientId');
			Route::post('doctor/update_appointment_status', 'Api\AppointmentController@updateAppointmentStatus');
			
			Route::post('doctor/update_appointment', 'Api\AppointmentController@updateAppointment');
			Route::post('doctor/availability/add', 'Api\DoctorController@addDoctorAvailability');
			Route::post('doctor/day/availability', 'Api\DoctorController@addDoctorAvailabilityByDayNumber');
			Route::post('doctor/availability/delete', 'Api\DoctorController@deleteDoctorAvailability');
			Route::post('doctor/availability/update', 'Api\DoctorController@updateDoctorAvailability');

			Route::get('doctor/patient/{patient_id}/profile', 'Api\UserController@getPatientProfileById');

			Route::post('doctor/patient/notes', 'Api\NotesController@getNotesByPatinetId');
			Route::post('doctor/patient/vitals', 'Api\VitalController@getVitalsByPatientId');

			Route::post('doctor/notes', 'Api\NotesController@saveNotes');

			Route::post('doctor/patient/search', 'Api\DoctorController@searchPatient');

			Route::get('doctor/{patient_id}/intake/form', 'Api\IntakeFormController@getPatientIntakeFormByPatientId');
			Route::get('doctor/{patient_id}/intake/form/status', 'Api\IntakeFormController@getIntakeFormStatusByPatientId');
			Route::post('doctor/{patient_id}/intake/form', 'Api\IntakeFormController@saveIntakeFormByPatientId');

			Route::get('doctor/task/categories', 'Api\TaskController@getTaskCategories');
			Route::get('doctor/task/category/{categ_id}', 'Api\TaskController@getTaskByCategId');
			Route::post('doctor/task', 'Api\TaskController@storeTask');
			Route::post('doctor/assign_task', 'Api\TaskController@assignTask');
			Route::get('doctor/patient/{patient_id}/tasks', 'Api\TaskController@getTaskByPatientId');
			Route::get('doctor/task/category/{categ_id}/subcateg', 'Api\TaskController@getCategSubCateg');
			Route::get('doctor/added_tasks', 'Api\TaskController@getAddedTasksByDoctor');


			Route::get('doctor/staff/types', 'Api\StaffController@getStaffTypeList');
			Route::post('doctor/staff', 'Api\StaffController@createStaff');
			Route::get('doctor/staff', 'Api\StaffController@getStaffList');
			Route::post('doctor/staff/status', 'Api\StaffController@updateStaffStatus');

			Route::prefix('doctor/medication')->group(function () {
				Route::post('/create', 'Api\MedicationController@addPatientMedication');
				Route::get('/reminders', 'Api\MedicationController@getPatientAllReminders');
				Route::get('/reminder/{reminder_id}/edit', 'Api\MedicationController@getPatientReminderInfo');
				Route::post('/reminder/update', 'Api\MedicationController@updatePatientMedicationReminder');
				Route::get('/prescription', 'Api\MedicationController@getPatientPrescriptionById');
					// Route::get('prescription', 'Api\MedicineController@getPatientPrescription');
			});
		});

		Route::group(['middleware' => ['check_system_nurse']], function () {
			Route::get('patient/list', 'Api\UserController@getPatientList');
		});

		Route::post('test_notification', 'Api\PushNotificationController@testPushNotification'); 
		Route::post('device/token', 'Api\UserController@updateDeviceToken');

		Route::post('chat/session', 'Api\TokBoxController@createSession');
    	Route::post('chat/token', 'Api\TokBoxController@createTokBoxToken');
    	Route::post('call/notification', 'Api\TokBoxController@sendNotification');
    	Route::get('call/notification', 'Api\TokBoxController@getTokNotification');

		Route::post('/search/users', 'Api\UserController@searchUserByEmailOrPhone');
		Route::get('/support', 'Api\UserController@getSupportUsers');

		Route::post('/chat/group', 'Api\ChatRoomController@createChatGroup');
		Route::get('/chat/groups', 'Api\ChatRoomController@getChatGroups');
		Route::get('/chat/group/detail', 'Api\ChatRoomController@getChatGroupDetail');
	    Route::post('/group_image', 'Api\ChatRoomController@updateGroupImage');
	    Route::post('/chat/group/update', 'Api\ChatRoomController@addUserToGroup');
	    Route::post('/send_message', 'Api\ChatRoomController@sendMessage');
	    Route::post('/group/messages', 'Api\ChatRoomController@getGroupRoomMessages');

	    Route::post('change_password', 'Api\UserController@changePassword');

	    Route::get('profile', 'Api\UserController@profileData');
	    Route::get('logout', 'Api\UserController@getLogout');
	});    
    
});

